<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends MY_Controller
{

	public function __construct()
	{	
		parent::__construct();
		
		if(Filter::user_type() != "admin"){
			$this->session->set_flashdata('message','You Do Not have right Permission.');
			redirect('auth/logout','refresh');
		}

	}
}