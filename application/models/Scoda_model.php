<?php 
class Scoda_model extends CI_Model
{
	public function get_menus()
	{
		$query=$this->db->order_by('menu_order','ASC')->get('menu');
		$users=$query->result_array();
		return $users;
	}

	public function infographics()
	{
		$query=$this->db->order_by('id','ASC')->get('infographic');
		$users=$query->result_array();
		return $users;
	}

	public function testimonials()
	{
		$query=$this->db->order_by('testimonial_id','ASC')->get('testimonials');
		$users=$query->result_array();
		return $users;

	}
	public function num_rows()
	{
		$query=$this->db->order_by('notice_id','ASC')->get('notice');
		return $query->num_rows();
	}

	public function notice($limit,$offset)
	{
		$query=$this->db->order_by('notice_id','ASC')->limit($limit,$offset)->get('notice');
		return $query->result_array();
	}

		function getNotice($userId)
	{
	
		$query = $this->db->get_where('notice', array('page_slug' => $userId));
		$user=$query->row_array();
		return $user;

	}

	function getJob($userId)
	{
	
		$query = $this->db->get_where('jobs', array('job_id' => $userId));
		$user=$query->row_array();
		return $user;

	}
	
}



 ?>