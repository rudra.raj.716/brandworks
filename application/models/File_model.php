<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class File_Model extends MY_Model{

		public $_table = "files";
		public $primary_key = "file_id";


		public function delete_file($file_id)
		{
			$this->load->library('File');

			$file_name = $this->get($file_id)->original_name;

			$status = $this->file->delete_file($file_name,'assets\uploads');

			$this->delete($file_id);
			
		}


	}
?>