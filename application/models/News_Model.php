<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_Model extends MY_Model
{
	public $_table = "news";
	public $primary_key = "news_id";

	public function get_num_rows(){
		return $this->db->select('*')->from('news')->order_by('news_published_date', 'DESC')->get()->num_rows();
		return $result;
	}

	public function get_news($limit = 0, $offset = 0)
	{
		return $this->db->select('*')->from('news')->order_by('news_published_date', 'DESC')->limit($limit,$offset)->get()->result();
		//return $result;
	}

	public function get_news_home($limit = 5)
	{
		return $this->db->select('*')->from('news')->where('news_image !=', '')->order_by('news_published_date','DESC')->order_by('news_id','DESC')->limit(5)->get()->result();
	}
}
