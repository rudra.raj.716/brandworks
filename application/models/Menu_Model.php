<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_Model extends MY_Model
{
	public $_table = "menu";
	public $primary_key = "menu_id";

}