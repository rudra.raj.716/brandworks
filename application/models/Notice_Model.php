<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notice_Model extends MY_Model
{
	public $_table = "notice";
	public $primary_key = "notice_id";

	public function get_num_rows($notice_type){
		return $this->db->select('*')->from('notice')->order_by('notice_date', 'DESC')->where('notice_type',$notice_type)->get()->num_rows();
		return $result;
	}

	public function get_notice($limit = 0, $offset = 0, $notice_type)
	{
		return $this->db->select('*')->from('notice')->where('notice_type',$notice_type)->order_by(' notice_date', 'DESC')->limit($limit,$offset)->get()->result();
		return $result;
	}

	public function get_notice_by_type($notice_type = 'General')
	{
		return $this->db->select('*')->from('notice')->where('notice_type',$notice_type)->where('notice_image !=', '')->order_by('notice_date','DESC')->order_by('notice_id','DESC')->limit(5)->get()->result();
	}
}
