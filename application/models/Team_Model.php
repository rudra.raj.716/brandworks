<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team_Model extends MY_Model
{
	public $_table = "teams";
	public $primary_key = "team_id";

}