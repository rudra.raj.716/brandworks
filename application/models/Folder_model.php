<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Folder_Model extends MY_Model{

		public $_table = "folders";
		public $primary_key = "folder_id";


		public function delete_folder($folder_id)
		{
			$this->delete($folder_id);

			$this->load->model('File_model');

			$allfiles = $this->File_model->get_many_by('folder_id',$folder_id);

			foreach($allfiles as $af)
			{
				$this->File_model->delete_file($af->file_id);
			}
		}

	}
?>