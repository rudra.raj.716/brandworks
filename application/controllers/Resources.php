<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Resources extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->data = new stdClass();
		$this->load->model('Program_Model');
	}

	public function index($slug)
	{	
		$this->data->program = (array)$this->db->select('*')->from('programs')->where('page_slug',$slug)->get()->row();

		$this->template->title('Kathmandu School of Law')
	        			->set_layout('frontend/single.php')
	       				->build('frontend/program.php', $this->data);
	}	
}