<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Scoda extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->data = new stdClass();
		$this->load->model('Scoda_model');
	}
	public function index()
	{


			$menus=$this->Scoda_model->get_menus();
			$data=array();
			$data['menus']=$menus;




		$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/new_home.php',$data);
	}

	public function about_us()
	{
		$menus=$this->Scoda_model->get_menus();
			$data=array();
			$data['menus']=$menus;
		$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/about_us.php',$data);
	}

	public function privacy_policy()
	{
		$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/privacy_policy.php');
	}

	public function career()
	{
		$menus=$this->Scoda_model->get_menus();
			$data=array();
			$data['menus']=$menus;

		$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/career.php',$data);

	}

	public function notice()
	{
			$config=[
			'base_url'=>base_url('scoda/notice'),
			'per_page'=>2,
			'total_rows'=>$this->Scoda_model->num_rows(),	

			];

			$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

		    
			$this->pagination->initialize($config);
			$data=array();
			$menus=$this->Scoda_model->get_menus();

			$paginate=$this->Scoda_model->num_rows();
			$users=$this->Scoda_model->notice($config['per_page'], $this->uri->segment(3));
			$data['users']=$users;

			
			$data['menus']=$menus;
			$data['paginate']=$paginate;
			
			$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/notice.php',$data);
	}

	public function blog_standard()
	{	
		$menus=$this->Scoda_model->get_menus();
			$data=array();
			$data['menus']=$menus;



		$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/blog_standard.php',$data);

	}

	public function single_notice($id)
	{
			$users=$this->Scoda_model->getNotice($id);
			$data=array();
			$data['users']=$users;
			$menus=$this->Scoda_model->get_menus();
			
			$data['menus']=$menus;

					$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/blog_standard.php',$data);


	}
	public function projects()
	{
		$users=$this->Scoda_model->isHomepage();
		$data=array();
		
		$data['users']=$users;
		$menus=$this->Scoda_model->get_menus();
		$data['menus']=$menus;

					$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/new_home.php',$data);		

	}
	// public function apply($id)
	// {
	// 		$users=$this->Scoda_model->getJob($id);
	// 		$data=array();
	// 		$data['users']=$users;

	// 		$menus=$this->Scoda_model->get_menus();
			
			
	// 		$data['menus']=$menus;

			
    		
	// 	$this->template->title('Scoda')
	//         			->set_layout('frontend/new_public.php')
	//        				->build('frontend/apply.php',$data);

	// }

	// public function box_columns()
	// {
	// 	$this->template->title('Scoda')
	//         			->set_layout('frontend/new_public.php')
	//        				->build('frontend/box-4-columns.php');		
	// }

	 public function portfolio()
	{

			$menus=$this->Scoda_model->get_menus();
			$data=array();
			$data['menus']=$menus;

		$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/portfolio.php',$data);		

	}

	public function contact()
	{
		if($_POST)
		{
			$from =  $this->input->post('email');  // User email pass here
   			$subject = $this->input->post('subject');
   			$name= $this->input->post('name');
   			


    $to = 'avikkhadka10@gmail.com';              // Pass here your mail id

    
    $emailContent .='<tr><td style="height:20px"></td></tr>';

    $emailContent .= "<strong>".$name."</strong>"."<br>";
    	
    $emailContent .= $this->input->post('message');  //   Post message available here


    
                    


    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'ssl://smtp.gmail.com';
    $config['smtp_port']    = '465';
    $config['smtp_timeout'] = '60';

    $config['smtp_user']    = 'avikkhadka10@gmail.com';    //Important
    $config['smtp_pass']    = 'yaoomnoo03';  //Important

    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['mailtype'] = 'html'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not 

     

    $this->email->initialize($config);
    $this->email->set_mailtype("html");
    $this->email->from($from);
    $this->email->to($to);
    $this->email->subject($subject);
    $this->email->message($emailContent);
    $this->email->send();

    $this->session->set_flashdata('msg',"Mail has been sent successfully");
    $this->session->set_flashdata('msg_class','alert-success');
			redirect('Scoda/contact',$data);
			

		}
		$menus=$this->Scoda_model->get_menus();
			$data=array();
			$data['menus']=$menus;

			
    		
		$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/contact.php',$data);

	}

	public function carousel()
	{
		$this->load->view('frontend/carousel');
	}

	public function us()
	{

			$menus=$this->Scoda_model->get_menus();
			$data=array();
			$data['menus']=$menus;

			
    		
		$this->template->title('Scoda')
	        			->set_layout('frontend/new_public.php')
	       				->build('frontend/us.php',$data);



	}

	


}