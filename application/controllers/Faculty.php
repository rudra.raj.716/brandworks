<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Faculty extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->data = new stdClass();
		$this->load->model('Faculty_Model');
	}

	public function index($faculty_type)
	{
		if($faculty_type == 'full-time')
		{
			$type = 'Full Time Faculty';
		}
		if($faculty_type == 'visiting-faculty')
		{
			$type = 'Visiting Faculty';
		}
		if($faculty_type == 'visiting-researchers')
		{
			$type = 'Visiting Researchers';
		}
		if($faculty_type == 'visiting-fellows')
		{
			$type = 'Visiting Fellows';
		}
		
		$this->data->type = $type;
		$this->data->faculty = (array)$this->db->select('*')->from('faculty')->where('type',$type)->order_by('faculty_order','ASC')->get()->result();
		$this->template->title('Kathmandu School of Law')
	        			->set_layout('frontend/single.php')
	       				->build('frontend/faculty.php', $this->data);
	}

	public function member($member_slug)
	{
		$this->data->member = (array)$this->db->select('*')->from('faculty')->where('page_slug',$member_slug)->order_by('faculty_order','ASC')->get()->row();
		
		$this->template->title('Kathmandu School of Law')
	        			->set_layout('frontend/single.php')
	       				->build('frontend/faculty_single.php', $this->data);
	}
}