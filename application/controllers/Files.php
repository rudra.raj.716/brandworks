<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends Admin_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('Folder_model');
		$this->load->model('File_model');
		$this->load->library('File');
		$this->data = new stdClass;
	}


	public function all_folders($project_id=0)
	{
		$this->data->folders = $this->Folder_model->get_many_by('project_id',$project_id);	
		$this->data->project_id = $project_id;
		$this->template->title('Files')
	        			->set_layout('backend/admin.php')
	       				->build('admin/all_folders.php',$this->data);
	}

	public function make_folder($project_id)
	{
			$post = $this->input->post();
			$post['project_id'] = $project_id;

			$this->form_validation->set_rules('folder_name', 'Folder Name', 'required|is_unique[folders.folder_name]');

				if($this->form_validation->run() == TRUE)
                {
                     $this->Folder_model->insert($post);
                     $this->session->set_flashdata('message','Successfully Created new folder');
                }
                else
                {
                	$this->session->set_flashdata('errors', validation_errors());
                	
                }
               
 			redirect('files/all_folders/' . $project_id,'refresh');
	}

	public function all_files($folder_id)
	{
		$this->data->folder_info = (array)$this->Folder_model->get($folder_id);
		$this->data->files = $this->File_model->get_many_by('folder_id',$folder_id);
		$this->template->title('Files')
	        			->set_layout('backend/admin.php')
	       				->build('admin/all_files.php',$this->data);

	}

	public function ajax_upload_files($folder_id)
	{
		$data = $this->file->move_file($_FILES , 'assets/uploads');
		$data['folder_id'] = $folder_id;
		$file_id = $this->File_model->insert($data);

		 echo $file_id;
		     
	}




	public function edit_file($file_id)
	{
		$post = $this->input->post();

		$file_info = $this->File_model->get($file_id);

		$this->File_model->update($file_id,$post);
		
		redirect('files/all_files/' .  $file_info->folder_id);

	}

	public function delete_files($file_id)
	{
		$this->File_model->delete_file($file_id);		
	}

	public function delete_folder($folder_id)
	{
		$this->Folder_model->delete_folder($folder_id);
	}
	

}
