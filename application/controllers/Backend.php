<?php defined('BASEPATH') or exit('No direct script access allowed');

class Backend extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->data = new stdClass();
	}

	public function dashboard()
	{
		$this->template->title('Dashboard')
			->set_layout('backend/admin.php')
			->build('admin/dashboard.php', $this->data);
	}

	public function page()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('page');
			$crud->set_subject('Page');
			$crud->field_type('page_slug', 'hidden');
			$crud->field_type('page_url', 'hidden');
			$crud->unset_texteditor('meta_description', 'meta_keywords', 'og_description', 'featured_image_description');
			$crud->columns('page_title', 'page_slug', 'page_url');
			$crud->set_field_upload('featured_image', 'assets/uploads/');
			$crud->set_field_upload('og_image', 'assets/uploads/');
			$crud->unset_texteditor('page_intro');
			$crud->callback_before_insert(function ($post_array) {
				$post_array['page_slug'] = General_helper::slugify($post_array['page_title']);
				$post_array['page_url'] = base_url() . 'page/' . $post_array['page_slug'];
				return $post_array;
			});

			$crud->callback_before_update(function ($post_array, $primarykey) {
				$post_array['page_slug'] = General_helper::slugify($post_array['page_title']);
				$post_array['page_url'] = base_url() . 'page/' . $post_array['page_slug'];
				return $post_array;
			});

			$output = $crud->render();

			$this->template->title('Pages')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function menu($parent_id = 0)
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('menu');
			$crud->where('parent_id', $parent_id);
			$crud->set_subject('Menu');
			$crud->order_by('menu_order', 'ASC');
			$crud->add_action('Childrens', '', 'backend/menu', 'fa fa-child');

			$output = $crud->render();

			$this->template->title('Dashboard')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function slider()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('slider');
			$crud->set_subject('Slider');
			$crud->set_field_upload('slider_image', 'assets/uploads');
			$output = $crud->render();

			$this->template->title('Slider')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function aboutus()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('about_us');
			$crud->set_subject('About Us');
			$crud->set_field_upload('aboutus_image', 'assets/uploads');
			$crud->unset_texteditor('aboutus_description');
			$output = $crud->render();
			$this->template->title('Award')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function contact()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('contact');
			$crud->unset_add();
			$crud->unset_edit();
			$crud->order_by('contact_id', 'DESC');
			$crud->set_subject('Contact');
			$output = $crud->render();
			$this->template->title('Contact')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function faq()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('faq');
			$output = $crud->render();
			$this->template->title('FAQ')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function project_category()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('project_category');

			$crud->set_field_upload('featured_image', 'assets/uploads/');
			$crud->unset_texteditor('meta_description', 'meta_keywords');

			$crud->field_type('page_slug', 'hidden');

			$crud->callback_before_insert(function ($post_array) {
				$post_array['page_slug'] = General_helper::slugify($post_array['project_category_name']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$crud->callback_before_update(function ($post_array, $primarykey) {
				$post_array['page_slug'] = General_helper::slugify($post_array['project_category_name']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$output = $crud->render();
			$this->template->title('Project Category')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function project()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('projects');
			$crud->unset_texteditor('meta_description', 'meta_keywords');
			$crud->set_relation('project_category_id', 'project_category', 'project_category_name');
			$crud->set_field_upload('project_image', 'assets/uploads/');
			$crud->field_type('page_slug', 'hidden');
			$crud->callback_before_insert(function ($post_array) {
				$post_array['page_slug'] = General_helper::slugify($post_array['project_title']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$crud->callback_before_update(function ($post_array, $primarykey) {
				$post_array['page_slug'] = General_helper::slugify($post_array['project_title']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$output = $crud->render();
			$this->template->title('Projects')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function clients()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('clients');
			$crud->set_subject('Clients');
			$crud->set_field_upload('client_image', 'assets/uploads');
			$output = $crud->render();
			$this->template->title('Clients')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function whatwedo()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('whatwedo');
			$crud->set_subject('What We Do');
			$crud->set_field_upload('whatwe_do_image', 'assets/uploads');
			$crud->unset_texteditor('whatwedo_description');
			$crud->field_type('page_slug', 'hidden');
			$crud->callback_before_insert(function ($post_array) {
				$post_array['page_slug'] = General_helper::slugify($post_array['whatwedo_title']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$crud->callback_before_update(function ($post_array, $primarykey) {
				$post_array['page_slug'] = General_helper::slugify($post_array['whatwedo_title']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$output = $crud->render();
			$this->template->title('Award')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function project_gallery()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('project_gallery');
			$crud->set_subject('Project Gallery');
			$crud->set_field_upload('gallery_image', 'assets/uploads');

			$crud->set_relation('project_id', 'projects', 'project_title');
			$output = $crud->render();

			$this->template->title('Gallery')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}
}
