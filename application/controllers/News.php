<?php defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->data = new stdClass();
		$this->load->model('News_Model');
	}

	public function faculty_news()
	{	
		$this->load->model('News_Model');
		$config['base_url'] = base_url('news/faculty-news');
		$config['total_rows'] = $this->News_Model->get_num_rows();
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;
		 // Bootstrap 4 Pagination fix
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
    // $config['next_tag_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tag_close']  = '</span></li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tag_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tag_close']  = '</span></li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->data->news = (array)$this->News_Model->get_news($config["per_page"], $page);
		$this->template->title('Kathmandu School of Law')
	        			->set_layout('frontend/single.php')
	       				->build('frontend/news.php', $this->data);
	}	

	public function single($slug)
	{
		$this->data->news_single = (array)$this->db->select('*')->from('news')->where('page_slug',$slug)->order_by('news_published_date','DESC')->get()->row();
		
		$this->template->title('Kathmandu School of Law')
	        			->set_layout('frontend/single.php')
	       				->build('frontend/news_single.php', $this->data);
	}
}