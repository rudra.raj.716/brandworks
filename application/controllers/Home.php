<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->data = new stdClass();
		$this->load->model('Page_Model');
		$this->load->model('Slider_Model');
		$this->load->model('Contact_Model');
		$this->load->model('Team_Model');
		$this->load->model('Notice_Model');
		$this->load->model('Service_Model');
		$this->load->model('Faq_Model');
		$this->load->model('Testimonial_Model');
		$this->load->model('Product_Model');
		$this->load->model('ProductCategory_Model');
		$this->load->helper('captcha');
		$this->load->model('Project_Model');
		$this->load->model('ProjectCategory_Model');
		$this->load->model('Menu_Model');
	}

	public function index()
	{
		$data=array();
		$data['aboutus'] = $this->db->get('about_us')->row_array();
		$data['settings'] = $this->db->get('setting')->row_array();
		$data['services'] = $this->db->get('services',3)->result_array();
		$data['testimonials'] = $this->db->get('testimonials')->result_array();
		$data['clients'] = $this->db->order_by('rankorder','ASC')->get('clients')->result_array();
		$data['whatwedo'] = $this->db->get('whatwedo')->result_array();
		$data['faq'] = $this->db->get('faq')->result_array();
		$data['awards'] = $this->db->limit(8,0)->get('awards')->result_array();
		$data['sliders'] = $this->db->get('slider')->result_array();
		$data['adv_type'] = $this->db->get('advertisement_type')->result_array();
		
		$socials=array();
		
		$data['socials']=$socials;
		$this->template->title('Avani')
	        			->set_layout('frontend/public.php')
	       				->build('frontend/home.php',$data);

	}


	public function casestudy()
	{
		$data['casestudy'] = $this->db->select('*')->from('case_study')->get()->result_array();
		$this->template->title('Avani')
		->set_layout('frontend/singlegrid.php')
		->build('frontend/casestudy.php', $data);

	}

	public function case($slug)
	{
		$data['case'] = $this->db->select('*')->from('case_study')->where('page_slug',$slug)->get()->row_array();
		$case_id = $data['case']['case_study_id'];
		$data['case_extra'] = $this->db->select('*')->from('case_extra')->where('case_id',$case_id)->get()->result_array();
		$data['casestudy'] = $this->db->select('*')->from('case_study')->get()->result_array();

		$this->template->title('Avani')
		->set_layout('frontend/singlegrid.php')
		->build('frontend/casesingle.php', $data);
	}

	public function awards()
	{
		$data['awards'] = $this->db->select('*')->from('awards')->get()->result_array();
		$this->template->title('Avani')
		->set_layout('frontend/singlegrid.php')
		->build('frontend/awards.php', $data);
	}

	public function projects($slug)
	{
		$projecttype = $this->db->select('*')->from('advertisement_type')->where('page_slug',$slug)->get()->row_array();
		$project_type_id = $projecttype['advertisement_type_id'];
		$projects = $this->db->select('*')->from('advertisement')->where('advertisement_type_id', $project_type_id)->get()->result_array();
		$data['projecttype'] = $projecttype;
		$data['projects'] = $projects;
		$this->template->title('Avani')
		->set_layout('frontend/singlegrid.php')
		->build('frontend/advertisementtype.php', $data);
	}
}
