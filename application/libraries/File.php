<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth
*
* Author: Ben Edmunds
*		  ben.edmunds@gmail.com
*         @benedmunds
*
* Added Awesomeness: Phil Sturgeon
*
* Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
*
* Created:  10.01.2009
*
* Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
* Original Author name has been kept but that does not mean that the method has not been modified.
*
* Requirements: PHP5 or above
*
*/

class File
{
	/**
	 * account status ('not_activated', etc ...)
	 *
	 * @var string
	 **/
	protected $status;

	/**
	 * extra where
	 *
	 * @var array
	 **/
	public $_extra_where = array();

	/**
	 * extra set
	 *
	 * @var array
	 **/
	public $_extra_set = array();

	/**
	 * caching of users and their groups
	 *
	 * @var array
	 **/
	public $_cache_user_in_group;

	/**
	 * __construct
	 *
	 * @author Ben
	 */
	public function __construct()
	{
		
	}

	/**
	 * __call
	 *
	 * Acts as a simple way to call model methods without loads of stupid alias'
	 *
	 * @param $method
	 * @param $arguments
	 * @return mixed
	 * @throws Exception
	 */
		

	public function move_file($files,$path,$file_url=NULL)
	{
		$data = [];
		 $ds= DIRECTORY_SEPARATOR;  //1
 
			$storeFolder = $path;   //2

		if (!empty($files)) {
		     
		    $tempFile = $files['file']['tmp_name'];        


		      
		    $targetPath = FCPATH . $storeFolder . $ds; 
		     
		    $targetFile =  $targetPath. $files['file']['name']; 

		    $file_name = $files['file']['name'];

		    if (file_exists($targetFile)) {
		    	$file_name = rand().$files['file']['name'];
		    	$targetFile = $targetPath .$file_name;
		    }
		   	
		    move_uploaded_file($tempFile,$targetFile);
		    $data['original_name'] = $file_name;
		    $data['file_type'] = pathinfo($file_name, PATHINFO_EXTENSION);
		    $data['file_name'] = pathinfo($file_name, PATHINFO_FILENAME);

		    if($file_url == NULL){
		    	$data['file_url'] = base_url() . 'assets/uploads/' . $file_name;	
		    }else{
		    	$data['file_url'] = base_url() . $file_url . $file_name;
		    }
		    
		    

		}

		return $data;
	}

	public function delete_file($file_name,$path)
	{
		$ds= DIRECTORY_SEPARATOR;  //1
		$storeFolder = $path;   //2
		$targetPath = FCPATH . $storeFolder . $ds; 


		 $unlink = unlink($targetPath . $file_name);
			return $unlink;
	}

}
