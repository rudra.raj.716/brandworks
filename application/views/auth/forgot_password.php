<?php $this->load->view('layouts/backend/partials/admin_header.php'); ?>
<body class="hold-transition login-page" style="background-color: #FFF" >
	<div class="login-box">
	  
			<h1><?php //echo lang('forgot_password_heading');?></h1>
			<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

			<div id="infoMessage"><?php echo $message;?></div>

			 <div class="panel panel-default" >
                <div class="panel-heading">Forgot Password</div>
                <div class="panel-body">

				<?php echo form_open("auth/forgot_password");?>
					 <div class="form-group">

					      	<label for="identity"> Email Address :</label>
					      	<?php echo form_input($identity);?>

				      </div>

				       <div class="form-group" style="text-align: center; margin-top: 10px;">
				       <div style="height: 10px;"></div> <br>
				       <input type="submit" value="Submit" class = 'btn btn-primary btn-block'>
				      <?php //echo form_submit('submit', lang('forgot_password_submit_btn'),['class' => 'btn btn-primary']);?>
				      </div>

				<?php echo form_close();?>
			</div>
		</div>
	</div>
</body>