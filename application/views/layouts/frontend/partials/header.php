<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <?php $meta_tags = Filter::get_meta_tags('home',0);
  $first_uri = $this->uri->segment(1);
  if ($this->uri->segment(1) == "case")
  {
        $meta_tags = Filter::get_meta_tags('case',$this->uri->segment(2));
  }
  elseif ($this->uri->segment(1) == "projects")
  {
        $meta_tags = Filter::get_meta_tags('projects',$this->uri->segment(2));
  } 
  elseif ($this->uri->segment(1) == "page")
  {
    $meta_tags = Filter::get_meta_tags('page',$this->uri->segment(2));
  }
  ?>
  
  <title><?php echo $meta_tags->meta_title; ?><?php if($first_uri == 'casestudy'){ echo ' | Case Study'; }elseif($first_uri == 'awards'){ echo ' | Awards';}elseif($first_uri == 'contact'){ echo ' | Contact';} ?></title>
  <meta name="description" content="<?php echo $meta_tags->meta_description; ?>">
  <meta name="keywords" content="<?php echo $meta_tags->meta_keywords; ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:title" content="<?php echo $meta_tags->og_title; ?>" />
  <meta property="og:url" content="<?php echo $meta_tags->og_url; ?>" />
  <meta property="og:image" content="<?php echo base_url().'assets/uploads/'.$meta_tags->og_image; ?>" />
  <meta property="og:type" content="<?php echo $meta_tags->og_type; ?>" />
  <meta property="og:description" content="<?php echo $meta_tags->og_description; ?>" />
  <meta property="og:locale" content="<?php echo $meta_tags->og_locale; ?>" />

  <script type="application/ld+json">
  <?php echo $meta_tags->schema_our; ?>
  </script>

  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owlcarousel.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owlthemedefault.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,300&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/aa17207af2.js" crossorigin="anonymous"></script>
  <meta name="theme-color" content="#fafafa">



</head>

<body>
  <!-- avani template creation -->

  <div class="menu_area">
    <div class="container">
      <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <!-- Brand -->
        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/logo.png" alt=""></a>

        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Navbar links -->
        <?php
        $menus = General_helper::get_menus();
        //pr($menus,1);
        ?>

        <div class="collapse navbar-collapse pull-right" id="collapsibleNavbar">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>">Home</a></li>
            <?php foreach ($menus as $menu) {
            ?>
              <?php if (isset($menu['sub'])) { ?>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $menu['main']->menu_title; ?>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php foreach($menu['sub'] as $mk => $mv){ ?>
                      <a class="dropdown-item" href="<?php echo base_url() . $mv->link; ?>"><?php echo $mv->menu_title;?></a>
                    <?php } ?>
                  </div>

                </li>
              <?php } else { ?>
                <li class="nav-item"><a class="nav-link" href='<?php echo base_url() .$menu['main']->link; ?>'><?php echo $menu['main']->menu_title; ?></a></li>
              <?php } ?>
            <?php } ?>
            <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>contact">Contact</a></li>
          </ul>
        </div>
      </nav>
    </div>
  </div>