<div id="footer_area">
  <div class="container">
    <div class="row">
    <?php $aboutus = Filter::aboutus(); ?>
      <div class="col-md-4">
        <img src="<?php echo base_url();?>assets/img/logo.png" >
        <p class = "aboutdesc">Avani Advertising is a leading Communication, Branding, and Marketing agency in Nepal.</p>
      </div>

      <div class="col-md-4 offset-md-1">
        <h2>Quick Links</h2>
        <ul>
          <?php 
        
          foreach($menus as $menu) 
          {?>
          
          <?php if (isset($menu['sub'])) { ?>
            <?php foreach($menu['sub'] as $mk => $mv){ ?>
              <li><a href = "<?php echo base_url() . $mv->link; ?>"><?php echo $mv->menu_title;?></a></li>
            <?php } ?>
          <?php }else{ ?>
            <li><a href = "<?php echo base_url() . $menu['main']->link; ?>"><?php echo $menu['main']->menu_title; ?></a></li>
          
          <?php } ?>
        <?php }?>
        <li><a href = "<?php echo base_url() . 'contactus' ?>">Contact Us</a></li>
        </ul>
        
      </div>

      <div class="col-md-3">
        <h2>Contact Us</h2>
        <ul>
          <li><i class="fa fa-map-marker myicon"></i> <a class = "myiconinner" href="#">Kupondole, Lalitpur, Nepal</a> </li>
          <li><i class="fa fa-mobile myicon"></i> <i class="icofont icofont-iphone"></i> <a href="#" class = "myiconinner">00977-1-5012014, 5546043</a> </li>
          <li><i class="fa fa-envelope myicon"></i> <a href="#" class = "myiconinner">info@avani.com.np</a> </li>
          <li><i class="fa fa-globe myicon"></i> <a href="#" class = "myiconinner">www.avani.com.np</a> </li>
          

          <?php $socials = Filter::get_social_links(); ?>
          <li>
            <?php foreach($socials as $social)
          {?> 
          <a target = '_blank' href="<?php echo $social['socialmedia_link'];?>" class = "myiconsocial"><i class="<?php echo $social['socialmedia_icon'];?> footersocial"> </i></a>
          <?php }?>
           </li>

        </ul>
      </div>
    </div>
  </div>
</div>

<div id="copyright_area">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <p>Copyright &copy; <?php echo date('Y');?> - All Rights Reserved - avani.com.np</p>
      </div>
      <div class="col-md-6 right">
        <p>Developed By <a target = '_blank' style = 'color:#fff;' href = 'https://mantrait.com.np'>Mantra IT Solution</a></p>
      </div>
    </div>
  </div>
</div>

  <script src="<?php echo base_url();?>assets/js/vendor/modernizr-3.11.2.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/plugins.js"></script>


  <script src="https://www.google-analytics.com/analytics.js" async></script>
  <script src="<?php echo base_url();?>assets/js/jquery.js" ></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" async></script>
  <script src="<?php echo base_url();?>assets/js/owlcarousel.js" ></script>
  <script src="<?php echo base_url();?>assets/js/main.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
</body>
</html>