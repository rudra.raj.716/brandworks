
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.incognitothemes.com/scoda/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2021 06:36:16 GMT -->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Avani</title>
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/master.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">

</head>
<body>

<!--=== Loader Start ======-->
<div id="loader-overlay">
  <div class="loader-wrapper">
    <div class="scoda-pulse"></div>
  </div>
</div>
<!--=== Loader End ======-->

<!--=== Wrapper Start ===-->
<div class="wrapper" id="main">

  <!--=== Header Start ===-->
  <nav class="navbar navbar-default navbar-fixed navbar-transparent bootsnav on no-full">
    <!--=== Start Top Search ===-->
    <div class="fullscreen-search-overlay" id="search-overlay"> <a href="#" class="fullscreen-close" id="fullscreen-close-button"><i class="icofont icofont-close"></i></a>
      <div id="fullscreen-search-wrapper">
        <form method="get" id="fullscreen-searchform">
          <input type="text" value="" placeholder="Type and hit Enter..." id="fullscreen-search-input" class="search-bar-top">
          <i class="icofont icofont-search-1 fullscreen-search-icon">
          <input value="" type="submit">
          </i>
        </form>
      </div>
    </div>
    <!--=== End Top Search ===-->

    <div class="container">
      <!--=== Start Atribute Navigation ===-->
      <div class="attr-nav">
        
      </div>
      <!--=== End Atribute Navigation ===-->

      <!--=== Start Header Navigation ===-->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="icofont icofont-navigation-menu"></i> </button>
        <div class="logo"> <a href="<?php echo base_url();?>"> <img class="logo logo-display" src="<?php echo base_url();?>assets/uploads/new_logo.png" alt=""> <img class="logo logo-scrolled" src="<?php echo base_url();?>assets/uploads/new_logo.png" alt=""> </a> </div>
      </div>
      <!--=== End Header Navigation ===-->

      <!--=== Collect the nav links, forms, and other content for toggling ===-->
      <div class="collapse navbar-collapse" id="navbar-menu">

        <ul class="nav navbar-nav" data-in="fadeIn" data-out="fadeOut">
          <?php 
          foreach($menus as $menu)
          {

          ?>
          <li class="dropdown megamenu-fw"> <a href="<?php echo base_url().$menu['link'];?>">

            <?php echo $menu['menu_title'];?></a>
           <?php }?> 
                       </div>
                  </div>
                </div>
                <!--=== end row ===-->
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!--=== /.navbar-collapse ===-->
    </div>
  </nav>
  <!--=== Header End ===-->
