<!-- Project Section -->
<?php $notice = $this->db->select('*')->from('notice')->where('notice_type','news')->order_by('notice_date','DESC')->limit(3)->get()->result();
?>
<?php if(!empty($notice)){ ?>
  <!-- Blog Section -->
  <div class="blog-section">
    <div class="auto-container">
      <!-- Sec Title -->
      <div class="sec-title centered">
        <div class="title">our blog</div>
        <h2><span>Latest </span>From Our Press</h2>
      </div>
      <div class="inner-container">
        <div class="clearfix row g-0">
          <!-- Column -->
          <div class="column col-lg-8 col-md-12 col-sm-12">
          
            <!-- News Block -->
            <?php if(isset($notice[0])){
            $notice1 = $notice[0];
            ?>
            <div class="news-block">
              <div class="inner-box">
                <div class="clearfix">
                  <!-- Image Column -->
                  <div class="image-column col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-column">
                      <div class="image">
                        <a href="<?php echo base_url();?>blog-details/<?php echo $notice1->page_slug; ?>"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $notice1->notice_image; ?>" alt="" /></a>
                      </div>
                    </div>
                  </div>
                  <!-- Content Column -->
                  <div class="content-column col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-column">
                      <div class="arrow-one"></div>
                      <h4><a href="<?php echo base_url();?>blog-details/<?php echo $notice1->page_slug; ?>"><?php echo $notice1->title; ?></a></h4>
                      <div class="post-date"><?php echo date('jS F Y',strtotime($notice1->notice_date)); ?></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            
            <!-- News Block -->
            <?php if(isset($notice[1])){
            $notice2 = $notice[1];
            ?>
            <div class="news-block">
              <div class="inner-box">
                <div class="clearfix row g-0">
                  <!-- Content Column -->
                  <div class="content-column col-lg-6 col-md-6 col-sm-12 order-lg-1 order-2">
                    <div class="inner-column">
                      <div class="arrow-two"></div>
                      <h4><a href="<?php echo base_url();?>blog-details/<?php echo $notice2->page_slug; ?>"><?php echo $notice2->title; ?></a></h4>
                      <div class="post-date"><?php echo date('jS F Y',strtotime($notice2->notice_date)); ?></div>
                    </div>
                  </div>
                  <!-- Image Column -->
                  <div class="image-column col-lg-6 col-md-6 col-sm-12 order-lg-2 order-1">
                    <div class="inner-column">
                      <div class="image">
                        <a href="<?php echo base_url();?>blog-details/<?php echo $notice2->page_slug; ?>"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $notice2->notice_image; ?>" alt="" /></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            
          </div>
          
          <!-- Column -->
          <?php if(isset($notice[2])){
            $notice3 = $notice[2];
          ?>
          <div class="column col-lg-4 col-md-12 col-sm-12">
            <!-- News Block Two -->
            <div class="news-block-two">
              <div class="inner-box">
                <div class="image">
                  <a href="<?php echo base_url();?>blog-details/<?php echo $notice3->page_slug; ?>"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $notice3->notice_image; ?>" alt="" /></a>
                  <div class="arrow"></div>
                </div>
                <div class="lower-content">
                  <h4><a href="<?php echo base_url();?>blog-details/<?php echo $notice3->page_slug; ?>"><?php echo $notice3->title; ?></a></h4>
                  <div class="post-date"><?php echo date('jS F Y',strtotime($notice3->notice_date)); ?></div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
          
        </div>
      </div>
    </div>
  </div>
  <!-- End Blog Section -->
  <?php } ?>