<!-- Project Section -->
<?php $projects = $this->db->select('*,projects.page_slug as real_slug')->from('projects')->join('project_category','projects.project_category_id = project_category.project_category_id')->order_by('project_id','ASC')->limit(4)->get()->result();
?>
<?php if(!empty($projects)){ ?>
  <div class="project-section">
    <div class="auto-container">
    
      <!-- Sec Title -->
      <div class="sec-title">
        <div class="clearfix">
          <div class="pull-left">
            <div class="title">latest projects</div>
            <h2>See Our <span>Successful Businesses</span></h2>
          </div>
         <!--  <div class="pull-right">
            <a href="<?php //echo base_url(); ?>assets/frontend/service.html" class="cases">all Cases <span class="arrow ti-angle-right"></span></a>
          </div> -->
        </div>
      </div>
      
    </div>
    <div class="outer-container">
      <div class="row clearfix">



        <!-- Column -->
        <div class="column col-lg-6 col-md-12 col-sm-12">


          <div class="row clearfix">
            
            <!-- Inner Column -->
            <?php 
            if(isset($projects[0])){
            $project0 = $projects[0];
           ?>
            <div class="inner-column col-lg-6 col-md-6 col-sm-12">
              <div class="gallery-block">
                <div class="inner-box">
                  <div class="image">
                    <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $project0->project_image; ?>" alt="" />
                    <div class="overlay-box">
                      <div class="overlay-inner">
                        <h3><a href="<?php echo base_url(); ?>project-details/<?php echo $project0->real_slug; ?>"><?php echo $project0->project_title; ?></a></h3>
                        <div class="designation"><?php echo $project0->project_category_name; ?></div>
                        <a href="<?php echo base_url(); ?>project-details/<?php echo $project0->real_slug; ?>" class="arrow ti-angle-right"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

            <?php 
            if(isset($projects[1])){
            $project1 = $projects[1]; ?>
            <!-- Inner Column -->
            <div class="inner-column col-lg-6 col-md-6 col-sm-12">
              <div class="gallery-block">
                <div class="inner-box">
                  <div class="image">
                    <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $project1->project_image; ?>" alt="" />
                    <div class="overlay-box">
                      <div class="overlay-inner">
                        <h3><a href="<?php echo base_url(); ?>project-details/<?php echo $project1->real_slug; ?>"><?php echo $project1->project_title; ?></a></h3>
                        <div class="designation"><?php echo $project1->project_category_name; ?></div>
                        <a href="<?php echo base_url(); ?>project-details/<?php echo $project1->real_slug; ?>" class="arrow ti-angle-right"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>


            <?php 
            if(isset($projects[2])){
               $project2 = $projects[2];

             ?>


            <!-- Inner Column -->
            <div class="inner-column col-lg-12 col-md-12 col-sm-12">
              <div class="gallery-block">
                <div class="inner-box">
                  <div class="image">
                    <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $project2->project_image; ?>" alt="" />
                    <div class="overlay-box">
                      <div class="overlay-inner">
                        <h3><a href="<?php echo base_url(); ?>project-details/<?php echo $project2->real_slug; ?>"><?php echo $project2->project_title; ?></a></h3>
                        <div class="designation"><?php echo $project2->project_category_name; ?></div>
                        <a href="<?php echo base_url(); ?>project-details/<?php echo $project2->real_slug; ?>" class="arrow ti-angle-right"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>




            <?php } ?>



          </div>
        </div>
        
        <?php 
        if(isset($projects[3])){
        $project3 = $projects[3]; ?>

        <!-- Column -->
        <div class="column col-lg-6 col-md-12 col-sm-12">
          <div class="gallery-block">
            <div class="inner-box">
              <div class="image">
                <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $project3->project_image; ?>" alt="" />
                <div class="overlay-box">
                  <div class="overlay-inner">
                    <h3><a href="<?php echo base_url(); ?>project-details/<?php echo $project3->real_slug; ?>"><?php echo $project3->project_title; ?></a></h3>
                    <div class="designation"><?php echo $project3->project_category_name; ?></div>
                    <a href="<?php echo base_url(); ?>project-details/<?php echo $project3->real_slug; ?>" class="arrow ti-angle-right"></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
  <!-- End Project Section -->





<?php } ?>