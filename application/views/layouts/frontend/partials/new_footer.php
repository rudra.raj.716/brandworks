  <!--=== Footer Start ===-->
  <footer class="footer" id="footer-fixed">
    <div class="footer-main">
      <div class="container">
        <div class="row">
          <div class="col-sm-6  col-md-3 ">
            <div class="widget widget-text">
              <div class="logo logo-footer"><a href="index.html"> <img class="logo logo-display" src="<?php echo base_url();?>assets/uploads/new_logo.png" alt=""></a> </div>
              <p>We are a fully in-house digital agency focusing on branding, marketing, web design and development with clients ranging from start-ups, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius quam ut magna ultricies pellentesque.</p>
            </div>
          </div>
<!--           <div class="col-sm-6 col-md-2 "> -->
            <!-- <div class="widget widget-links">
              <h5 class="widget-title">Work With Us</h5>
              <ul>
                <li><a href="#">Themeforest</a></li>
                <li><a href="#">Audio Jungle</a></li>
                <li><a href="#">Code Canyon</a></li>
                <li><a href="#">Video Hive</a></li>
                <li><a href="#">Envato Market</a></li>
              </ul>
            </div>
          </div> -->
          <div class="col-sm-7 col-sm-offset-2 col-md-3 ">
            <div class="widget widget-links">
              <h5 class="widget-title">Useful Links</h5>
              <ul>
                <?php
                $query=$this->db->order_by('menu_id','ASC')->get('menu');
                $users=$query->result_array();
          
                foreach($users as $user)
                {
                ?>
                <li><a href="about-us.html"><?php echo $user['menu_title'];?></a></li>
              <?php }?>
              </ul>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-text widget-links">
              <h5 class="widget-title">Contact Us</h5>
              <ul>
                <li> <i class="icofont icofont-google-map"></i> <a href="#">Park Lane Llandrindod Wells, LD7 9QW</a> </li>
                <li> <i class="icofont icofont-iphone"></i> <a href="#">+44 1632 960290</a> </li>
                <li> <i class="icofont icofont-mail"></i> <a href="#">helloscoda@gmail.com</a> </li>
                <li> <i class="icofont icofont-globe"></i> <a href="#">www.envato.com</a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-xs-12">

            <ul class="social-media">
              <?php
          $query=$this->db->order_by('socialmedia_id','ASC')->get('socialmedia');
          $users=$query->result_array();
            foreach($users as $user)
            {
          ?>
              <li><a href="<?php echo $user['socialmedia_link'];?>" class="<?php echo $user['socialmedia_icon'];?>"></a></li>
                <?php } ?>
            </ul>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="copy-right text-right">© <?php echo date('Y');?> Impact. All rights reserved</div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--=== Footer End ===-->

  <!--=== GO TO TOP  ===-->
  <a href="#" id="back-to-top" title="Back to top">&uarr;</a>
  <!--=== GO TO TOP End ===-->

</div>
<!--=== Wrapper End ===-->

<!--=== Javascript Plugins ===-->
<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>
<script src="<?php echo base_url();?>assets/js/master.js"></script>
<script src="<?php echo base_url();?>assets/js/bootsnav.js"></script>
<!--=== Javascript Plugins End ===-->

<script>
  window.onload = function() {
    document.getElementById("main").classList.add("loaded")

    lax.setup()

    const update = () => {
      lax.update(window.scrollY)
      window.requestAnimationFrame(update)
    }

    window.requestAnimationFrame(update)

    let w = window.innerWidth
    window.addEventListener("resize", function() {
      if(w !== window.innerWidth) {
        lax.updateElements()
      }
    });
  }
</script>

</body>

<!-- Mirrored from www.incognitothemes.com/scoda/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2021 06:41:44 GMT -->
</html>
