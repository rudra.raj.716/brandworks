  <!-- Project Section -->
<?php $testimonials = $this->db->select('*')->from('testimonials')->get()->result();
?>

  <!-- Testimonial Section -->
  <div class="testimonial-section">
    <div class="auto-container">
      <!-- Sec Title -->
      <div class="sec-title">
        <div class="title">testimonials</div>
        <h2><span>+200 Clients </span>Love Us</h2>
      </div>
      <div class="testimonial-carousel owl-carousel owl-theme">
        
        <?php foreach($testimonials as $key => $val){ ?>
          <div class="testimonial-block">
          <div class="inner-box">
            <div class="quote icofont-quote-right"></div>
            <div class="author"><?php echo $val->testimonial_title; ?> <span>/ <?php echo $val->testimonial_by; ?></span></div>
            <div class="text"><?php echo $val->testimonial_description; ?></div>
          </div>
        </div>
        <?php } ?>
        <!-- Testimonial Block -->
      </div>
    </div>
  </div>
  <!-- End Testimonial Section -->