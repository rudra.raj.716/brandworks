<!-- Experts Section -->
<?php $experts = $this->db->select('*')->from('teams')->order_by('team_order','ASC')->limit(3)->get()->result();
?>

  <div class="experts-section">
    <div class="auto-container">
    
      <!-- Sec Title -->
      <div class="sec-title">
        <div class="clearfix">
          <div class="pull-left">
            <div class="title">our services</div>
            <h2>We Are <span>Friendly & Profressional</span></h2>
          </div>
          <div class="pull-right">
            <a href="<?php echo base_url(); ?>assets/frontend/service.html" class="experts">all experts <span class="arrow ti-angle-right"></span></a>
          </div>
        </div>
      </div>
      
      <div class="row clearfix">
        
        <!-- Team Block -->
        <?php foreach($experts as $key => $value){ ?>
        <div class="team-block col-lg-4 col-md-6 col-sm-6 col-12">
          <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
            <div class="image">
              <a><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $value->image; ?>" alt="" /></a>
              <!-- Social Box -->
              <ul class="social-box">
                <li><a href="<?php echo $value->twitter_url; ?>" class="icofont-twitter"></a></li>
                <li><a href="<?php echo $value->facebook_url; ?>" class="icofont-facebook"></a></li>
                <li><a href="<?php echo $value->instagram_url; ?>" class="icofont-linkedin"></a></li>
              </ul>
            </div>
            <div class="lower-box mt-0">
              <h4><a><?php echo $value->name; ?></a></h4>
              <div class="designation"><?php echo $value->designation; ?></div>
            </div>
          </div>
        </div>
        <?php } ?>

        
      </div>
    </div>
  </div>
  <!-- End Experts Section -->