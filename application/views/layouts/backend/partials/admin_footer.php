<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/backend/js/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/backend/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo base_url(); ?>assets/backend/js/dashboard.js"></script> -->


<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/backend/js/demo.js"></script>

<script src="<?php echo base_url(); ?>assets/backend/js/Chart.min.js"></script>

<script src="<?php echo base_url(); ?>assets/backend/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/select2.full.min.js"></script>

<script>

$(document).on("focus",".datepicker", function(){
  
    $(this).datepicker({
      autoclose:true,
      format:'yyyy-mm-dd'
    });
});

$(function()
{

  /*$('.datepicker').datepicker({
      autoclose:true,
      format:'yyyy-mm-dd'
    });*/

    $('.select2').select2();

  $('.deletesure').click(function()
  {
    var result = confirm("Are You Sure, You Want to delete?");
    if(result)
    {
      return true;
    }
    return false;
  });
})

</script>
</body>
</html>
