<?php
$current_url = $this->uri->segment(1) . '/' . $this->uri->segment(2);
$main_url = $this->uri->segment(1);

?>

<aside class="main-sidebar">

  <section class="sidebar">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <!-- <li class="header">MAIN NAVIGATION</li> -->
      <li class="<?php if ($current_url == 'dashboard/') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("/backend/dashboard"); ?>">
          <span>Dashboard</span>
        </a>
      </li>

      <li class="<?php if ($current_url == 'backend/page' || $current_url == 'backend/slider' || $current_url == 'backend/slogan' || $current_url == 'backend/socialmedia' || $current_url == 'backend/menu' || $current_url == 'auth/change_password' || $current_url == 'backend/about_us' || $current_url == 'backend/slider') {
                    echo 'active';
                  } ?>">

        <a href="#">
          <span>Settings</span>
          <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">

          <li class="<?php if ($current_url == 'backend/page') {
                        echo 'active';
                      } ?>"><a href="<?php echo base_url('backend/page'); ?>">Pages</a></li>
          <!-- <li class="<?php //if($current_url == 'backend/slider') { echo 'active'; } 
                          ?>"><a href="<?php //echo base_url('backend/slider'); 
                                        ?>">Slider</a></li> -->
          <li class="<?php if ($current_url == 'backend/menu') {
                        echo 'active';
                      } ?>"><a href="<?php echo base_url('backend/menu'); ?>">Menus</a></li>

          <li class="<?php if ($current_url == 'backend/aboutus') {
                        echo 'active';
                      } ?>"><a href="<?php echo base_url('backend/aboutus'); ?>">About Us</a></li>
          <!-- slogan
             -->

          <li class="<?php if ($current_url == 'backend/sliders') {
                        echo 'active';
                      } ?>"><a href="<?php echo base_url('backend/slider'); ?>">Sliders</a></li>

          <li class="<?php if ($current_url == 'auth/change_password') {
                        echo 'active';
                      } ?>"><a href="<?php echo base_url('auth/change_password'); ?>">Change Password</a></li>
        </ul>
      </li>

      <li class="<?php if ($current_url == 'backend/project_category') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("backend/project_category"); ?>">
          <span>Project Category</span>
        </a>
      </li>

      <li class="<?php if ($current_url == 'backend/project' || $current_url == 'backend/project_gallery') {
                    echo 'active';
                  } ?>">
        <a href="#">
          <span>Projects</span>
          <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if ($current_url == 'backend/project') {
                        echo 'active';
                      } ?> treeview">
            <a href="<?php echo base_url("/backend/project"); ?>">
              <span>Projects</span>
            </a>
          </li>

          <li class="<?php if ($current_url == 'backend/project_gallery') {
                        echo 'active';
                      } ?> treeview">
            <a href="<?php echo base_url("/backend/project_gallery"); ?>">
              <span>Gallery</span>
            </a>
          </li>
        </ul>
      </li>

      <li class="<?php if ($current_url == 'backend/clients') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("/backend/clients"); ?>">
          <span>Clients</span>
        </a>
      </li>

      <li class="<?php if ($current_url == 'backend/whatwedo') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("/backend/whatwedo"); ?>">
          <span>What We Do</span>
        </a>
      </li>

      <li class="<?php if ($current_url == 'backend/faq') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("/backend/faq"); ?>">
          <span>FAQ</span>
        </a>
      </li>

      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>