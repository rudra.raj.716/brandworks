<?php include('partials/admin_header.php'); ?>
<?php include('partials/admin_nav.php'); ?>
<?php include('partials/admin_leftnav.php'); ?>

<div class="content-wrapper" style="background: #fff">   
  <section class="content" >
      <?php if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message'); ?>
            <div class="alert alert-info">
              <p><?php echo $message; ?></p>
            </div>
      <?php }   ?>
    <?php echo $template['body']; ?>
  </section>

</div>

<footer class="main-footer">
  <div class="pull-right hidden-xs"> </div>
  
</footer>


<?php include(APPPATH.'/views/layouts/backend/partials/admin_footer.php'); ?>



