<!-- <header>
  <div class="overlay"></div>
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="<?php //echo base_url(); 
                  ?>assets/uploads/<?php //echo $settings['video_banner']; 
                                                            ?>" type="video/mp4">
  </video>
  <div class="container h-100">
    <div class="d-flex h-100 text-center align-items-center">
      <div class="w-100 text-white">
        <h1 class="display-3"><?php //echo $settings['banner1']; 
                              ?></h1>
        <p class="lead mb-0"><?php //echo $settings['banner2']; 
                              ?></p>
        <a href='<?php //echo $settings['banner1_link']; 
                  ?>'><button class='btn btn-primary avani-red'>GET A QUOTE</button></a>

      </div>
    </div>
  </div>
</header> -->



<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <?php foreach ($sliders as $key => $val) { ?>
      <?php if ($key == 0) { ?>
        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $key; ?>" class="active"></li>
      <?php } else { ?>
        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $key; ?>"></li>
      <?php } ?>
    <?php } ?>
  </ol>
  <div class="carousel-inner">

    <?php foreach ($sliders as $key => $val) { ?>
      <?php if ($key == 0) { ?>
        <div class="carousel-item active">
          <img class="d-block w-100" src="<?php echo base_url(); ?>assets/uploads/<?php echo $val['slider_image']; ?>" alt="<?php echo $val['slider_img_alt']; ?>">
          <div class="carousel-caption d-none d-md-block">
            <!-- <h5>Lorem ipsum dolor sit amet.</h5>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iusto, cupiditate.</p> -->
          </div>
        </div>
      <?php } else { ?>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo base_url(); ?>assets/uploads/<?php echo $val['slider_image']; ?>" alt="<?php echo $val['slider_img_alt']; ?>">

          <div class="carousel-caption d-none d-md-block">
            <!-- <h5>Lorem ipsum dolor sit amet.</h5>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit, magnam?</p> -->
          </div>


        </div>
      <?php } ?>
    <?php } ?>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>



<div class="aboutus_area">
  <div class="container">
    <div class="row">

      <div class="col-md-6 col-sm-8 col-xs-12">
        <div class='content_area'>
          <h1><?php echo $aboutus['aboutus_title']; ?></h1>
          <p><?php echo $aboutus['aboutus_description']; ?>
          </p>
        </div>
      </div>

      <div class="col-md-6 col-sm-4 col-xs-12">
        <div class='head_area'>
          <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $aboutus['aboutus_image']; ?>" alt='<?php echo $aboutus['aboutus_image_alt']; ?>'>

        </div>
      </div>
    </div>
  </div>
</div>


<div class="service_area">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class='main_title'>Our Services</h1>
      </div>
      <?php
      foreach ($services as $service) {
      ?>
        <div class="col-md-4">
          <div class='service_block'>
            <!-- <i class="<?php //echo $service['service_icon']; 
                            ?>"></i> -->
            <img src = "<?php echo base_url().'assets/uploads/'.$service['service_image']; ?>" alt = '<?php echo $service['service_image_alt']; ?>'>
            <h4><?php echo $service['service_title']; ?></h4>
            <p><?php echo $service['service_description']; ?></p>
          </div>
        </div>

      <?php } ?>


    </div>
  </div>
</div>



<div class="what_we_do_area">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class='main_title'>What We Do</h1>
      </div>
      <?php
      foreach ($whatwedo as $wha) {
      ?>
        <div class="col-md-3">
          <div class="whatwedoblock">
            <img src="<?php echo base_url() ?>assets/uploads/<?php echo $wha['whatwe_do_image']; ?>" alt="<?php echo $wha['whatwedo_image_alt']; ?>" />
            <!-- <img src = "img/avanieye.png" style=""> -->
            <h4><?php echo $wha['whatwedo_title']; ?></h4>
            <p><?php echo $wha['whatwedo_description']; ?></p>
          </div>
        </div>
      <?php } ?>


    </div>
  </div>
</div>

<div class="tvc_area white">
  <div class="container">
    <div class="col-md-12">
      <h1 class='main_title'>Projects</h1>
    </div>

    <div class="row">

        <?php foreach($adv_type as $key => $val){ ?>
          <div class="col-md-6 text-center">
        <div class="tvcblock">
          <div class="img-box">
            <a href='<?php echo base_url(); ?>projects/<?php echo $val['page_slug']; ?>'><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $val['advertisement_type_image']; ?>" alt="<?php echo $val['advertiment_type_featured_image_alt']; ?>" width="100%"></a>
          </div>
          <div class="text-box">
            <a href='<?php echo base_url(); ?>projects/<?php echo $val['page_slug']; ?>'>
              <h1 class="overlay font-weight-bolder"><?php echo $val['advertisement_type_name']; ?></h1>
            </a>
          </div>
        </div>
      </div>
        <?php } ?>
        

    </div>

  </div>
</div>



<div class="awards_area gray">
  <div class="container">
    <div class="col-md-12">
      <h1 class='main_title'>Awards</h1>
    </div>

    <div class="owl-carousel owl-theme">
            <?php foreach ($awards as $award) { ?>
              <div class="item"> <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $award['award_image']; ?>" alt="<?php echo $award['award_image_alt']; ?>">
              <h5 style = 'text-align:center; color:#504141; font-weight:bold; margin-top:10px;'>Test H3</h5>
            </div>
            <?php } ?>
          </div>
    </div>
</div>



<div class="testimonial3 py-5 white">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 text-center">
        <h4 class="mb-3">Testimonials</h4>

      </div>
    </div>
    <!-- Row  -->
    <div class="owl-carousel owl-theme testi3 mt-4">
      <!-- item -->

      <?php
      foreach ($testimonials as $key => $val) { ?>
        <div class="item">
          <div class="card card-shadow border-0 mb-4">
            <div class="card-body">
              <h6 class="font-weight-light mb-3">“<?php echo $val['testimonial_description']; ?>”</h6>
              <div class="d-block d-md-flex align-items-center">
                <span class="thumb-img"><img src="<?php echo base_url(); ?>/assets/uploads/<?php echo $val['testimonial_image']; ?>" alt="<?php echo $val['testimonial_alt_image']; ?>" class="rounded-circle" /></span>
                <div class="ml-3">
                  <h6 class="mb-0 customer"><?php echo $val['testimonial_title']; ?></h6>
                  <div class="font-10">
                    <a href="" class="text-success"><i class="icon-star"></i></a>
                    <a href="" class="text-success"><i class="icon-star"></i></a>
                    <a href="" class="text-success"><i class="icon-star"></i></a>
                    <a href="" class="text-success"><i class="icon-star"></i></a>
                    <a href="" class="text-muted"><i class="icon-star"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>



      <!-- item -->
      <!-- item -->

      <!-- item -->
    </div>
  </div>
</div>




<div class="clients_area gray">
  <div class="container">
    <div class="col-md-12">
      <h1 class='main_title'>Clients</h1>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="client_inner_area">





          <div class="owl-carousel owl-theme">
            <?php foreach ($clients as $client) { ?>

              <div class="item"> <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $client['client_image']; ?>" alt="<?php echo $client['client_image_alt']; ?>"></div>

            <?php } ?>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>





<div class="faq_area white">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <h1 class='main_title'>Frequently Asked Questions</h1>
      </div>



      <div class="col-md-6">
        <div id="accordion">
          <?php foreach ($faq as $k => $v) { ?>
            <div class="card">
              <div class="card-header" id="<?php echo $v['faq_id']; ?>">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $v['faq_id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $v['faq_id']; ?>">
                    <div>
                      <h4><?php echo $v['faq_title']; ?> <span class='float-right'><i class="fa fa-angle-down"></i></span></h4>
                    </div>
                  </button>
                </h5>
              </div>

              <div id="collapse<?php echo $v['faq_id']; ?>" class="collapse" aria-labelledby="<?php echo $v['faq_id']; ?>" data-parent="#accordion">
                <div class="card-body">
                  <?php echo $v['faq_description']; ?>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>

      <div class="col-md-6">
        <div id="accordion">
          <?php foreach ($faq as $k => $v) { ?>
            <div class="card">
              <div class="card-header" id="<?php echo $v['faq_id']; ?>1">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $v['faq_id']; ?>1" aria-expanded="true" aria-controls="collapse<?php echo $v['faq_id']; ?>">
                    <div>
                      <h4><?php echo $v['faq_title']; ?> <span class='float-right'><i class="fa fa-angle-down"></i></span></h4>
                    </div>
                  </button>
                </h5>
              </div>

              <div id="collapse<?php echo $v['faq_id']; ?>1" class="collapse" aria-labelledby="<?php echo $v['faq_id']; ?>" data-parent="#accordion">
                <div class="card-body">
                  <?php echo $v['faq_description']; ?>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>




<!-- <div class="awards_area">
  <div class="container">
    <div class="col-md-12">
      <h1 class='main_title'>Awards</h1>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="client_inner_area">
          <div class="owl-carousel owl-theme">
            <?php //foreach ($awards as $award) { 
            ?>

              <div class="item"> <img src="<?php //echo base_url(); 
                                            ?>assets/uploads/<?php //echo $award['award_image']; 
                                                                                      ?>"></div>

            <?php //} 
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->