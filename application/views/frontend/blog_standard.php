<?php   


 ?>
  <!--=== page-title-section start ===-->
  <section class="title-hero-bg blog-cover-bg" data-stellar-background-ratio="0.2">
    <div class="container">
      <div class="page-title text-center">
        <h1>Notice</h1>
        <h4 class="text-uppercase mt-30 white-color">Checkout Our Latest Posts</h4>
      </div>
    </div>
  </section>
  <!--=== page-title-section end ===-->

  <!--=== Blogs Start ======-->
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="post">
            <div class="post-img"> <img class="img-responsive" src="<?php echo base_url();?>assets/uploads/<?php echo $users['notice_image'];?>" alt=""/> </div>
            <div class="post-info">
              <h3><a href="blog-grid.html"><?php echo $users['title'];?></a></h3>
              <h6><?php echo $users['notice_date'];?></h6>
              <p><?php echo $users['notice_intro'];?></p>
              <a class="readmore white-color" href="#"><span>Read More</span> <i class="icofont icofont-simple-right"></i></a> </div>
          </div>
          <div class="blog-standard">
            <p><?php echo $users['description'];?></p>
            <blockquote>
              <p>Donec dui ipsum, pulvinar sit amet mattis quis, dapibus eu libero. Curabitur lobortis, diam non semper placerat, ipsum felis pulvinar magna, quis dapibus enim velit vel lorem.</p>
            </blockquote>
          </div>
          <div class="post-tags"> <a href="#">Design</a> <a href="#">Branding</a> <a href="#">Stationery</a> <a href="#">Development</a> <a href="#">Concept</a> </div>
          <div class="post-controls">
            <div class="post-share">
              <ul>
                <li> Share: </li>
                <li> <a href="#"><i class="icofont icofont-facebook"></i></a> </li>
                <li> <a href="#"><i class="icofont icofont-twitter"></i></a> </li>
                <li> <a href="#"><i class="icofont icofont-linkedin"></i></a> </li>
              </ul>
            </div>
            <div class="comments-info"> <a href="#"> <i class="icofont icofont-comment"></i> 0</a> </div>
          </div>
          <div id="respond" class="comment-respond">
            <h2 id="reply-title" class="comment-reply-title">Post a comment</h2>
            <form method="post" id="form-comments" class="comment-form">
              <div class="form-group">
                <textarea name="comment" id="comment-field" class="form-control" placeholder="Comment" rows="5"></textarea>
              </div>
              <div class="row-form row">
                <div class="col-form col-md-6">
                  <div class="form-group">
                    <input name="author" type="text" class="form-control" placeholder="Name">
                  </div>
                </div>
                <div class="col-form col-md-6">
                  <div class="form-group">
                    <input name="email" type="text" class="form-control" placeholder="Email">
                  </div>
                </div>
              </div>
              <p class="form-submit">
                <input name="submit" type="submit" id="submit-btn" class="btn btn-color btn-square" value="Post Comment">
                <input type="hidden" name="comment_post_ID">
                <input type="hidden" name="comment_parent" id="comment_parent" value="0">
              </p>
            </form>
          </div>
        </div>
        <!--=== Left Side End===-->
          
          
        </div>
        <!--=== Right Side End ===-->
      </div>
    </div>
  </section>
  <!--=== Blogs End ======-->

 