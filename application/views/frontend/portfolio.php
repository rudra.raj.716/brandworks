  <!--=== Header End ======-->
  
  <!--=== page-title-section start ===-->
  <section class="title-hero-bg portfolio-cover-bg" data-stellar-background-ratio="0.2">
    <div class="container">
      <div class="page-title text-center">
        <h1>Portfolio</h1>
        <h4 class="text-uppercase mt-30 white-color">Our Recent Works</h4>
      </div>
    </div>
  </section>
  <!--=== page-title-section end ===-->
  
  <!--=== Portfolio Start ======-->
  <section class="pt-100 pt-100">
      
    <div class="container">
      
      <div class="row">
        
        <div class="portfolio-container text-center">
          <!-- <ul id="portfolio-filter" class="list-inline filter-transparent">
            <li class="active" data-group="all">All</li>
            <li data-group="design">Design</li>
            <li data-group="web">Web</li>
            <li data-group="branding">Branding</li>
            <li data-group="print">Print</li>
          </ul> -->
          
          <ul id="portfolio-grid" class="three-column hover-two">
            <?php
          $query=$this->db->order_by('project_id','ASC')->get('projects');
          $users=$query->result_array();
          
          foreach($users as $user)
          {
          ?>
            <li class="portfolio-item" data-groups='["all", "print", "branding"]'>
              <div class="portfolio">
                <div class="dark-overlay"></div>
                <img src="<?php echo base_url();?>assets/uploads/<?php echo $user['project_image'];?>" alt="">
                <div class="portfolio-wrap">
                  <div class="portfolio-description">
                    <h3 class="portfolio-title"><?php echo $user['project_title'];?></h3>
                    <!-- <a href="single-portfolio.html" class="links">Print Design</a> </div> -->
                  <!--=== /.project-info ===-->
                  <ul class="portfolio-details">
                     <li><a class="alpha-lightbox" href="<?php echo base_url();?>assets/images/portfolio/grid/1.jpg"><i class="icofont icofont-search-1"></i></a></li>
                    <li><a href="<?php echo $user['project_link'];?>"><i class="icofont icofont-link-alt"></i></a></li>
                  </ul>
                </div>
              </div>
              <!--=== /.portfolio ===-->
            </li>
          <?php }?>   
              <!--=== /.portfolio ===-->
            </li>
         
          </ul>
          
        </div>
    
      </div>
  <!--     <div class="row mt-100">
        <p class="text-center"><a class="btn btn-color btn-circle">Start a Project</a></p>
      </div>
    </div> -->

  </section>
  <!--=== Portfolio End ======-->
  
  
  
  <!--=== Footer Start ======-->
  

<!-- Mirrored from www.incognitothemes.com/scoda/box-3-columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2021 07:06:58 GMT -->
</html>