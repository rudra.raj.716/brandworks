
	<!-- Service Banner Section -->
    <div class="service-banner-section" data-bg-image="<?php echo base_url(); ?>assets/uploads/<?php echo $page['cover_image'];?>">
		<div class="auto-container">
			<div class="content-box">
				<h2><?php echo $page['page_subtitle']; ?></h2>
				<div class="text"><?php echo $page['page_intro']; ?></div>
				<a href="<?php echo base_url().'home/contact'; ?>" class="theme-btn btn-style-one"><span class="txt">get in touch</span></a>
				
				<!-- Lower Box -->
				<div class="lower-box clearfix">
					<div class="pull-left">
						<div class="book">
							<span class="icon icofont-phone"></span>
							book through <br> call now
						</div>
					</div>
					<div class="pull-right">
						<a class="phone" href="tel:+977-15553222">+977-15553222, 9801200100/101</a>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>
	<!-- End Service Banner Section -->
	
<?php include(APPPATH.'views/layouts/frontend/partials/servicesection.php'); ?>
<?php include(APPPATH.'views/layouts/frontend/partials/testimonialsection.php'); ?>
<?php include(APPPATH.'views/layouts/frontend/partials/partnersection.php'); ?>
	

	<script>
$(function()
{
	//$('.main-header').addClass('style-two');
	$('.main-footer').addClass('style-two');
});
</script>