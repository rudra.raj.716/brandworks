<div class="banner-text-left lernen_banner bg-services" style="padding: 80px 0 80px;
    background-position: top center!important; background: linear-gradient(rgba(0, 0, 0, .6), rgba(0, 0, 0, .2)), url(<?php echo base_url(); ?>assets/img/avanibanner.jpg); background-size: cover!important; text-align: center; position: relative; ">
  <div class="container">
    <div class="row">
      <div class='col-md-12'>
        <div class="lernen_banner_title" style="display: block; width: 100%;">
          <h1 style='text-align:center; color:#fff; display:block; '><?php echo $case['case_title_1']; ?></h1>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="case_study_area">
  <div class="container">
    <div class="row">
      <div class='col-md-6'>
        <p><?php echo $case['case_description']; ?></p>
      </div>
      <div class='col-md-6'>
        <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $case['cover_image']; ?>" alt = "<?php echo $case['cover_image_alt'];?>"/>
      </div>
    </div>
  </div>
</div>

<?php if (!empty($case_extra)) { ?>
  <div class="case_single_extra">
    <div class="container">
      <div class="row">
        <?php
        foreach ($case_extra as $cases) {
        ?>
          <div class="col-md-4">
            <div class='service_block'>
              <i class="<?php echo $cases['case_extra_icon']; ?>"></i>
              <!-- <img src = "img/avanieye.png" style=""> -->
              <h4><?php echo $cases['case_extra_number']; ?></h4>
              <p><?php echo $cases['case_extra_desc']; ?></p>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
<?php } ?>

<div class="title_area gray">
  <div class="container">
    <div class="row">
      <div class='col-md-6'>
      <h2 class='casestudysubheader'><?php echo $case['the_client']; ?></h2>
       <?php echo $case['the_client_description']; ?>
      </div>
      <div class='col-md-6'>
        <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $case['the_client_image']; ?>" alt = "<?php echo $case['the_client_image_alt']; ?>"/>
      </div>
    </div>
  </div>
</div>


<div class="title_area white">
  <div class="container">
    <div class="row">
      <div class='col-md-6'>
        <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $case['the_challenge_image']; ?>" alt="<?php echo $case['the_challenge_image_alt']; ?>"/>
        </div>
      <div class='col-md-6'>
      <h2 class='casestudysubheader'><?php echo $case['the_challenge']; ?></h2>
       <?php echo $case['the_challenge_desc']; ?>
      </div>
    </div>
  </div>
</div>


<div class="title_area gray">
  <div class="container">
    <div class="row">
     
      <div class='col-md-6'>
      <h2 class='casestudysubheader'><?php echo $case['the_strategy']; ?></h2>
      <?php echo $case['the_strategy_desc']; ?>
      </div>

      <div class='col-md-6'>
        <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $case['the_strategy_image']; ?>" alt = "<?php echo $case['the_strategy_image_alt']; ?>"/>
      </div>
    </div>
  </div>
</div>



<div class="title_area white">
  <div class="container">

    <div class="row">
      <div class='col-md-6'>
        <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $case['the_results_image']; ?>" alt = "<?php echo $case['the_results_image_alt']; ?>"/>
      </div>

      <div class='col-md-6'>
      <h2 class='casestudysubheader'><?php echo $case['the_results']; ?></h2>
      <?php echo $case['the_results_desc']; ?>
      </div>
    </div>

  </div>
</div>



<div class="testimonial3 py-5 bg-light">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 text-center">
        <h4 class="mb-3">Further Case Studies</h4>

      </div>
    </div>
    <!-- Row  -->
    <div class="owl-carousel owl-theme testi3 mt-4">
      <!-- item -->

      <?php
      foreach ($casestudy as $key => $val) { ?>
        <div class="item">
          <div class="card card-shadow border-0 mb-4">
            <div class="card-body1">
              <a href = '<?php echo base_url();?>case/<?php echo $val['page_slug']; ?>'><img src="<?php echo base_url() . 'assets/uploads/' . $val['cover_image']; ?>" alt = "<?php echo $val['cover_image_alt']; ?>"/></a>
            </div>
          </div>
        </div>
      <?php } ?>



      <!-- item -->
      <!-- item -->

      <!-- item -->
    </div>
  </div>
</div>