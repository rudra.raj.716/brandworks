
  <!--=== page-title-section start ===-->
  <section class="title-hero-bg widget-cover-bg" data-stellar-background-ratio="0.2">
    <div class="container">
      <div class="page-title text-center">
        <h1>Privacy Policy</h1>
      </div>
    </div>
  </section>
  <!--=== page-title-section end ===-->

  <!--=== Terms &amp; Conditions Start ===-->
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 terms">
          <h2 class="font-700">Privacy Policy</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eget nisl gravida, <a href="#"><span class="default-color">Privacy Policy</span></a> faucibus ligula. Nam eu neque nunc. Suspendisse egestas dolor ante, nec tincidunt sem malesuada at. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi vehicula metus sit amet turpis malesuada commodo.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="#"><span class="default-color">Privacy Policy</span></a> Morbi eget nisl gravida, interdum nunc quis, faucibus ligula.</p>
          <p>Praesent tincidunt, massa et porttitor imperdiet, lorem velit posuere erat, <a href="#"><span class="default-color">Privacy Policy</span></a> sit amet gravida odio magna in ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eget nisl gravida, interdum nunc quis, faucibus ligula. Nam eu neque nunc. Suspendisse egestas dolor ante, nec tincidunt sem malesuada at. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi vehicula metus sit amet turpis malesuada commodo.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eget nisl gravida, interdum nunc quis, faucibus ligula.</p>
          <p>Praesent tincidunt, massa et porttitor imperdiet, lorem velit posuere erat, sit amet gravida odio magna in ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <p>Vivamus massa urna, pharetra eget malesuada eu, pharetra vitae arcu. Duis dapibus aliquam ante, ut posuere odio commodo sed. Pellentesque non hendrerit purus. Morbi tellus mauris, convallis non dictum non, elementum eget leo. Donec non ex est. Pellentesque magna urna, maximus ac augue id, pretium lobortis tortor. </p>
          <p>Nam porttitor, mauris sit amet molestie dictum, ex velit viverra urna, eget euismod justo augue in eros. Duis ut erat ac erat scelerisque pulvinar in ut neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <p>Nunc a tortor vitae nisl pharetra consectetur in eu purus. Pellentesque facilisis nunc mauris. Etiam finibus, lacus eu fringilla mattis, mi lectus consequat augue, in pharetra neque lacus sit amet velit. Donec imperdiet lacinia ullamcorper. </p>
          <p>Ut consectetur risus ligula, quis feugiat enim malesuada ac. Nullam dapibus blandit maximus. Donec a orci dui. Vivamus porta varius metus in tempor.</p>
        </div>
      </div>
    </div>
  </section>
  <!--=== Terms &amp; Conditions End ===-->

