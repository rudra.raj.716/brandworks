	<?php //pr($products,1); ?>
	<!-- Page Title Section -->
    <div class="page-title-section">
    	<div class="auto-container">
			<ul class="post-meta">
				<li><a href="<?php echo base_url(); ?>">Index</a></li>
				<li>Product</li>
			</ul>
			<h2><span>Latest</span> From <?php echo (!empty($category))? $category['product_category_name'] : 'Our Product'; ?></h2>
		</div>
	</div>
	<!-- End Page Title Section -->

	<!-- Project Section -->
	<div class="project-section section-padding">
		<div class="outer-container">
			<div class="row clearfix">

				<!-- Column -->
				<div class="column col-lg-12 col-md-12 col-sm-12">
					<div class="row clearfix">
						
						<?php 
						
						foreach($products as $product) { ?>
						<!-- Inner Column -->

						<div class="inner-column col-lg-4 col-md-6 col-sm-12">
							<div class="gallery-block">
								<div class="inner-box">
									<div class="image">
										<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $product->featured_image; ?>" alt="" />
										<div class="overlay-box">
											<div class="overlay-inner">
												<h3><a href="<?php echo base_url(); ?>product-details/<?php echo $product->page_slug ?>"><?php echo $product->product_name; ?></a></h3>
												<div class="designation">Finance, Consulting</div>
												<a href="<?php echo base_url(); ?>product-details/<?php echo $product->page_slug; ?>" class="arrow ti-angle-right"></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<?php } ?>
						
					
							
					</div>
				</div>
				
				
				
			</div>
		</div>
	</div>
	<!-- End Project Section -->

<script>
$(function()
{
	$('.main-header').addClass('style-three');
	$('.main-footer').addClass('style-two');
});
</script>