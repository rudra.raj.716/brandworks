<div class="banner-text-left lernen_banner bg-services" style="padding: 80px 0 80px;
    background-position: top center!important; background: linear-gradient(rgba(0, 0, 0, .6), rgba(0, 0, 0, .2)), url(<?php echo base_url(); ?>assets/img/avanibanner.jpg); background-size: cover!important; text-align: center; position: relative; ">
  <div class="container">
    <div class="row">
      <div class='col-md-12'>
        <div class="lernen_banner_title" style="display: block; width: 100%;">
          <h1 style='text-align:center; color:#fff; display:block; '>Contact Us</h1>
        </div>
      </div>
    </div>
  </div>

</div>



<div class="awards_area">
  <div class="container">

    <div class="row">
      <div class="col-md-8">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" role="alert">
            <?php echo $this->session->flashdata('error'); ?>

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
          </div>
          <?php } ?>

          <?php if ($this->session->flashdata('msg')) { ?>
          <div class="alert alert-success" role="alert">
            <?php echo $this->session->flashdata('msg'); ?>

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
          </div>
          <?php } ?>

          <form method="POST" action="<?= base_url() . 'Contact' ?>">
            <div class="messages"></div>
            <div class="form-group">
              <label class="sr-only" for="name">Name</label>
              <input type="text" name="name" class="form-control" id="name" required="required" placeholder="Your Name" data-error="Your Name is Required">
              <div class="help-block with-errors mt-20"></div>
            </div>
            <div class="form-group">
              <label class="sr-only" for="email">Email</label>
              <input type="email" name="email" class="form-control" id="email" placeholder="Your Email" required="required" data-error="Please Enter Valid Email">
              <div class="help-block with-errors mt-20"></div>
            </div>
            <div class="form-group">
              <label class="sr-only" for="subject">Subject</label>
              <input type="text" name="subject" class="form-control" id="subject" placeholder="Your Subject">
            </div>
            <div class="form-group">
              <label class="sr-only" for="message">Message</label>
              <textarea name="message" class="form-control" id="message" rows="7" placeholder="Your Message" required data-error="Please, Leave us a message"></textarea>
              <div class="help-block with-errors mt-20"></div>
            </div>
            <button type="submit" name="submit" class="btn avani-red" style='margin-top:0px;'>Send Mail</button>
          </form>
      </div>
      <div class="col-md-4 sidelocation">
        <h3 class="white-color">Find Us At</h3>
        <address>
          Kupondole, Lalitpur, Nepal<br>
          <abbr title="Phone">P:</abbr> 00977-1-5012014, 5546043<br>
          <a href="mailto:#">info@avani.com.np</a>
        </address>

        <h3 class="white-color">Work Timing</h3>
        <p> <span>Sun - Fri: 10am to 6pm</span> <br>
         
        </p>
      </div>
    </div>


  </div>
</div>

<div class="mapouter">
  <div class="gmap_canvas">
    <iframe width="100%" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=Avani%20Advertising%20Pvt.%20Ltd.,%20Kupondole%20Rd,%20Kathmandu&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
    </iframe>
  </div>
</div>

<style>

</style>