
  
  <!--=== page-title-section start ===-->
  <section class="title-hero-bg portfolio-cover-bg" data-stellar-background-ratio="0.2">
    <div class="container">
      <div class="page-title text-center">
        <h1>Boxed 4 Columns</h1>
        <h4 class="text-uppercase mt-30 white-color">Our Recent Works</h4>
      </div>
    </div>
  </section>
  <!--=== page-title-section end ===-->
  
  <!--=== Portfolio Start ======-->
  <section class="pt-100 pt-100">
    <div class="container">
      <div class="row">
        <div class="portfolio-container text-center">
          <ul id="portfolio-filter" class="list-inline filter-transparent">
            <li class="active" data-group="all">All</li>
            <li data-group="design">Design</li>
            <li data-group="web">Web</li>
            <li data-group="branding">Branding</li>
            <li data-group="print">Print</li>
          </ul>
          <ul id="portfolio-grid" class="four-column hover-two">
            <li class="portfolio-item" data-groups='["all", "print", "branding"]'>
              <div class="portfolio">
                <div class="dark-overlay"></div>
                <img src="<?php echo base_url();?>assets/images/portfolio/grid/1.jpg" alt="">
                <div class="portfolio-wrap">
                  <div class="portfolio-description">
                    <h3 class="portfolio-title">Business Cards</h3>
                    <a href="single-portfolio.html" class="links">Print Design</a> </div>
                  <!--=== /.project-info ===-->
                  <ul class="portfolio-details">
                    <li><a class="alpha-lightbox" href="<?php echo base_url();?>assets/images/portfolio/grid/1.jpg"><i class="icofont icofont-search-1"></i></a></li>
                    <li><a href="single-portfolio.html"><i class="icofont icofont-link-alt"></i></a></li>
                  </ul>
                </div>
              </div>
              <!--=== /.portfolio ===-->
            </li>
            <li class="portfolio-item" data-groups='["all", "print", "branding"]'>
              <div class="portfolio">
                <div class="dark-overlay"></div>
                <img src="<?php echo base_url();?>assets/images/portfolio/grid/2.jpg" alt="">
                <div class="portfolio-wrap">
                  <div class="portfolio-description">
                    <h3 class="portfolio-title">Magazine</h3>
                    <a href="single-portfolio.html" class="links">Branding</a> </div>
                  <!--=== /.project-info ===-->
                  <ul class="portfolio-details">
                    <li><a class="alpha-lightbox" href="<?php echo base_url();?>assets/images/portfolio/grid/2.jpg"><i class="icofont icofont-search-1"></i></a></li>
                    <li><a href="single-portfolio.html"><i class="icofont icofont-link-alt"></i></a></li>
                  </ul>
                </div>
              </div>
              <!--=== /.portfolio ===-->
            </li>
            <li class="portfolio-item" data-groups='["all", "branding"]'>
              <div class="portfolio">
                <div class="dark-overlay"></div>
                <img src="<?php echo base_url();?>assets/images/portfolio/grid/3.jpg" alt="">
                <div class="portfolio-wrap">
                  <div class="portfolio-description">
                    <h3 class="portfolio-title">Rabycad CD Design</h3>
                    <a href="single-portfolio.html" class="links">Branding</a> </div>
                  <!--=== /.project-info ===-->
                  <ul class="portfolio-details">
                    <li><a class="alpha-lightbox" href="<?php echo base_url();?>assets/images/portfolio/grid/3.jpg"><i class="icofont icofont-search-1"></i></a></li>
                    <li><a href="single-portfolio.html"><i class="icofont icofont-link-alt"></i></a></li>
                  </ul>
                </div>
              </div>
              <!--=== /.portfolio ===-->
            </li>
            <li class="portfolio-item" data-groups='["all", "print", "design"]'>
              <div class="portfolio">
                <div class="dark-overlay"></div>
                <img src="<?php echo base_url();?>assets/images/portfolio/grid/4.jpg" alt="">
                <div class="portfolio-wrap">
                  <div class="portfolio-description">
                    <h3 class="portfolio-title">Micro Chips</h3>
                    <a href="single-portfolio.html" class="links">Web Design</a> </div>
                  <!--=== /.project-info ===-->
                  <ul class="portfolio-details">
                    <li><a class="alpha-lightbox" href="<?php echo base_url();?>assets/images/portfolio/grid/4.jpg"><i class="icofont icofont-search-1"></i></a></li>
                    <li><a href="single-portfolio.html"><i class="icofont icofont-link-alt"></i></a></li>
                  </ul>
                </div>
              </div>
              <!--=== /.portfolio ===-->
            </li>
            <li class="portfolio-item" data-groups='["all", "print", "web"]'>
              <div class="portfolio">
                <div class="dark-overlay"></div>
                <img src="<?php echo base_url();?>assets/images/portfolio/grid/5.jpg" alt="">
                <div class="portfolio-wrap">
                  <div class="portfolio-description">
                    <h3 class="portfolio-title">Flyer</h3>
                    <a href="single-portfolio.html" class="links">Print Design</a> </div>
                  <!--=== /.project-info ===-->
                  <ul class="portfolio-details">
                    <li><a class="alpha-lightbox" href="<?php echo base_url();?>assets/images/portfolio/grid/5.jpg"><i class="icofont icofont-search-1"></i></a></li>
                    <li><a href="single-portfolio.html"><i class="icofont icofont-link-alt"></i></a></li>
                  </ul>
                </div>
              </div>
              <!--=== /.portfolio ===-->
            </li>
            <li class="portfolio-item" data-groups='["all", "design", "web"]'>
              <div class="portfolio">
                <div class="dark-overlay"></div>
                <img src="<?php echo base_url();?>assets/images/portfolio/grid/6.jpg" alt="">
                <div class="portfolio-wrap">
                  <div class="portfolio-description">
                    <h3 class="portfolio-title">Flat Web Design</h3>
                    <a href="single-portfolio.html" class="links">Web Design</a> </div>
                  <!--=== /.project-info ===-->
                  <ul class="portfolio-details">
                    <li><a class="alpha-lightbox" href="<?php echo base_url();?>assets/images/portfolio/grid/6.jpg"><i class="icofont icofont-search-1"></i></a></li>
                    <li><a href="single-portfolio.html"><i class="icofont icofont-link-alt"></i></a></li>
                  </ul>
                </div>
              </div>
              <!--=== /.portfolio ===-->
            </li>
            <li class="portfolio-item" data-groups='["all", "web", "print"]'>
              <div class="portfolio">
                <div class="dark-overlay"></div>
                <img src="<?php echo base_url();?>assets/images/portfolio/grid/7.jpg" alt="">
                <div class="portfolio-wrap">
                  <div class="portfolio-description">
                    <h3 class="portfolio-title">Flyer Design</h3>
                    <a href="single-portfolio.html" class="links">Print Design</a> </div>
                  <!--=== /.project-info ===-->
                  <ul class="portfolio-details">
                    <li><a class="alpha-lightbox" href="<?php echo base_url();?>assets/images/portfolio/grid/7.jpg"><i class="icofont icofont-search-1"></i></a></li>
                    <li><a href="single-portfolio.html"><i class="icofont icofont-link-alt"></i></a></li>
                  </ul>
                </div>
              </div>
              <!--=== /.portfolio ===-->
            </li>
            <li class="portfolio-item" data-groups='["all", "branding"]'>
              <div class="portfolio">
                <div class="dark-overlay"></div>
                <img src="<?php echo base_url();?>assets/images/portfolio/grid/8.jpg" alt="">
                <div class="portfolio-wrap">
                  <div class="portfolio-description">
                    <h3 class="portfolio-title">Card Mocks</h3>
                    <a href="single-portfolio.html" class="links">Stationery Design</a> </div>
                  <!--=== /.project-info ===-->
                  <ul class="portfolio-details">
                    <li><a class="alpha-lightbox" href="<?php echo base_url();?>assets/images/portfolio/grid/8.jpg"><i class="icofont icofont-search-1"></i></a></li>
                    <li><a href="single-portfolio.html"><i class="icofont icofont-link-alt"></i></a></li>
                  </ul>
                </div>
              </div>
              <!--=== /.portfolio ===-->
            </li>
          </ul>
        </div>
      </div>
      <div class="row mt-100">
        <p class="text-center"><a class="btn btn-color btn-circle">Start a Project</a></p>
      </div>
    </div>
  </section>
  <!--=== Portfolio End ======-->
  
  <!--=== Clients Start ======-->
  <section class="pt-50 pb-50 white-bg">
    <div class="container">
      <div class="row">
        <div id="client-slider" class="owl-carousel">
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/1.png" alt="01"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/2.png" alt="02"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/3.png" alt="03"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/4.png" alt="04"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/5.png" alt="05"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/6.png" alt="06"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/7.png" alt="07"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/8.png" alt="08"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/9.png" alt="09"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>/images/clients/10.png" alt="10"/> </div>
        </div>
      </div>
    </div>
  </section>
 