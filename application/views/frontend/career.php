

  <!--=== page-title-section start ===-->
  <section class="title-hero-bg widget-cover-bg" data-stellar-background-ratio="0.2">
    <div class="container">
      <div class="page-title text-center">
        <h1>Careers</h1>
      </div>
    </div>
  </section>
  <!--=== page-title-section end ===-->

  <!--=== Careers Start ===-->
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 terms">
          <h2 class="font-700">Currently Opening</h2>
          <ul class="careers-listing">
             <?php
          $query=$this->db->order_by('job_id','ASC')->get('jobs');
          $users=$query->result_array();
          
          foreach($users as $user)
          {
          ?>
            <li>
              <div class="row">
                <div class="career-main col-md-6">
                  <h5 class="font-700"><?php echo $user['job_title'];?></h5>
                  <span class="default-color career-type">Part time</span> <span class="career-location"><i class="icofont icofont-google-map-circle"></i>Tokyo, Japan</span> </div>
                <div class="col-md-3 career-category"><span>Career Opportunities</span></div>
                <div class="career-details col-md-3"> <a href="<?=base_url().'scoda/apply/'.$user['job_id']?>" class="btn btn-color btn-square btn-animate"><span>Apply Now <i class="icofont icofont-simple-right"></i></span></a> </div>
              </div>
            </li>
            <?php }?>
          </ul>
          <h2 class="mt-80 font-700">Job Description</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eget nisl gravida, interdum nunc quis, faucibus ligula. Nam eu neque nunc. Suspendisse egestas dolor ante, nec tincidunt sem malesuada at. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi vehicula metus sit amet turpis malesuada commodo.</p>
          <p>Praesent tincidunt, massa et porttitor imperdiet, lorem velit posuere erat, sit amet gravida odio magna in ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
          <p>Vivamus massa urna, pharetra eget malesuada eu, pharetra vitae arcu. Duis dapibus aliquam ante, ut posuere odio commodo sed. Pellentesque non hendrerit purus. Morbi tellus mauris, convallis non dictum non, elementum eget leo. Donec non ex est. Pellentesque magna urna, maximus ac augue id, pretium lobortis tortor. </p>
        </div>
      </div>
    </div>
  </section>
  <!--=== Careers End ===-->

