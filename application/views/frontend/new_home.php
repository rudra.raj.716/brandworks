


  <!--=== Flex Slider Start ===-->
  <section class="pt-0 pb-0">
    <div class="slider-bg flexslider">
       <?php
          $query=$this->db->order_by('slider_id','ASC')->get('slider');
          $users=$query->result_array();
          
          foreach($users as $user)
          {
          ?>
       
      <ul class="slides">
        <!--=== SLIDE 1 ===-->
        
        <li>

          <div class="slide-img" style="background:url(<?php echo base_url();?>assets/uploads/<?php echo $user['slider_image'];?>) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text white-color">
              <div class="container text-center">
                <h2 class="white-color font-500 letter-spacing-5"><?php echo $user['slider_title'];?></h2>
                <h1 class="white-color text-uppercase font-700"><?php echo $user['slider_topcaptain'];?></h1>
                <p class="text-center mt-30"><a href="<?php echo $user['slider_link'];?>" class="btn btn-white btn-rounded btn-animate"><span>Start a Project</span></a> </p>
              </div>
            </div>
          </div>
        </li>
      <?php }?>
        <!--=== SLIDE 2 ===-->

  </section>
  <!--=== Flex Slider End ===-->

  <!--=== Who We Are Start ===-->
  <section class="first-ico-box">
    <div class="dn-bg-lines">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
    <div class="left_parallax">
      <div class="vertical-text">
        <h3 data-lax-preset="driftRight" data-lax-optimize=true class="lax chunkyText font-700 dark-color">Design</h3>
      </div>
    </div>
    <div class="right_parallax">
      <h3 data-lax-preset="driftLeft" data-lax-optimize=true class="lax chunkyText font-700 red-color">Creative.</h3>
    </div>
    <div class="container">
      <!-- slogan -->
      <?php
          $query=$this->db->order_by('id','ASC')->get('setting');
          $users=$query->result_array();
          
          foreach($users as $user)
          {
          ?>

      <div class="row">
        <div class="col-sm-8 section-heading">
          <h2 class="wow fadeTop" data-wow-delay="0.1s"><?php echo $user['slogan1'];?></h2>
          
          <p class="mt-30 wow fadeTop" data-wow-delay="0.3s"><?php echo $user['slogan3'];?></p>
        </div>
      </div>
    <?php }?>
            
 
      <div class="row mt-50">
             <?php
          $query=$this->db->order_by('service_id','ASC')->get('services',3);
          $users=$query->result_array();
          foreach($users as $user)
          {
          ?>
        <div class="col-md-4 feature-box text-center radius-icon wow fadeTop" data-wow-delay="0.1s"> <i class="<?php echo $user['service_icon'];?>"></i>
          <h4 class="text-uppercase"><?php echo $user['service_title'];?></h4>
          <p><?php echo $user['service_description'];?></p>
        </div>
      <?php }?>

      </div>
    </div>
  </section>
  <!--=== Who We Are End ===-->

  <!--=== Portfolio Start ===-->
  <section class="pt-0 pb-0">

    <div class="container-fluid">
      <div class="row">
                 
        <div class="portfolio-container text-center">

          <ul id="portfolio-grid" class="three-column">
                <?php
                  $query=$this->db->get_where('projects',array('is_homepage'=>'yes'),6);
                  $users= $query->result_array();  

          foreach($users as $user)
          {
          ?>
            <li class="portfolio-item wow fadeIn wow fadeIn" data-wow-delay="0.1s" data-groups='["all", "print", "branding"]'>
              <div class="portfolio gallery-image-hover">
                <div class="dark-overlay"></div>
                <img src="<?php echo base_url();?>assets/uploads/<?php echo $user['project_image'];?>" alt="">
                <div class="portfolio-wrap">
                  <div class="portfolio-description">
                    <h3 class="portfolio-title"><?php echo $user['project_title'];?></h3>
                    <a href="single-portfolio.html" class="links"></a> </div>
                  <!--=== /.project-info ===-->
                  <ul class="portfolio-details">
                    <li><a class="alpha-lightbox" href="<?php echo base_url();?>assets/images/portfolio/grid/1.jpg"><i class="icofont icofont-search-1"></i></a></li>
                    <li><a href="<?php echo $user['project_link'];?>"><i class="icofont icofont-link-alt"></i></a></li>
                  </ul>
                </div>
              </div>

              <!--=== /.portfolio ===-->
            </li>
          <?php }?>  
          </ul>
        </div>

      </div>
    </div>
    
  </section>
  <!--=== Portfolio End ===-->

  <!--=== Counter Start ===-->
  <section class="dark-bg pt-80 pb-80">
    <div class="container-fluid">
      <div class="row">
        <?php
          $query=$this->db->order_by('id','ASC')->get('infographic');
          $users=$query->result_array();
          foreach($users as $user)
          {
          ?>

        <div class="col-md-4 counter text-center col-sm-6 wow fadeTop" data-wow-delay="0.1s">
          
          <h2 class="count white-color font-700"><?php echo $user['number'];?></h2>
          <h3><?php echo $user['name'];?></h3>
        </div>
        <?php }?>

       </div>
    </div>
  </section>
  <!--=== Counter End ===-->

  <!--=== Our Team Start ===-->
  <section class="white-bg pb-0">
    <div class="dn-bg-lines">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-8 section-heading">
          <h2 class="wow fadeTop" data-wow-delay="0.1s">Meet Our Team</h2>
          <h4 class="text-uppercase wow fadeTop" data-wow-delay="0.2s">- We Are Stronger -</h4>
        </div>
      </div>
    </div>

        <div class="container-fluid">
                    
      <div class="row mt-50">
           <?php
          $query=$this->db->order_by('team_id','ASC')->get('teams');
          $users=$query->result_array();
            foreach($users as $user)
            {
          ?>
        <div class="col-md-3 col-sm-6 col-xs-12 wow fadeTop remove-padding" data-wow-delay="0.3s">
          <div class="team-member-container gallery-image-hover"> <img src="<?php echo base_url();?>assets/uploads/<?php echo $user['image'];?>" class="img-responsive" alt="team-01">
            <div class="member-caption">
              <div class="member-description text-center">
                <div class="member-description-wrap">
                  <h4 class="member-title"><?php echo $user['name'];?></h4>
                  <p class="member-subtitle"><?php echo $user['designation'];?></p>
                  <ul class="member-icons">
                    <li class="social-icon"><a href="#"><i class="icofont icofont-facebook"></i></a></li>
                    <li class="social-icon"><a href="#"><i class="icofont icofont-twitter"></i></a></li>
                    <li class="social-icon"><a href="#"><i class="icofont icofont-youtube"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

        </div>
                <?php }?>
        <!--=== Member End ===-->
      
        <!--=== Member End ===-->
              
      </div>

    </div>

  </section>
  <!--=== Our Team End ===-->

  <!--=== Testimonails Start ===-->
  <section class="parallax-bg-18 fixed-bg" data-stellar-background-ratio="0.2">
    <div class="overlay-bg"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-8 section-heading white-color">
          <h2 class="wow fadeTop" data-wow-delay="0.1s">Testimonials</h2>
          <h4 class="text-uppercase wow fadeTop" data-wow-delay="0.2s">- Happy Clients -</h4>
        </div>
      </div>
                
      <div class="row">
        <div class="col-md-12">
          <div class="slick testimonial">
            <!--=== Slide ===-->
            <?php
          $query=$this->db->order_by('testimonial_id','ASC')->get('testimonials');
          $users=$query->result_array();
          
          foreach($users as $user)
          {
          ?>
       
            <div class="testimonial-item">
              <div class="testimonial-content"> <img class="img-responsive img-circle" src="<?php echo base_url();?>assets/uploads/<?php echo $user['testimonial_image'];?>" alt="avatar-1"/>
                <h5><?php echo $user['testimonial_title'];?></h5>
                <p><?php echo $user['testimonial_by'];?> </p>
                <h4><?php echo $user['testimonial_description'];?>.</h4>
              </div>
            </div>
 <?php }?>
            <!--=== Slide ===-->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--=== Testimonails End ===-->

  <!--=== Blogs Start ===-->
  <section>
    <div class="dn-bg-lines">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-8 section-heading">
          <h2 class="wow fadeTop" data-wow-delay="0.1s">Notice</h2>
          <h4 class="text-uppercase wow fadeTop" data-wow-delay="0.2s">- Latest News -</h4>
        </div>
      </div>
              
      <div class="row mt-50">
        <div class="col-md-12 remove-padding">
          <div class="owl-carousel blog-slider">
                 <?php
          $query=$this->db->order_by('notice_id','ASC')->get('notice',5);
          $users=$query->result_array();
          
          foreach($users as $user)
          {

          ?>
            <div class="post">

              <div class="post-img"> <img class="img-responsive" src="<?php echo base_url();?>assets/uploads/<?php echo $user['notice_image'];?>" alt=""/> </div>
              <div class="post-info">
                <h3><a href="blog-grid.html"><a href="<?=base_url().'scoda/single_notice/'.$user['page_slug']?>"><?php echo ellipsize($user['title'],40,1);?></a></h3>
                <h6><?php echo $user['notice_date'] ?></h6>
                <p><?php echo ellipsize($user['description'],30,1);?></p>
                <a class="readmore" href="#"><span>Read More</span></a> </div>
            </div>
          <?php }?>

          </div>
        </div>
      </div>
    </div>
  </section>

<!--=== Blogs End ===-->
<section class="pt-50 pb-50">
    <div class="container">
      <div class="row">
        <div id="client-slider" class="owl-carousel">
          <?php
          $query=$this->db->order_by('client_id','ASC')->get('clients');
          $users=$query->result_array();
          
          foreach($users as $user)
          {

          ?>
          <div class="client-logo"> <img class="img-responsive" height="200px" width="200px" src="<?php echo base_url();?>assets/uploads/<?php echo $user['client_image'];?>" alt="01"/> </div>
          <?php }?>
        </div>
      </div>
    </div>
  </section>

  <!--=== Call to Action Start ======-->
  <section class="pt-50 pb-50 dark-bg">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="cta-heading-left">
            <p class="subtitle mt-20">careers</p>
            <h3>Let's write your story, together.</h3>
          </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-6">
          <div class="cta-heading-right">
            <p class="mt-20 content-text">We do not tell you our story. We write it together. Partnering with us means a seat at the table where you will be heard.</p>
            <!-- <p class="mt-50"><a class="btn btn-rounded btn-color">Contact Us</a></p> -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--=== Call to Action End ======-->


