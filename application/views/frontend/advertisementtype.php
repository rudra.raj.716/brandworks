<div class="banner-text-left lernen_banner bg-services" style = "padding: 80px 0 80px;
    background-position: top center!important; background: linear-gradient(rgba(0, 0, 0, .6), rgba(0, 0, 0, .2)), url(<?php echo base_url();?>assets/img/avanibanner.jpg); background-size: cover!important; text-align: center; position: relative; ">
        <div class="container">
            <div class="row">
              <div class = 'col-md-12'>
                <div class="lernen_banner_title" style = "display: block; width: 100%;">
                    <h1 style = 'text-align:center; color:#fff; display:block; '><?php echo $projecttype['advertisement_type_name'];?></h1>
                </div>
              </div>
            </div>
        </div>
</div>



<div class="awards_area">
  <div class="container">
   
    <div class="row">
      <div class="col-md-12">
        <div class = "client_inner_area">
          <div class = "row">
          <?php if($projecttype['page_slug'] == 'tvc'){ ?>
          <?php foreach ($projects as $pro) { ?>
            <div class = 'col-md-4 tvcad'>

            <iframe width="100%" height="300" src="<?php echo $pro['advertisement_youtube']; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


                <!-- <video width="100%" height="auto" controls>
                <source src="<?php //echo base_url(); ?>assets/uploads/<?php //echo $pro['advertisement_video']; ?>" type="video/mp4">
                <source src="movie.ogg" type="video/ogg">Your browser does not support the video tag.
                </video> -->
            </div>
          <?php } ?>
            <?php }else{ ?>
          <?php foreach ($projects as $pro) { ?>
            <div class = 'col-md-3'>
            <a data-fancybox="gallery" href="<?php echo base_url(); ?>assets/uploads/<?php echo $pro['advertisement_image']; ?>"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $pro['advertisement_image']; ?>" alt="<?php echo $pro['advertisement_featured_image_alt']; ?>"></a>
            </div>
          <?php } } ?>
        </div>
      </div>
    </div>
  </div>

  
</div>
</div>
