 <!-- <div class="page-banner-image-section">
		<div class="image">
			<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $project['project_image']; ?>" alt="" />
		</div>
	</div> -->
<!-- Page Title Section -->
    <div class="page-title-section">
    	<div class="auto-container">
			<ul class="post-meta">
				<li><a href="<?php echo base_url(); ?>">Index</a></li>
				<li>Project</li>
			</ul>
			<h2><span><?php echo $project['project_title']; ?></span></h2>
		</div>
	</div>
	<!-- End Page Title Section -->
	
	<!-- Start Project Details -->
	<div class="project-section section-padding">
		<div class="auto-container">
			<div class="row">

				<!-- Portfolio Left -->
				<div class="col-lg-4 col-md-6 col-12">
					<div class="work-left work-details">
						<div class="portfolio-main-info">
							<!-- <h2 class="title">About the <br> project</h2> -->
							<!-- Start Details List -->
							<div class="work-details-list mt-60">

								<div class="details-list">
									<label>Date</label>
									<span><?php echo $project['project_date']; ?></span>
								</div>

								<div class="details-list">
									<label>Client</label>
									<span><?php echo $project['client'] ?></span>
								</div>

								<div class="details-list">
									<label>Category</label>
									<span><a href=""><?php echo $category['project_category_name']; ?></a></span>
								</div>

								<div class="details-list">
									<label>Awards</label>
									<span><?php echo $project['award'] ?></span>
								</div>

							</div>
							<!-- End Details List -->
							<!-- Start Work Share -->
							<!-- <div class="work-share section-padding-top-70">
								<h6 class="heading heading-h6">SHARE</h6>
							</div> -->
						</div>
					</div>
				</div>

				<!-- Work Right -->
				<div class="col-lg-7 col-md-6 offset-lg-1 col-12">
					<div class="work-left work-details mt-lg-30">
						<div class="work-main-info">
							<div class="work-content">
								<!-- <h6 class="title">ABOUT THE PROJECT</h6> -->

								<div class="desc mt-40">
									<div class="content mb-25">
										<?php echo $project['project_description']; ?>
									</div>
									

									<!-- <div class="work-btn">
										<a class="theme-btn btn-style-one" href="#"><span class="txt">Go to link</span></a>
									</div> -->

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
	<!-- Start Project Details -->

<script>
$(function()
{
	$('.main-header').addClass('style-three');
	$('.main-footer').addClass('style-two');
});
</script>