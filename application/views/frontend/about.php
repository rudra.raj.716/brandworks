<!-- Page Banner Image Section -->
    <div class="page-banner-image-section">
		<div class="image">
			<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $page['cover_image'];?>" alt="" />
		</div>
	</div>
	<!-- End Page Banner Image Section -->
	
<?php include(APPPATH.'views/layouts/frontend/partials/aboutsection.php'); ?>
<?php include(APPPATH.'views/layouts/frontend/partials/expertsectionall.php'); ?>
<?php include(APPPATH.'views/layouts/frontend/partials/testimonialsection.php'); ?>
<?php include(APPPATH.'views/layouts/frontend/partials/partnersection.php'); ?>

	
	
	
<script>
$(function()
{
	$('.main-header').addClass('style-two');
	$('.main-footer').addClass('style-two');
});
</script>