

  <!--=== page-title-section start ===-->
  <section class="title-hero-bg widget-cover-bg" data-stellar-background-ratio="0.2">
    <div class="container">
      <div class="page-title text-center">
        <h1>Careers</h1>
      </div>
    </div>
  </section>
  <!--=== page-title-section end ===-->

  <!--=== Careers Start ===-->
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 terms">
          <h2 class="font-700"><?php echo $users['job_title'];?></h2>
          <ul class="careers-listing">
             
            <li>
              <div class="row">
                <div class="career-main col-md-6">
                  <p><strong><?php echo $users['job_title']; ?></strong></p>
                  <p><strong><?php echo $users['job_description']; ?></strong></p>
                  <p><strong>Opening Date</strong></p>
                  <p><strong>Ending Date</strong></p>
                   </div>
                
            </li>
            
          </ul>
          <h2 class="mt-80 font-700">Apply Below</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eget nisl gravida, interdum nunc quis, faucibus ligula. Nam eu neque nunc. Suspendisse egestas dolor ante, nec tincidunt sem malesuada at. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi vehicula metus sit amet turpis malesuada commodo.</p>
          <p>Praesent tincidunt, massa et porttitor imperdiet, lorem velit posuere erat, sit amet gravida odio magna in ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
          <p>Vivamus massa urna, pharetra eget malesuada eu, pharetra vitae arcu. Duis dapibus aliquam ante, ut posuere odio commodo sed. Pellentesque non hendrerit purus. Morbi tellus mauris, convallis non dictum non, elementum eget leo. Donec non ex est. Pellentesque magna urna, maximus ac augue id, pretium lobortis tortor. </p>
        </div>
      </div>
    </div>
  </section>
  <!--=== Careers End ===-->

