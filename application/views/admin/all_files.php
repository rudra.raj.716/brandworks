
<?php //pr($this->session->flashdata('errors'),1); ?>
<?php if($this->session->flashdata('errors')) { ?>
            <div class="alert alert-danger">
              <?php echo $this->session->flashdata('errors'); ?>
            </div>
<?php } ?>




<div class="box box-warning">
  <div class="box-header with-border">
      <h3 class="box-title">All Files of Folder <b><?php echo $folder_info['folder_name']; ?></b></h3>
      <!--  <a href="" class="pull-right btn btn-success"  data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add Files</a> -->
  </div>
            <!-- /.box-header -->
  <div class="box-body">


 
      <span style="background: red;color:#fff;border-radius: 5px;">Note: Drag the images inside dotted area.</span>
      <form action="<?php echo base_url(); ?>files/ajax_upload_files/<?php echo $folder_info['folder_id']; ?>" class="dropzone needsclick dz-clickable" id="mydropzone">
      </form>

      <h3>All Uploaded files</h3>

   
      <br/>
      <?php if(!empty($files)) {  ?>
        <?php foreach($files as $f) { ?>
          <div class="col-lg-4 parentofthumb">  
            <div class="thumbnail">

            
                <a class="edit" style="right:40px" fileid="<?php echo $f->file_id; ?>" data-toggle="modal" data-target="#myModal<?php echo $f->file_id; ?>"><i class="fa fa-edit"></i></a>
            


                <a class="close" fileid="<?php echo $f->file_id; ?>">&times;</a>
            

                <?php if($f->file_type == 'pdf') {  ?>
                  <img  src="<?php echo base_url(); ?>assets/img/pdf.jpg" class="img-responsive" style="max-height: 100px">
                <?php } elseif($f->file_type == 'docx' || $f->file_type=='doc') {  ?>

                 <img  src="<?php echo base_url(); ?>assets/img/word.ico" class="img-responsive" style="max-height: 100px">
                  <?php } elseif($f->file_type == 'xl' || $f->file_type=='xls') {  ?>

                   <img  src="<?php echo base_url(); ?>assets/img/xls.png" class="img-responsive" style="max-height: 100px">

                <?php }  else {?>
                    <img  alt="<?php echo $f->file_name; ?>" src="<?php echo $f->file_url; ?>" class="img-responsive" style="max-height: 100px">
                <?php } ?>
                
                <div class="caption" style="text-align: center;">
                 <p><a href="<?php echo $f->file_url; ?>" target="_blank"><?php echo $f->file_name; ?></a></p>
                </div>
              </div>
          </div>

          <!-- Modal -->
            
            <form name="" action="<?php echo base_url(); ?>files/edit_file/<?php echo $f->file_id; ?>" method="post">
            <div class="modal fade" id="myModal<?php echo $f->file_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit File of <?php echo $f->file_name; ?></h4>
                  </div>
                  <div class="modal-body">
                      <div class="form-group">
                        <label>File Name : </label>
                        <input type="text" name="file_name" class="form-control" value="<?php echo $f->file_name; ?>">
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
          

          <?php } ?>
        <?php } ?>

   </div>
</div>





<!-- Modal -->
<style>
.close,.edit {
    position: absolute;
    top: 2px;
    right: 20px;
    z-index: 100;
    cursor: pointer
}
</style>

<script>

$(function()
{
  var delete_url = "<?php echo base_url(); ?>files/delete_files/";
  Dropzone.options.mydropzone = {
     addRemoveLinks: true,
     init:function()
     {

      this.on("success", function(file, response) {
         file.serverId = response; // If you just return the ID when storing the file

      });


      this.on("removedfile", function(file) {
        
        if (!file.serverId) { return; } // The file hasn't been uploaded
         $.post(delete_url + file.serverId); // Send the file id along
      });
     }
};

  $('.close').click(function()
  {
      var result = confirm("Are You Sure, You Want to delete?");
      if(result)
      {
       var filed_id = $(this).attr('fileid');
        $.post(delete_url + filed_id);
        $(this).parents('.parentofthumb').remove();
      }
      return false;

      
  });

})
</script>