<?php
/**
* formats the printing of array in a good way
* @param array() $my_array, flag bool, 1 for die and 0 for die.
* @return none
**/

function pr($my_array, $flag = 0)
	{
		echo '<pre>';
			print_r($my_array);
		echo '</pre>';
		if($flag == 1)
		die();
	}

class General_helper
{
	/**
	* converts an array to commas separated value with strings
	* @param array(single dimension)
	* @return string(comma separted values)
	**/
	public function array_to_comma($my_array)
	{	
		if(!empty($my_array))
		{
		$neo_array = array();
		foreach ($my_array as $key => $value) {
			$neo_array[] = "'".$value."'";
		}
		$comma_string = implode(",", $neo_array);
		return $comma_string;
		}
		else
		{
			return array();
		}
	}

	public static function get_menus()
	{
		$ci = & get_instance(); // instance of CI
		$menus = $ci->db->select('*')->from('menu')->order_by('menu_order','ASC')->get()->result();
	
		$parent_menus = $submenus = array();
		foreach ($menus as $key => $menu) {
			if($menu->parent_id == 0 && $menu->parent_id < 1)
			{
				// pr($menu,1);
				$parent_menus[$menu->menu_id]['main'] = $menu; 
			}else{	
				$parent_menus[$menu->parent_id]['sub'][] = $menu;
			}
		}
		return $ci->menus = $parent_menus;
	}

	public static function get_clients()
	{
	$ci = & get_instance(); // instance of CI
	$menus = $ci->db->select('*')->from('clients')->get()->result();
	return $menus;
	}


	public static function is_default_group($get_group_name)
	{
	$default_groups_name = array('admin','Project-Incharge','Project-Manager','Office-Manager','Analyzer','Equipment-Manager','HR-Manager');
	return in_array($get_group_name, $default_groups_name, true);
	}

	/**
	* converts an array_of_objects into a single dimensional array with a desired $key
	* @param array_of_objects, key
	* @return array
	**/
	public function array_of_object_to_single_dimension_by_key($object_of_array,$key)
	{
		$empty_array = array();
		foreach($object_of_array as $value)
		{
			$empty_array[] = $value->$key;
		}
		
		return $empty_array;
	}

	/**
	* sends the difference in date in days
	* @param $greater_date(date), $lesser_date(date)
	* @return $days(int)
	**/
	public function calculate_difference_in_date_in_days($greater_date, $lesser_date)
	{
		  $now = strtotime($greater_date);
		  $your_date = strtotime($lesser_date);
		  $datediff = $now - $your_date;
		  $date = floor($datediff/(60*60*24));
		  return $date;
	}


	/**
	 * [uploads a file.]
	 * @param  $_FILES(ghost param), string $uploadDir
	 * @return [boolean]            
	 */
	public static function upload_files($uploadDir = '/kingsmis/uploads/files/')
	{
		//$uploadDir = '/kingsmis/uploads/files/';
		$filename = key($_FILES);
		if (!empty($_FILES)) {
			$tempFile   = $_FILES[$filename]['tmp_name'];
			$uploadDir  = $_SERVER['DOCUMENT_ROOT'] . $uploadDir;
			$file_name = rand().$_FILES[$filename]['name'];
			$targetFile = $uploadDir . $file_name;
			//$url = base_url().'uploads/files/'.$file_name;
			if(move_uploaded_file($tempFile, $targetFile))
			{
				return $file_name;
			}
			else
			{
				return false;
			}
		}else
		{
			echo "asd";die;
			return false;
		}
	}

	public static function unique_field_name($field_name) {
	    return 's'.substr(md5($field_name),0,8); //This s is because is better for a string to begin with a letter and not with a number
    }

	
	public function time_elapsed_string($ptime)
	{

		//date_default_timezone_set('Asia/Kathmandu');

		date_default_timezone_set('Asia/Kathmandu');
		/*echo date('Y-m-d H:m:s');
		echo "<br/>";
		echo $ptime;*/
		/*echo date('H:m:s');
		echo "<br/>";
		echo $ptime;*/
		/*die();*/
			/*echo date('Y:m:d H:i:s');
			die();*/

	    $etime = strtotime(date('Y-m-d H:i:s')) - $ptime;
	   /* echo date('H:m:s');
		echo "<br/>";
		echo date('H:m:s',$ptime);*/
		//die();
	    if ($etime < 1)
	    {
	        return '0 seconds';
	    }

	    $a = array( 365 * 24 * 60 * 60  =>  'year',
	                 30 * 24 * 60 * 60  =>  'month',
	                      24 * 60 * 60  =>  'day',
	                           60 * 60  =>  'hour',
	                                60  =>  'minute',
	                                 1  =>  'second'
	                );
	    $a_plural = array( 'year'   => 'years',
	                       'month'  => 'months',
	                       'day'    => 'days',
	                       'hour'   => 'hours',
	                       'minute' => 'minutes',
	                       'second' => 'seconds'
	                );

	    foreach ($a as $secs => $str)
	    {
	        $d = $etime / $secs;
	        if ($d >= 1)
	        {
	            $r = round($d);
	            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
	        }
	    }
	}

	public static function array_to_pie_format($gender_array)
	{
		$total = 0;
		foreach($gender_array as $k => $v)
		{
			$gender_array[$k] = count($v);
			$total = $total + count($v);
		}	
		$pie_array = array();
		foreach($gender_array as $k => $v)
		{
			$pie_array[$k] = round(($v/$total)*100,0);
		}
		return $gender_array;
	}

	public function get_graph_intervals($graph_data)
	{
		$count = count($graph_data);
		$add_total = 0;
		foreach($graph_data as $key => $val)
		{
			$add_total = $val + $add_total;
		}
		$avg_interval = $add_total / $count;
		$first_point = 0;
		for($i = 0; $i <= $add_total; $i = $i+$avg_interval )
		{
			$to_return[] = $i;
		}
		return array_reverse($to_return);
	}



// 	$this->db->select('*')->from('news')->where(...); 
// $q = $this->db->get(); 
// return $q->num_rows();

//////////////////count total number of approvals //////////////
	public static function count_approvals(){
		$ci = & get_instance();

		$tot = $ci->db->select('*')
				->from('approval')
				->where("approval_status","Waiting")
				->get()->num_rows();
		return $tot;

	}

/////////////// count total number of notification///////////////
	public static function count_notification(){

		$ci = & get_instance();
		
		$tot = $ci->db->select('*')
				->from('notification')
				->where("notification_status","notviewed")
				->get()->num_rows();
		return $tot;
		
	}

	
	public static function is_serialized($value, &$result = null)
{
	// Bit of a give away this one
	if (!is_string($value))
	{
		return false;
	}
	// Serialized false, return true. unserialize() returns false on an
	// invalid string or it could return false if the string is serialized
	// false, eliminate that possibility.
	if ($value === 'b:0;')
	{
		$result = false;
		return true;
	}
	$length	= strlen($value);
	$end	= '';
	switch ($value[0])
	{
		case 's':
			if ($value[$length - 2] !== '"')
			{
				return false;
			}
		case 'b':
		case 'i':
		case 'd':
			// This looks odd but it is quicker than isset()ing
			$end .= ';';
		case 'a':
		case 'O':
			$end .= '}';
			if ($value[1] !== ':')
			{
				return false;
			}
			switch ($value[2])
			{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				break;
				default:
					return false;
			}
		case 'N':
			$end .= ';';
			if ($value[$length - 1] !== $end[0])
			{
				return false;
			}
		break;
		default:
			return false;
	}
	if (($result = @unserialize($value)) === false)
	{
		$result = null;
		return false;
	}
	return true;
}

	public static function slugify($string)
	{
	  // replace non letter or digits by -
		$string = strtolower($string);
		$slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
		return $slug;
	}

}