<?php 
Class Filter
{
	public static function get_group_by_name($name)
	{
		$ci = & get_instance();
		$ci->load->model('group_model');
		return $ci->group_model->get_by('name',$name)->id;
	}

  public static function get_menu()
  {

    $ci = & get_instance();
    $ci->load->model('Menu_Model');
    $menu = $ci->db->select('*')->from('menu')->order_by('menu_order','ASC')->get()->result();
    //$menu = $ci->Menu_Model->get_menu_by_parent(0);
    if(!empty($menu))
    {
      foreach ($menu as $key => $value) {
        $value->firstlevel = $ci->Menu_Model->get_menu_by_parent($value->menu_id);
        //$menu1 = $menu['firstlevel'];
        /*if(!empty($menu1))
        {
          foreach ($menu1 as $k => $v) {
            $menu['firstlevel']['secondlevel'] = $ci->menu_model->get_menu_by_parent($v->menu_id);
          }
        }*/
      }
    }
    return $menu;
  }

  public static function get_user_name($id)
  {
    $ci = & get_instance();
    $ci->load->model('Menu_Model');
    $user = $ci->Menu_Model->get($id);
    if($user){
      return $user->first_name .' '. $user->last_name;
    }else{
      return "Unknown";
    }    
  }

   public static function get_user_details($id)
    {
      $ci = & get_instance();
      $ci->load->model('user_model');
      $user = $ci->user_model->get($id);
      if($user){
        return $user;
      }else{
        return false;
      }    
    }

	public static function on($functions)
	{
		$ci = & get_instance();
		$controller = $ci->uri->segment(1);
		$function = $ci->uri->segment(2);
		$second_function = $ci->uri->segment(3);
		
    
		$status=false;

		if(!$functions)
		{
			return false;
		}

		//pr($functions,1);

		foreach($functions as $key=>$f)
		{
				
				if($key == $controller)
				{

					if(!empty($f))
					{

						if(in_array($function, $f))
						{
							return true;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return true;
					}
				}
		}
	}

	protected function list_controllers()
	{
		$controller = array();		

	    $ci->load->helper('file');

	    // Scan files in the /application/controllers directory
	    // Set the second param to TRUE or remove it if you 
	    // don't have controllers in sub directories
	    $files = get_dir_file_info(APPPATH.'controllers', FALSE);

	    // Loop through file names removing .php extension
	    foreach (array_keys($files) as $key=>$file)
	    {
	    	
	        $controller[$key]['controller_name']= str_replace(EXT, '', $file);
	        /*echo $controller['controller_name'];
	        die();*/
	        $controller[$key]['functions'] = get_class_methods($controller[$key]['controller_name']);
	    }

	    return $controller;

	}

	public static function user_type()
	{

		$ci =& get_instance();
		$id = $ci->ion_auth->get_user_id();
    $groups = $ci->ion_auth->get_users_groups($id)->result();
    if(empty($groups))
    {
      die('You havent been assigned to a group');
    }
		//$groups= $ci->db->query("select dg.name from users as du right join groups as dg on du.id = dg.id where du.id = $id")->result();

    //pr($groups,1);

		return $groups[0]->name;
		

  	}

  	public static function current_user($mid=null)
  	{	
      if(self::checklogin() !== true)
      {
        //echo Filter::user_type();
        redirect('auth/login');
        die();
      }

  		$ci =& get_instance();
  		$id = $ci->ion_auth->get_user_id();

      
  		if($mid == null)
       		{
  			$user_info = $ci->db->query("select * from users inner join users_groups on users_groups.user_id = users.id where users.id=$id")->result()[0];
  		}
  		else
  		{
  			$user_info = $ci->db->query("select * from users inner join users_groups on users_groups.user_id = users.id where users.id=$mid")->result()[0];
  		}
      
	  	return $user_info;  		
  	}

  		public static function get_users_by_type($id)
  	{	
      //echo $id;
      //die();
  		$ci =& get_instance();

  		/*echo "select * from users inner join users_groups on users_groups.user_id = users.id where groups.id=$id";
  		die();*/	

  		$user_info = $ci->db->query("select * from users inner join users_groups on users_groups.user_id = users.id inner join groups on groups.id = users_groups.group_id where users_groups.group_id=$id")->result();
      //echo $ci->db->last_query();
  		//pr($user_info,1);
	  	return $user_info;  		
  	}


      public static function get_user_by_type($id)
    { 
      //echo $id;
      //die();
      $ci =& get_instance();

      /*echo "select * from users inner join users_groups on users_groups.user_id = users.id where groups.id=$id";
      die();*/  

      $user_info = $ci->db->query("select * from users inner join users_groups on users_groups.user_id = users.id inner join groups on groups.id = users_groups.group_id where users_groups.user_id=$id")->result();
      //echo $ci->db->last_query();
      //pr($user_info,1);
      return $user_info;      
    }

  	public static function get_users_except($id){
  		
  		$ci =& get_instance();
  		
  		/*echo "select * from users inner join users_groups on users_groups.user_id = users.id where groups.id=$id";
  		die();*/	
  		$user_info = $ci->db->query("select * from users inner join users_groups on users_groups.user_id = users.id inner join groups on groups.id = users_groups.group_id where users_groups.group_id!=$id")->result();
  		
	  	return $user_info; 
	  }

  	public static function checklogin()
  	{
  		$ci = & get_instance();
  		if ($ci->ion_auth->logged_in()) {
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	}

  	public static function is_executive($teacher_id)
  	{
  		$count = $ci->db->select('*')->from('teachers')->where('teacher_id',$teacher_id)->where('is_executive','yes')->get()->num_rows;
  		if($count == 0)
  		{
  			return false;
  		}
  		else
  		{
  			return true;
  		}
  	}

  	public static function cross_check_student_parent_profile($id){

  		$current_user =Filter::current_user();
        $user_type = Filter::user_type();

        $student = $ci->student_m->get($id);  

        if($user_type == 'student')
        {
             if($current_user->other_details->student_id != $id)
                {
                        redirect('main');
                }
        }
        else if($user_type == 'parent')
        {
            if($student->parent_id != $current_user->other_details->parent_id)
            {
                redirect('main');
            }
        }
  	}

  	public static function parent_students($parent_id)
  	{
  		$ci->load->model('student_m');
  		$students = $ci->student_m->get_many_by('parent_id',$parent_id);
  		//pr($students,1);

  		return $students;
  	}


    public static function calculate_total_amount($string, $is_openbox = 'no')
    {
      if($is_openbox == 'no')
      {
        $count = 0;
        $total_amount = 0;
        $rec_amount = 0;
        $current_array = explode('+', $string);
        if($current_array[0] != '')
        {
          for($i = 0; $i<count($current_array); $i++)
          {
            $count++;
            $current_elem = explode('*', $current_array[$i]);

            if(isset($current_elem[1]))
            {
              $total_amount = $total_amount + $current_elem[1];
              if(isset($current_elem[3]))
              {
                if($current_elem[3] == 'delivered')
                {   
                  $rec_amount = $rec_amount + $current_elem[1];
                }
              }
            }

          }
        }

         $result = array('total_amount' => $total_amount, 'count' => $count , 'rec_amount' => $rec_amount);
        return $result;
      }
      elseif($is_openbox == 'yes')
      {
        $box_array = array();
        $total_amount = 0;
        $rec_amount = 0;
        $current_array = explode('+', $string);
        if($current_array[0] != '')
        {
        for($i = 0; $i<count($current_array); $i++)
        {
          $current_elem = explode('*', $current_array[$i]);
          $total_amount = $total_amount + $current_elem[2];
          if(isset($current_elem[3]))
          {
            if($current_elem[3] == 'delivered')
            {   
            $rec_amount = $rec_amount + $current_elem[2];
            }
          }
          $box_array[] = $current_elem[0];          
        }
        }
        $box_array_new = array_unique($box_array);
        $total_boxes = count($box_array_new);

        $result = array('total_amount' => $total_amount, 'count' => $total_boxes, 'rec_amount' => $rec_amount);
        return $result;
      }

     
    }

    public static function convert_product_qty($data)
    {
     // pr($data,1);
            $ci = & get_instance();
          $ci->load->model('category_model');
        $explode = explode('+',$data);
        $newArray  = [];
        $newArray['qty'] = count($explode);
        foreach($explode as $exp)
        {
          $explode1 = explode('*' , $exp);
          $explode1[2] = $ci->category_model->get($explode1[2])->category_name;

          $newArray['products'][] = $explode1;
        }
       return $newArray;
    }

    public static function get_meta_tags($page_name, $page_slug)
    {
      $ci = & get_instance();
      if($page_name == 'home')
      {
        //pull meta tags for home
       $meta_tags = $ci->db->select('meta_title,meta_description,meta_keywords,og_title,og_url,og_image,og_type,og_description,og_locale,schema_our')->from('setting')->get()->row();
       //pr($meta_tags,1);
       return $meta_tags;
      }
      elseif($page_name=='case')
      {
         //pull meta tags for home
       $meta_tags = $ci->db->select('meta_title,meta_description,meta_keywords,og_title,og_url,og_image,og_type,og_description,og_locale,schema_our')->from('case_study')->where('page_slug',$page_slug)->get()->row();

       return $meta_tags;
      }
      elseif($page_name=='projects')
      {
       $meta_tags = $ci->db->select('meta_title,meta_description,meta_keywords,og_title,og_url,og_image,og_type,og_description,og_locale,schema_our')->from('advertisement_type')->where('page_slug',$page_slug)->get()->row(); 
       
       return $meta_tags;
      }

      elseif($page_name=='page')
      {

        $meta_tags = $ci->db->select('meta_title,meta_description,meta_keywords,og_title,og_url,og_image,og_type,og_description,og_locale,schema_our')->from('page')->where('page_slug',$page_slug)->get()->row();       
      
        return $meta_tags;
      }

      
    }

    public static function get_social_links()
    {
      $ci = & get_instance();
      $social_media = $ci->db->select('*')->from('socialmedia')->get()->result_array();
      return $social_media;
    }

    public static function aboutus()
    {
      $ci = & get_instance();
      $aboutus = $ci->db->get('about_us')->row_array();
      return $aboutus;
    }
}


