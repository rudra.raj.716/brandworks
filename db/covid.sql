-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2020 at 11:53 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `covid`
--

-- --------------------------------------------------------

--
-- Table structure for table `collaborations`
--

CREATE TABLE `collaborations` (
  `collaboration_id` int(11) NOT NULL,
  `collaboration_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collaboration_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collaboration_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rankorder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `collaborations`
--

INSERT INTO `collaborations` (`collaboration_id`, `collaboration_title`, `collaboration_image`, `collaboration_description`, `rankorder`) VALUES
(33, '', '09660-logo_01.png', '', 0),
(34, '', '9746f-logo_02.png', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_subject` varchar(255) NOT NULL,
  `contact_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `contact_name`, `contact_email`, `contact_subject`, `contact_message`) VALUES
(14, 'sushant chapagain', 'thirstyhacker97@gmail.com', '', 'sdasjdkakd');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `file_type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `folder_id` int(11) NOT NULL,
  `file_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`file_id`, `file_name`, `file_type`, `folder_id`, `file_url`, `original_name`) VALUES
(10, '001', 'jpg', 4, 'http://asa-ca.com.np/assets/uploads/001.jpg', '001.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `folder_id` int(11) NOT NULL,
  `folder_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`folder_id`, `folder_name`, `project_id`) VALUES
(4, 'Page Photographs', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'user', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `infographics`
--

CREATE TABLE `infographics` (
  `info_id` int(11) NOT NULL,
  `numberinfo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labelinfo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logomark` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `infographics`
--

INSERT INTO `infographics` (`info_id`, `numberinfo`, `labelinfo`, `logomark`) VALUES
(1, '79', 'Completed Projects', 'flaticon-briefcase'),
(2, '750', 'Happy Clients', 'flaticon-happy'),
(3, '36', 'Customer Services', 'flaticon-idea'),
(4, '20', 'Answered Questions', 'flaticon-customer-service');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(250) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `link` varchar(250) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `url_type` enum('internal','external') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_title`, `menu_order`, `link`, `parent_id`, `url_type`) VALUES
(75, 'Business Law and Arbitration Research Center', 1, 'page/business-law-and-arbitration-research-center', 74, 'internal'),
(76, 'Center For the Training of Trial Advocacy and Proffesionalism', 2, 'page/center-for-the-training-of-trial-advocacy-and-professionalism', 74, 'internal'),
(77, 'Center of Philosophical and Juristic Discourse', 3, 'page/center-of-philosophical-and-juristic-discourse', 74, 'internal'),
(102, 'Our Team', 3, 'home/teams', 0, 'internal'),
(26, 'Home', 0, 'home', 0, 'internal'),
(91, 'KSL Policy and Legal Research Center', 7, 'page/ksl-policy-and-legal-research-center', 74, 'internal'),
(40, 'About Us', 1, 'page/about-us', 0, 'internal'),
(48, 'Full Time Faculty', 0, 'faculty/full-time', 47, 'internal'),
(49, 'Adjunct Faculty', 1, 'faculty/adjunct-faculty', 47, 'internal'),
(50, 'Visiting Researchers/Fellows', 2, 'faculty/visiting-researchers', 47, 'internal'),
(51, 'Visiting Faculty', 3, 'faculty/visiting-faculty', 47, 'internal'),
(52, 'Faculty in News', 4, 'news/faculty-news', 47, 'internal'),
(54, 'Action Research', 1, 'page/action-research', 53, 'internal'),
(55, 'KSLPRC', 2, 'page/kslprc', 53, 'internal'),
(56, 'Research Department', 3, 'page/research-department', 53, 'internal'),
(84, 'MCD', 4, '', 57, 'internal'),
(85, 'RD', 5, '', 57, 'internal'),
(82, 'CLFSD', 2, '', 57, 'internal'),
(83, 'HRHLD', 3, '', 57, 'internal'),
(103, 'Contact Us', 4, 'home/contact', 0, 'internal'),
(81, 'CLD', 1, '', 57, 'internal'),
(72, 'Notices', 1, 'notices/all', 71, 'internal'),
(73, 'Exam & Results', 2, 'exam', 71, 'internal'),
(88, 'Clinical Law Department', 4, 'page/clinical-law-department', 74, 'internal'),
(89, 'Human Rights and Humanitarian Law Department', 5, 'page/human-rights-and-humanitarian-law-department', 74, 'internal'),
(90, 'International Law and Relations Study Center', 6, 'page/international-law-and-relations-study-center', 74, 'internal'),
(92, 'Moot Courts Department', 8, 'page/moot-courts-department', 74, 'internal'),
(93, 'Research Department', 9, 'page/research-department', 74, 'internal'),
(94, 'Criminal Law and Forensic Science Department', 10, 'page/criminal-law-and-forensic-science-department', 74, 'internal'),
(95, 'Human Rights and Criminal Justice Clinic', 11, 'page/human-rights-and-criminal-justice-clinic', 74, 'internal'),
(96, 'Kathmandu School of Law Review ', 12, 'kslr', 74, 'internal');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `notice_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_date` date NOT NULL,
  `notice_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_intro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`notice_id`, `title`, `description`, `notice_date`, `notice_image`, `notice_intro`) VALUES
(27, 'New Website Launched of Acharya Subas & Associates', '<p>\n	<font color=\"#333333\" face=\"Lato, sans-serif\"><span style=\"font-size: 15px;\">Our news website have been launched, we will provide all notices and news related to our company via our official website.&nbsp; If you have any suggestions on the website please email us at info@asc-ca.com.np</span></font></p>\n', '2020-03-28', '4b3ce-seo_01.png', 'Our news website have been launched, we will provide all notices and news related to our company via our official website.  If you have any suggestions on the website please email us at info@asc-ca.com.np'),
(28, '3rd AGM notice 076/077 of Aadar Multi-Investment Co. Ltd.', '<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">Our news website have been launched, we will provide all notices and news related to our company via our official website.&nbsp; If you have any suggestions on the website please email us at info@asc-ca.com.np</span></p>\n', '2020-09-22', '2d761-capture-7-.png', 'Our news website have been launched, we will provide all notices and news related to our company via our official website.  If you have any suggestions on the website please email us at info@asc-ca.com.np'),
(29, 'Our news website have been launched,', '<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">Our news website have been launched, we will provide all notices and news related to our company via our official website.&nbsp; If you have any suggestions on the website please email us at info@asc-ca.com.np</span></p>\n', '2020-09-30', '17e1a-capture-6-.png', 'Our news website have been launched, we will provide all notices and news related to our company via our official website.');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  `page_url` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_title`, `page_slug`, `page_content`, `page_url`) VALUES
(43, 'About Us', 'about-us', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 20px; color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">\n	&nbsp;</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 20px; color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">\n	Acharya Subas &amp; Associates, Chartered Accountants is an auditing and financial management consultancy firm. At ASA, we focus primarily on Auditing, Advisory, Taxation and Business Consultancy Services applying multi disciplined expert knowledge of various professionals.</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 20px; color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">\n	Our strategic intent is to create value for our clients and to bring competitive advantage to their activities. We meet their needs by local market knowledge with our extensive range of skills and industry expertise. The firm has a team of experienced core staff who are dedicated to provide efficient services in the field of Taxation, financial services, banking, information technology, etc. We have our own in-house training and updating system in place to update ourselves on local developments and requirements.</p>\n', 'http://asa-ca.com.np/page/about-us'),
(44, 'Mission and Vision', 'mission-and-vision', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 20px; color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">\n	Our strategic intent is to create value for our clients and to bring competitive advantage to their activities. We meet their needs by local market knowledge with our extensive range of skills and industry expertise. The firm has a team of experienced core staff who are dedicated to provide efficient services in the field of Taxation, financial services, banking, information technology, etc. We have our own in-house training and updating system in place to update ourselves on local developments and requirements.</p>\n', 'http://asa-ca.com.np/page/mission-and-vision'),
(45, 'Our Team', 'our-team', '<div>\n	<strong>Executive Team</strong></div>\n<div>\n	&nbsp;</div>\n<div>\n	Proprietor (Mr Subas Acharya)</div>\n<div>\n	Manager (Mr Sudesh Pandeya)</div>\n<div>\n	Manager (Mr. Kishu Kuikel)</div>\n<div>\n	&nbsp;</div>\n<div>\n	<p>\n		<strong>Other Professional Staffs</strong></p>\n	<p>\n		Most of the staffs of the firm have worked for adequate time and are experienced in handling the job assigned. The staffs are well trained in the financial management and review works of various concerns. The senior member of the Firm is involved in providing financial, tax and management consultancy along with facilitation for statutory and internal audit, financial advisory services, tax and VAT consultancy of various governmental, non-governmental, and commercial organizations.</p>\n	<p>\n		Apart from presence of staffs at firm level, we also have other Professional Accountants whom we can deploy on need basis, for the engagement performance.</p>\n</div>\n<p>\n	&nbsp;</p>\n', 'http://asa-ca.com.np/page/our-team');

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `resource_id` int(9) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_title`, `service_description`, `service_image`) VALUES
(1, 'Internal Audit & Control Services', '<p>\n	<span courier=\"\" lucida=\"\" style=\"font-family: Consolas, \" white-space:=\"\">Whether you are looking to develop an internal audit function or make an existing one more effective, ASA CA has the experience tools and dedicated full-time resources to help you succeed.</span></p>\n', 'b1dbd-seo_06.png'),
(2, 'Assurance Services', '<p>\n	<span courier=\"\" lucida=\"\" style=\"font-family: Consolas, \" white-space:=\"\">Our accounting member firms can guide you through the often complex financial, operational and compliance decisions you need to make. They have the skills to provide the reassurance that your stakeholders need. </span></p>\n', '8b1fe-seo_02.png'),
(3, 'Taxation Services', '<p>\n	<span courier=\"\" lucida=\"\" style=\"font-family: Consolas, \" white-space:=\"\">While we ensure, that you maintain compliance, we make definitely sure that we create value for your business, giving you the best tax advice &amp; assistance.</span></p>\n', 'e128a-seo_03.png'),
(4, 'Company Secretarial Services', '<p>\n	Our company secretarial support service is designed to help private companies meet their standard statutory compliance needs.</p>\n', '6ef4f-seo_04.png'),
(5, 'Advisory & Other Services', '<div>\n	Our company Advisoru and Other services is designed to help private companies meet their standard statutory compliance needs via advisory and counselling.</div>\n<div>\n	&nbsp;</div>\n', 'f2230-seo_05.png'),
(6, 'Trainings/Workshops', '<p>\n	Our company provide trainings and workshops to various professional and beginners via a group of highly skilled Chartered Accountant and specialists in the area of accounting.</p>\n', 'da812-seo_01.png');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL,
  `slider_title` varchar(255) NOT NULL,
  `slider_subheading` varchar(255) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `slider_url` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_title`, `slider_subheading`, `slider_image`, `slider_url`) VALUES
(9, 'Covid1', '', 'be4bd-c1.jpg', ''),
(11, 'c2', '', '374d1-c1.jpeg', '');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`team_id`, `name`, `designation`, `info`, `image`, `team_order`) VALUES
(1, 'sushant', 'Assistant Professor', '<p>\n	<span style=\"color: rgb(58, 58, 58); font-family: Roboto, Arial, sans-serif; font-size: 18px; background-color: rgb(254, 254, 254);\">Although the CodeIgniter paging library automatically detects the paging-related parameter from the URL, you could define a custom value if you have different URL pattern.</span></p>\n', '3db40-images.jpg', 1),
(2, 'sagun siwakoti', 'Assistant Professor', '<p>\r\n	<span style=\"color: rgb(58, 58, 58); font-family: Roboto, Arial, sans-serif; font-size: 18px; background-color: rgb(254, 254, 254);\">Although the CodeIgniter paging library automatically detects the paging-related parameter from the URL, you could define a custom value if you have different URL pattern.</span></p>\r\n', '3db40-images.jpg', 1),
(3, 'sagun siwakoti', 'Assistant Professor', '<p>\r\n	<span style=\"color: rgb(58, 58, 58); font-family: Roboto, Arial, sans-serif; font-size: 18px; background-color: rgb(254, 254, 254);\">Although the CodeIgniter paging library automatically detects the paging-related parameter from the URL, you could define a custom value if you have different URL pattern.</span></p>\r\n', '3db40-images.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_by` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonial_id`, `testimonial_title`, `testimonial_by`, `testimonial_description`, `testimonial_image`) VALUES
(5, ' Great & Talented Team!', 'Sikhar Dhawan - Batsman', '<p>\n	The master-builder of human happines no one rejects, dislikes avoids pleasure itself, because it is very pursue pleasure.&nbsp;</p>\n', '0e5f8-testi_01.png'),
(6, ' Awesome Services! ', 'Mitchell Starc - Fast Bowler', '<p>\n	Explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you completed.</p>\n', '4ab10-testi_02.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$uA399cm3NhoUkqXUla5fK.GFk405MrJUpaBqdsoqdw1Rskhn5txRe', '', 'info@asa-ca.com.np', '', NULL, NULL, NULL, 1268889823, 1600926581, 1, 'Admin', 'istrator', 'ADMIN', '9841546900');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(6, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `collaborations`
--
ALTER TABLE `collaborations`
  ADD PRIMARY KEY (`collaboration_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD UNIQUE KEY `file_id` (`file_id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD UNIQUE KEY `folder_id` (`folder_id`),
  ADD UNIQUE KEY `folder_name` (`folder_name`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infographics`
--
ALTER TABLE `infographics`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`resource_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `collaborations`
--
ALTER TABLE `collaborations`
  MODIFY `collaboration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `folder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `infographics`
--
ALTER TABLE `infographics`
  MODIFY `info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
  MODIFY `resource_id` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
