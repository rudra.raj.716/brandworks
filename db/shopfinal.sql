-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2018 at 05:22 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `kslnew`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_subject` varchar(255) NOT NULL,
  `status` enum('pending','processed','') NOT NULL,
  `contact_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'user', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(250) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `link` varchar(250) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_heading` varchar(250) NOT NULL,
  `news_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_heading`, `news_description`) VALUES
(1, 'New Website Launched', '<p>\n  Dear Readers, Our Store has Launched its website. You can buy products online now.</p>\n'),
(2, 'Dashain Offer of 50 %', '<p>\r\n We have decided to give a dashain offer upto 50 %. Be sure to check in at products in our online store</p>\r\n'),
(3, 'New Macbook Air and its Features', '<p>\r\n  Here we have enlisted the feature of new Mac Book Air, The beautiful beast.</p>\r\n'),
(4, 'Alienware Will be available in our store soon.', 'Gamebuddys be ready the new alienware is on its way. Check out the new features on the amazing series.');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `featured_image` varchar(255) NOT NULL,
  `page_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_title`, `page_slug`, `featured_image`, `page_content`) VALUES
(10, 'About Store', 'about_store', '', '<p font-size:="" open="" style="margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: ">\n Lorem ipsum dolor sit amet, sdfsdf consectetur adipiscing elit. Pellentesque tristique efficitur velit finibus varius. Nunc semper mauris a laoreet pulvinar. Quisque tempor mattis leo, nec auctor tortor ornare tincidunt. Phasellus id tortor nunc. Vivamus ante metus, tempor sed vestibulum id, finibus vehicula neque. Pellentesque vestibulum ac ligula sollicitudin porttitor. Mauris nisi lorem, feugiat vitae rutrum eget, scelerisque vel urna. Nullam gravida ultrices mi, ut porttitor nulla bibendum in. Sed interdum feugiat nulla ut feugiat. Morbi sit amet augue eu erat vestibulum tincidunt in vel lorem. Proin faucibus dapibus ante at feugiat.</p>\n<p font-size:="" open="" style="margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: ">\n  Nullam tincidunt ligula in urna pretium placerat. Donec fermentum ipsum velit, at malesuada leo volutpat eu. Praesent tristique volutpat nisl vel venenatis. Suspendisse orci elit, porta id ex at, cursus varius enim. Phasellus lobortis, mi nec faucibus fermentum, risus purus pharetra quam, eget ullamcorper felis lectus eu velit. Vestibulum pretium risus mauris. Suspendisse a urna elit.</p>\n<p font-size:="" open="" style="margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: ">\n  Aliquam vitae scelerisque dui. Etiam sit amet aliquet nulla. Nunc hendrerit ullamcorper lectus. Nulla non felis ut tortor sagittis feugiat. Duis pharetra venenatis pretium. Nullam enim augue, venenatis et fermentum sed, fermentum in tortor. Proin fringilla vel ipsum vel commodo. Aenean et massa quis dolor ullamcorper faucibus vel non mi.</p>\n<p font-size:="" open="" style="margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: ">\n  Donec volutpat rutrum nunc a interdum. Proin sed erat tempor augue euismod semper at et nisi. Morbi eu ultrices elit. In vitae lorem ac tortor pellentesque congue a eu erat. Aenean bibendum vel sapien vulputate gravida. Pellentesque id diam vitae nisi tristique varius. In eget semper tellus. Fusce fermentum tristique sem id bibendum. Maecenas nec placerat ante, luctus mattis augue. Nam sapien sapien, posuere ac finibus nec, tincidunt vel erat.</p>\n<p font-size:="" open="" style="margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: ">\n  Pellentesque ac leo sollicitudin ligula commodo facilisis vel ac tortor. In id libero dictum dolor vestibulum tristique. Etiam in mattis tellus. Cras id tortor vitae magna malesuada bibendum. Nulla leo sapien, sagittis ultrices mauris ornare, viverra malesuada orci. Nam vel elit sit amet quam interdum mattis tempus eu orci. Morbi turpis nibh, faucibus sit amet faucibus a, euismod at tellus. Ut vitae enim erat. Phasellus ut sapien laoreet, gravida ante quis, suscipit eros. Quisque eget ultrices quam, auctor vestibulum ante. Sed consectetur molestie nulla eget molestie. Integer sed fermentum ligula. Duis quis arcu id magna vehicula mollis. Morbi lobortis, eros sed egestas bibendum, enim neque sodales sapien, vel congue elit leo at nisl.</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL,
  `slider_title` varchar(255) NOT NULL,
  `slider_subheading` varchar(255) NOT NULL,
  `slider_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_title`, `slider_subheading`, `slider_image`) VALUES
(2, 'Buy One Get One Free', '', 'b94bb-banner01.jpg'),
(3, 'Your Convenient Laptop Store', '', '59dcb-banner02.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$1K58bD0VbNHVEOUDhIePBu5SAvMR2oDAHUsA0z9RXSs7hgXeNKw7y', '', 'admin@admin.com', '', NULL, NULL, 'jFQ0apcAidgHiOM5EkvQJ.', 1268889823, 1537505561, 1, 'Admin', 'istrator', 'ADMIN', '9841546900'),
(14, '::1', 'roshan@gmail.com', '$2y$08$Q/uHIYjGiXiqiFzATsOzqOvpuHKehQzFtn.n2SVsXSh8ctoLQQe3C', NULL, 'roshan@gmail.com', NULL, NULL, NULL, NULL, 1533180353, 1533185719, 1, 'roshan', 'lammichhane', 'netherlands', '7857567');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(6, 1, 1),
(13, 14, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
