-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2018 at 04:30 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `kslnew`
--

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE `alumni` (
  `alumni_id` int(11) NOT NULL,
  `fullname` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `designation` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_experience` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alumni_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alumni`
--

INSERT INTO `alumni` (`alumni_id`, `fullname`, `address`, `phone`, `email`, `date_of_birth`, `designation`, `work_experience`, `education`, `alumni_image`, `year`, `course`, `page_url`, `page_slug`) VALUES
(1, 'Sagun Siwakoti', 'Nikosera-3, Bhaktapur', '9841546900', 'sagunstark@gmail.com', '2018-11-13', 'Web Developer at Spark Technology', '<p>\n  Just a test</p>\n', '<p>\n  Just a test</p>\n', '811a6-0266554465.jpeg', '2009', 'BCIS', 'http://localhost/kslnew/alumni/sagun-siwakoti', 'sagun-siwakoti');

-- --------------------------------------------------------

--
-- Table structure for table `collaborations`
--

CREATE TABLE `collaborations` (
  `collaboration_id` int(11) NOT NULL,
  `collaboration_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collaboration_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collaboration_description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `collaborations`
--

INSERT INTO `collaborations` (`collaboration_id`, `collaboration_title`, `collaboration_image`, `collaboration_description`) VALUES
(1, 'University of Sydney(Australia)', 'e6664-0266554465.jpeg', '<p>\n  University of Sydney(Australia)</p>\n'),
(2, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\n  Rajiv Gandhi University</p>\n'),
(3, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\r\n  Rajiv Gandhi University</p>\r\n'),
(4, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\r\n  Rajiv Gandhi University</p>\r\n'),
(5, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\r\n  Rajiv Gandhi University</p>\r\n'),
(6, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\r\n  Rajiv Gandhi University</p>\r\n'),
(7, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\r\n  Rajiv Gandhi University</p>\r\n'),
(8, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\r\n  Rajiv Gandhi University</p>\r\n'),
(9, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\r\n  Rajiv Gandhi University</p>\r\n'),
(10, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\r\n Rajiv Gandhi University</p>\r\n'),
(11, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\r\n Rajiv Gandhi University</p>\r\n'),
(12, 'Rajiv Gandhi University', '33dc6-0266554465.jpeg', '<p>\r\n Rajiv Gandhi University</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_subject` varchar(255) NOT NULL,
  `status` enum('pending','processed','') NOT NULL,
  `contact_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `equipments`
--

CREATE TABLE `equipments` (
  `equipment_id` int(11) NOT NULL,
  `equipment_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `equipment_numbers` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `equipment_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `equipments`
--

INSERT INTO `equipments` (`equipment_id`, `equipment_name`, `equipment_numbers`, `equipment_category_id`) VALUES
(1, 'Desktop Computer', '40', 1),
(2, 'Chairs', '250', 2);

-- --------------------------------------------------------

--
-- Table structure for table `equipment_categories`
--

CREATE TABLE `equipment_categories` (
  `equipment_category_id` int(11) NOT NULL,
  `category_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `equipment_categories`
--

INSERT INTO `equipment_categories` (`equipment_category_id`, `category_name`) VALUES
(1, 'Computers'),
(2, 'Furnitures');

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE `exam` (
  `result_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_date` date NOT NULL,
  `notice_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`result_id`, `title`, `description`, `notice_date`, `notice_file`, `page_url`, `page_slug`) VALUES
(1, 'BA. LLB. results have been announced', '<p>\n  BA. LLB. results have been announced.</p>\n', '2018-11-22', 'd895f-0266554465.jpeg', 'http://localhost/kslnew/exam/ba-llb-results-have-been-announced', 'ba-llb-results-have-been-announced'),
(2, 'LLM results announced', '<p>\n LLM results announced</p>\n', '2018-11-21', '90e1b-artsfon.com-73713-1-.jpg', 'http://localhost/kslnew/exam/llm-results-announced', 'llm-results-announced'),
(3, 'LLM results announced', '<p>\r\n LLM results announced</p>\r\n', '2018-11-21', '90e1b-artsfon.com-73713-1-.jpg', 'http://localhost/kslnew/exam/llm-results-announced', 'llm-results-announced'),
(4, 'BA. LLB. results have been announced', '<p>\r\n  BA. LLB. results have been announced.</p>\r\n', '2018-11-22', 'd895f-0266554465.jpeg', 'http://localhost/kslnew/exam/ba-llb-results-have-been-announced', 'ba-llb-results-have-been-announced'),
(5, 'LLM results announced', '<p>\r\n LLM results announced</p>\r\n', '2018-11-21', '90e1b-artsfon.com-73713-1-.jpg', 'http://localhost/kslnew/exam/llm-results-announced', 'llm-results-announced'),
(6, 'LLM results announced', '<p>\r\n LLM results announced</p>\r\n', '2018-11-21', '90e1b-artsfon.com-73713-1-.jpg', 'http://localhost/kslnew/exam/llm-results-announced', 'llm-results-announced');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `faculty_id` int(11) NOT NULL,
  `full_name` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `date_of_birth` date NOT NULL,
  `designation` varchar(250) NOT NULL,
  `work_experience` text NOT NULL,
  `type` enum('Full Time Faculty','Visiting Faculty','Visiting Researchers','Visiting Fellows') NOT NULL,
  `education` text NOT NULL,
  `faculty_image` varchar(250) NOT NULL,
  `faculty_file` varchar(250) NOT NULL,
  `faculty_order` int(11) NOT NULL,
  `page_url` varchar(250) NOT NULL,
  `page_slug` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`faculty_id`, `full_name`, `address`, `phone`, `email`, `date_of_birth`, `designation`, `work_experience`, `type`, `education`, `faculty_image`, `faculty_file`, `faculty_order`, `page_url`, `page_slug`) VALUES
(1, 'Dr Yubaraj Sangroula', 'Bhaktapur', '767867678678', 'yrs@gmai.com', '1981-02-12', 'ED/ Proffesor', '<div>\n  Dr. Yubaraj Sangroula was appointed by the President of Nepal on recommendation of the Prime Minister. Attorney General is chief legal advisor of the Government, the president as well as Chief prosecutor of the State. Resigned from the position on 30th August, 2011. Before resignation, major works carried out were &#39;development of five years plan of action for strengthening of the performance of Attorney General Office&#39;, The launching of &#39;five years plan of actions&#39; for reforms of the Attorney General Office achieved (a) Establishment of the Research and Human Rights Monitoring Department; (b) Establishment of &quot;Prosecutors Training Center, (c) Launching of tri-monthly Bulletin for Reporting Issues of Human Rights Protection from Attorney General Office; (d) Issuance of Directives to Police Investigators and Prosecutors to Effectively Prosecute Violence against Dalit and other Vulnerable Communities; and Prevent Removal or Pardoning of Criminal Charges were major achievements made during the tenure. He played a leading role in introduction and advocacy of the &#39;Criminal Code, Criminal Procedure Code, and Sentencing Act, which are now passed and come into implementation from Bhadra of 2075 which is aimed to revolutionize the process of reforms of Criminal Justice System. As an Attorney General had played crucial role in &#39;ratification of Money Laundering Convention and Anti-Financing on Terrorism Convention.</div>\n<div>\n  &nbsp;</div>\n', 'Full Time Faculty', '<div>\n  Dr. Yubaraj Sangroula was appointed by the President of Nepal on recommendation of the Prime Minister. Attorney General is chief legal advisor of the Government, the president as well as Chief prosecutor of the State. Resigned from the position on 30th August, 2011. Before resignation, major works carried out were &#39;development of five years plan of action for strengthening of the performance of Attorney General Office&#39;, The launching of &#39;five years plan of actions&#39; for reforms of the Attorney General Office achieved (a) Establishment of the Research and Human Rights Monitoring Department; (b) Establishment of &quot;Prosecutors Training Center, (c) Launching of tri-monthly Bulletin for Reporting Issues of Human Rights Protection from Attorney General Office; (d) Issuance of Directives to Police Investigators and Prosecutors to Effectively Prosecute Violence against Dalit and other Vulnerable Communities; and Prevent Removal or Pardoning of Criminal Charges were major achievements made during the tenure. He played a leading role in introduction and advocacy of the &#39;Criminal Code, Criminal Procedure Code, and Sentencing Act, which are now passed and come into implementation from Bhadra of 2075 which is aimed to revolutionize the process of reforms of Criminal Justice System. As an Attorney General had played crucial role in &#39;ratification of Money Laundering Convention and Anti-Financing on Terrorism Convention.</div>\n<div>\n  &nbsp;</div>\n', '002a0-sagun2.jpg', 'a1363-sagun2.jpg', 1, 'http://localhost/kslnew/faculty/dr-yubaraj-sangroula', 'dr-yubaraj-sangroula'),
(2, 'Dr. Pragya Dhungel', 'Nikosera, Bhaktapur', '9841520885', 'pragyadhungel@gmail.com', '1991-12-04', 'SEO Lecturer', '<div>\n  Dr. Yubaraj Sangroula was appointed by the President of Nepal on recommendation of the Prime Minister. Attorney General is chief legal advisor of the Government, the president as well as Chief prosecutor of the State. Resigned from the position on 30th August, 2011. Before resignation, major works carried out were &#39;development of five years plan of action for strengthening of the performance of Attorney General Office&#39;, The launching of &#39;five years plan of actions&#39; for reforms of the Attorney General Office achieved (a) Establishment of the Research and Human Rights Monitoring Department; (b) Establishment of &quot;Prosecutors Training Center, (c) Launching of tri-monthly Bulletin for Reporting Issues of Human Rights Protection from Attorney General Office; (d) Issuance of Directives to Police Investigators and Prosecutors to Effectively Prosecute Violence against Dalit and other Vulnerable Communities; and Prevent Removal or Pardoning of Criminal Charges were major achievements made during the tenure. He played a leading role in introduction and advocacy of the &#39;Criminal Code, Criminal Procedure Code, and Sentencing Act, which are now passed and come into implementation from Bhadra of 2075 which is aimed to revolutionize the process of reforms of Criminal Justice System. As an Attorney General had played crucial role in &#39;ratification of Money Laundering Convention and Anti-Financing on Terrorism Convention.</div>\n<div>\n  &nbsp;</div>\n', 'Visiting Researchers', '<div>\n Dr. Yubaraj Sangroula was appointed by the President of Nepal on recommendation of the Prime Minister. Attorney General is chief legal advisor of the Government, the president as well as Chief prosecutor of the State. Resigned from the position on 30th August, 2011. Before resignation, major works carried out were &#39;development of five years plan of action for strengthening of the performance of Attorney General Office&#39;, The launching of &#39;five years plan of actions&#39; for reforms of the Attorney General Office achieved (a) Establishment of the Research and Human Rights Monitoring Department; (b) Establishment of &quot;Prosecutors Training Center, (c) Launching of tri-monthly Bulletin for Reporting Issues of Human Rights Protection from Attorney General Office; (d) Issuance of Directives to Police Investigators and Prosecutors to Effectively Prosecute Violence against Dalit and other Vulnerable Communities; and Prevent Removal or Pardoning of Criminal Charges were major achievements made during the tenure. He played a leading role in introduction and advocacy of the &#39;Criminal Code, Criminal Procedure Code, and Sentencing Act, which are now passed and come into implementation from Bhadra of 2075 which is aimed to revolutionize the process of reforms of Criminal Justice System. As an Attorney General had played crucial role in &#39;ratification of Money Laundering Convention and Anti-Financing on Terrorism Convention.</div>\n<div>\n  &nbsp;</div>\n', 'cac84-tpragya.jpg', 'd5805-tpragya.jpg', 1, 'http://localhost/kslnew/faculty/dr-pragya-dhungel', 'dr-pragya-dhungel'),
(3, 'Mr Sagun Siwakoti', 'Nikosera, Bhaktapur', '9841546900', 'sagunstark@gmail.com', '1991-05-25', 'Data Analyst', '<div>\n  Hello, I am Sagun Siwakoti. Welcome to my Blog. I am an IT Professional from Nepal. Currently, I am one of the co-founders and Technical Lead in Any Tech Pvt Ltd. I also work as IT Consultant/Supervisor for Kathmandu School of Law. I have 3 years of experience as Web Developer and 2 years of IT/Web Consultant.</div>\n<div>\n  &nbsp;</div>\n<div>\n My Journey into the world of technology began when i was 9. Back then computers used to be black and white(at least in context of where i live) and they had operating systems called DOS, where you had to type win95-&gt;enter to open windows :). I loved to learn all the black and white commands(cd,md,mkdir, i could name more) and that was my first step towards learning about computers. After I graduated i joined a couple of software companies as web developer and finally started creating websites with my own team.</div>\n<div>\n &nbsp;</div>\n<div>\n Apart from technology, my hobbies include watching football(I am a Liverpool Fan), debating politics(I am a social democrat), War and History(you can&rsquo;t be biased on this one), trekking and learning spirituality(i believe god is a mother), and strangely i could explain god to you in object oriented manner(god is a class and we are all objects), sounds crazy right.</div>\n', 'Visiting Researchers', '<div>\n  Hello, I am Sagun Siwakoti. Welcome to my Blog. I am an IT Professional from Nepal. Currently, I am one of the co-founders and Technical Lead in Any Tech Pvt Ltd. I also work as IT Consultant/Supervisor for Kathmandu School of Law. I have 3 years of experience as Web Developer and 2 years of IT/Web Consultant.</div>\n<div>\n  &nbsp;</div>\n<div>\n My Journey into the world of technology began when i was 9. Back then computers used to be black and white(at least in context of where i live) and they had operating systems called DOS, where you had to type win95-&gt;enter to open windows :). I loved to learn all the black and white commands(cd,md,mkdir, i could name more) and that was my first step towards learning about computers. After I graduated i joined a couple of software companies as web developer and finally started creating websites with my own team.</div>\n<div>\n &nbsp;</div>\n<div>\n Apart from technology, my hobbies include watching football(I am a Liverpool Fan), debating politics(I am a social democrat), War and History(you can&rsquo;t be biased on this one), trekking and learning spirituality(i believe god is a mother), and strangely i could explain god to you in object oriented manner(god is a class and we are all objects), sounds crazy right.</div>\n', 'a273e-sagun2.jpg', '9bb50-sagun2.jpg', 2, 'http://localhost/kslnew/faculty/mr-sagun-siwakoti', 'mr-sagun-siwakoti'),
(4, 'Dr. Ravi Prakash Vyas', 'Dadhikot, Bhaktapur', '9841534758', 'ravi@vyaspartners.com', '2018-11-08', 'Sr. Proffesor', '<p>\n  Just test</p>\n', 'Full Time Faculty', '<p>\n Just Test</p>\n', '9b7a6-0266554465.jpeg', 'd90a2-0266554465.jpeg', 4, 'http://localhost/kslnew/faculty/dr-ravi-prakash-vyas', 'dr-ravi-prakash-vyas');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `file_type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `folder_id` int(11) NOT NULL,
  `file_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`file_id`, `file_name`, `file_type`, `folder_id`, `file_url`, `original_name`) VALUES
(3, 'tsagun', 'jpg', 4, 'http://localhost/kslnew/assets/uploads/tsagun.jpg', 'tsagun.jpg'),
(4, 'sagun2', 'jpg', 4, 'http://localhost/kslnew/assets/uploads/sagun2.jpg', 'sagun2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `financials`
--

CREATE TABLE `financials` (
  `financial_id` int(11) NOT NULL,
  `financial_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `financial_year` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `financial_published_date` date NOT NULL,
  `financial_file` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `financials`
--

INSERT INTO `financials` (`financial_id`, `financial_name`, `financial_year`, `financial_published_date`, `financial_file`) VALUES
(1, 'Balance Sheet 2070', '2070', '2018-11-23', '8c534-animelist.txt'),
(2, 'Income Statement', '2070', '2018-11-08', '5ac61-0266554465.jpeg'),
(3, 'Cash Flow Statement', '2071', '2018-11-15', '6f50c-patanjali.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `folder_id` int(11) NOT NULL,
  `folder_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`folder_id`, `folder_name`, `project_id`) VALUES
(4, 'Page Photographs', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'user', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL,
  `job_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_date` date NOT NULL,
  `ending_date` date NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `job_title`, `job_description`, `opening_date`, `ending_date`, `page_slug`, `page_url`) VALUES
(1, 'IT Officer', '<p>\n  IT Officer</p>\n', '2018-11-02', '2018-11-30', 'it-officer', 'http://localhost/kslnew/jobs/it-officer'),
(2, 'SEO Consultant', '<p>\n  SEO Consultant</p>\n', '2018-11-09', '2018-11-30', 'seo-consultant', 'http://localhost/kslnew/jobs/seo-consultant'),
(3, 'Chief Accounting Officer', '<p>\n  Chief Accounting Officer</p>\n', '2018-11-09', '2018-11-21', 'chief-accounting-officer', 'http://localhost/kslnew/jobs/chief-accounting-officer');

-- --------------------------------------------------------

--
-- Table structure for table `jobsubmission`
--

CREATE TABLE `jobsubmission` (
  `submissionid` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_id` int(11) NOT NULL,
  `cvupload` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateofsubmission` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `management_committee`
--

CREATE TABLE `management_committee` (
  `m_id` int(11) NOT NULL,
  `fullname` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_photo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `management_committee`
--

INSERT INTO `management_committee` (`m_id`, `fullname`, `designation`, `member_photo`, `url`) VALUES
(1, 'Mrs Sudha KC', 'Academic Incharge', '5f2f0-0266554465.jpeg', 'http://google.com');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(250) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `link` varchar(250) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_title`, `menu_order`, `link`, `parent_id`) VALUES
(28, 'BA. LLB.', 1, 'http://localhost/kslnew/program/ba-llb', 27),
(27, 'Academics', 1, '', 0),
(26, 'Home', 0, 'http://localhost/kslnew/', 0),
(29, 'LLM', 2, 'http://localhost/kslnew/program/llm', 27),
(30, 'M.A', 4, 'http://localhost/kslnew/program/m-a', 27),
(31, 'MHRD', 3, 'http://localhost/kslnew/program/mhrd', 27),
(32, 'Special Programs', 5, 'http://localhost/kslnew/program/special-programs', 27),
(33, 'Academic Calendar', 6, '', 27),
(34, 'Handbook and Syllabus', 7, 'http://localhost/kslnew/page/handbook-and-syllabus', 27),
(35, 'APMA', 8, 'http://localhost/kslnew/page/apma', 27),
(36, 'Scholarships', 9, 'http://localhost/kslnew/page/scholarships', 27);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_heading` varchar(250) NOT NULL,
  `news_description` text NOT NULL,
  `news_published_date` date NOT NULL,
  `page_url` varchar(250) NOT NULL,
  `page_slug` varchar(250) NOT NULL,
  `news_image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_heading`, `news_description`, `news_published_date`, `page_url`, `page_slug`, `news_image`) VALUES
(1, 'New Website Launched', '<p>\n  Dear Readers, Our Store has Launched its website. You can buy products online now.</p>\n', '2018-11-08', 'http://localhost/kslnew/news/new-website-launched', 'new-website-launched', ''),
(2, 'Dashain Offer of 50 %', '<p>\n We have decided to give a dashain offer upto 50 %. Be sure to check in at products in our online store</p>\n', '2018-11-14', 'http://localhost/kslnew/news/dashain-offer-of-50-', 'dashain-offer-of-50-', 'ec420-ksl3.jpg'),
(3, 'New Macbook Air and its Features', '<p>\n  Here we have enlisted the feature of new Mac Book Air, The beautiful beast.</p>\n', '2018-12-28', 'http://localhost/kslnew/news/new-macbook-air-and-its-features', 'new-macbook-air-and-its-features', 'c7677-ksl4.jpg'),
(4, 'Alienware Will be available in our store soon', '<p>\n Gamebuddys be ready the new alienware is on its way. Check out the new features on the amazing series.</p>\n', '2018-11-15', 'http://localhost/kslnew/news/alienware-will-be-available-in-our-store-soon', 'alienware-will-be-available-in-our-store-soon', 'd6339-ksl1.gif'),
(5, 'KSL announces football team', '<p>\n KSL announces football team</p>\n', '2018-11-09', 'http://localhost/kslnew/news/ksl-announces-football-team', 'ksl-announces-football-team', '55905-ksl1.gif'),
(6, 'Just a test news', '<p>\n  Yet another test news</p>\n', '2018-12-21', 'http://localhost/kslnew/news/just-a-test-news', 'just-a-test-news', 'bdc03-ksl3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `notice_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_date` date NOT NULL,
  `notice_file` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_type` enum('General','ESDR','KSLR','') COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`notice_id`, `title`, `description`, `notice_date`, `notice_file`, `notice_type`, `notice_image`, `page_url`, `page_slug`) VALUES
(1, 'ESDR program announced', '<p>\n  ESDR program announced</p>\n', '2018-11-22', 'e90ff-artsfon.com-73713-1-.jpg', 'General', 'b93eb-ksl1.gif', 'http://localhost/kslnew/notice/esdr-program-announced', 'esdr-program-announced'),
(2, 'KSLR Program Announced', '<p>\n  KSLR Program Announced</p>\n', '2018-11-23', 'de5fb-0266554465.jpeg', 'General', '117e6-ksl2.jpg', 'http://localhost/kslnew/notice/kslr-program-announced', 'kslr-program-announced'),
(3, 'College Opens on Paush', '<p>\n  College Opens on Paush</p>\n', '2018-11-17', '7b6e6-0266554465.jpeg', 'General', '5b519-artsfon.com-73713-1-.jpg', 'http://localhost/kslnew/notice/college-opens-on-paush', 'college-opens-on-paush'),
(4, 'College Opens on Paush 1', '<p>\n  College Opens on Paush</p>\n', '2018-11-17', '7b6e6-0266554465.jpeg', 'General', 'b6674-550x400.jpg', 'http://localhost/kslnew/notice/college-opens-on-paush-1', 'college-opens-on-paush-1'),
(5, 'College Opens on Paush 2', '<p>\n  College Opens on Paush</p>\n', '2018-11-17', '7b6e6-0266554465.jpeg', 'General', 'a6171-0266554465.jpeg', 'http://localhost/kslnew/notice/college-opens-on-paush-2', 'college-opens-on-paush-2'),
(6, 'Just a test 1', '<p>\n Just a test 1</p>\n', '2018-12-20', '8a3fa-270x150x4.jpg', 'ESDR', '34f64-270x150x4.jpg', 'http://localhost/kslnew/notice/just-a-test-1', 'just-a-test-1'),
(7, 'Just a test 2', '<p>\n Just a test 2</p>\n', '2018-12-20', 'e100c-550x400.jpg', 'ESDR', 'b1947-550x400.jpg', 'http://localhost/kslnew/notice/just-a-test-2', 'just-a-test-2'),
(8, 'Just a test 2', '<p>\r\n Just a test 2</p>\r\n', '2018-12-20', 'e100c-550x400.jpg', 'ESDR', 'b1947-550x400.jpg', 'http://localhost/kslnew/notice/just-a-test-2', 'just-a-test-2'),
(9, 'Just a test 2', '<p>\r\n Just a test 2</p>\r\n', '2018-12-20', 'e100c-550x400.jpg', 'ESDR', 'b1947-550x400.jpg', 'http://localhost/kslnew/notice/just-a-test-2', 'just-a-test-2'),
(10, 'Just a test 2', '<p>\r\n  Just a test 2</p>\r\n', '2018-12-20', 'e100c-550x400.jpg', 'ESDR', 'b1947-550x400.jpg', 'http://localhost/kslnew/notice/just-a-test-2', 'just-a-test-2'),
(11, 'KSLR has just been published now', '<p>\n KSLR has just been published now</p>\n', '2018-12-21', '11947-0266554465.jpeg', 'KSLR', '1cf11-0266554465.jpeg', 'http://localhost/kslnew/notice/kslr-has-just-been-published-now', 'kslr-has-just-been-published-now'),
(12, 'KSLR has been successful', '<p>\n KSLR has been successful</p>\n', '2018-12-20', '672b5-artsfon.com-73713-1-.jpg', 'KSLR', '85870-artsfon.com-73713-1-.jpg', 'http://localhost/kslnew/notice/kslr-has-been-successful', 'kslr-has-been-successful'),
(13, 'Program FIMT and KSL 2019', '<p>\n  Program FIMT and KSL 2019</p>\n', '2018-12-07', '42690-0266554465.jpeg', 'KSLR', '4aff1-0266554465.jpeg', 'http://localhost/kslnew/notice/program-fimt-and-ksl-2019', 'program-fimt-and-ksl-2019'),
(14, 'Program FIMT and KSL 2019 2', '<p>\n  Program FIMT and KSL 2019 2</p>\n', '2018-12-05', '86d0e-ksl1.gif', 'KSLR', 'e4d40-ksl1.gif', 'http://localhost/kslnew/notice/program-fimt-and-ksl-2019-2', 'program-fimt-and-ksl-2019-2'),
(15, 'KSLR program', '<p>\n KSLR program</p>\n', '2018-12-08', 'a14a4-ksl3.jpg', 'KSLR', '5cbb7-ksl2.jpg', 'http://localhost/kslnew/notice/kslr-program', 'kslr-program'),
(16, 'asjkdnsa', '<p>\n asdn</p>\n', '2018-12-31', '', 'General', '', 'http://localhost/kslnew/notice/asjkdnsa', 'asjkdnsa');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  `page_url` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_title`, `page_slug`, `page_content`, `page_url`) VALUES
(16, 'Message From Professor Incharge', 'message-from-professor-incharge', '<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam diam lectus, condimentum id tempus nec, volutpat pretium sem. In commodo lacus in vehicula molestie. Donec ac porta velit, id sagittis felis. Phasellus non magna a magna commodo maximus nec eu metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin eleifend, erat nec fermentum tristique, lectus urna porta ante, et volutpat ante justo eu diam. Aenean nec sapien fringilla, aliquam risus vel, eleifend ligula.</p>\n<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n  <img alt=\"sagun2\" src=\"http://localhost/kslnew/assets/uploads/sagun2.jpg\" style=\"height: 267px; width: 200px; float: left; margin-left: 20px; margin-right: 20px;\" /></p>\n<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n  Suspendisse potenti. Pellentesque id lorem non ex suscipit semper. Maecenas lobortis volutpat porttitor. Quisque ligula ante, auctor et facilisis nec, consectetur ut nibh. Proin malesuada, eros a rhoncus convallis, arcu nisi porttitor lectus, nec blandit magna risus quis libero. Praesent ullamcorper sodales erat, quis luctus purus dapibus vel. Quisque interdum, quam non vehicula tempor, mi sem euismod est, sed ullamcorper quam mauris a quam. Donec ullamcorper metus tortor, sed fringilla nibh dictum at. Etiam porttitor vitae orci nec hendrerit. Sed sed neque sit amet enim efficitur posuere non vitae magna. Phasellus orci tellus, pellentesque eu interdum ut, faucibus sed nibh. Donec nisl lorem, malesuada condimentum congue in, suscipit a ipsum. Ut convallis, purus quis feugiat dapibus, purus felis vehicula risus, nec posuere mi elit ut erat. Quisque mollis, ex at porttitor efficitur, tortor quam feugiat enim, eget sagittis eros ante sed justo.</p>\n<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n  Vestibulum vitae mauris mi. Vestibulum placerat eros eu magna ornare porttitor. Sed id ipsum mauris. Nunc venenatis mattis augue, quis iaculis nisi ultricies iaculis. Etiam vitae sem vitae dolor tristique tincidunt. Suspendisse iaculis vel odio nec aliquet. Nulla elementum molestie lorem a egestas.</p>\n<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n  Vestibulum vitae mauris mi. Vestibulum placerat eros eu magna ornare porttitor. Sed id ipsum mauris. Nunc venenatis mattis augue, quis iaculis nisi ultricies iaculis. Etiam vitae sem vitae dolor tristique tincidunt. Suspendisse iaculis vel odio nec aliquet. Nulla elementum molestie lorem a egestas.</p>\n', 'http://localhost/kslnew/page/message-from-professor-incharge'),
(17, 'Handbook and Syllabus', 'handbook-and-syllabus', '<p>\n Handbook and Syllabus</p>\n', 'http://localhost/kslnew/page/handbook-and-syllabus'),
(18, 'APMA', 'apma', '<p>\n APMA</p>\n', 'http://localhost/kslnew/page/apma'),
(19, 'Scholarships', 'scholarships', '<p>\n Scholarships</p>\n', 'http://localhost/kslnew/page/scholarships');

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `program_id` int(11) NOT NULL,
  `program_title` varchar(250) NOT NULL,
  `program_duration` varchar(250) NOT NULL,
  `program_type` enum('Masters','Bachelors','Others') NOT NULL,
  `program_desc` longtext NOT NULL,
  `page_url` varchar(250) NOT NULL,
  `page_slug` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`program_id`, `program_title`, `program_duration`, `program_type`, `program_desc`, `page_url`, `page_slug`) VALUES
(2, 'BA LLB', '5 years', 'Bachelors', '<p>\n  <span font-size:=\"\" helvetica=\"\" style=\"color: rgb(51, 51, 51); font-family: \">KSL offers five-year B.A. LL.B. course after completing +2 or intermediate course in any stream. The course comprises of the studies on wide areas of law. In view of necessity to relate the study of law with wide range of professionalism, social science and technology, the course of B.A. LL.B. is designed accordingly. Hence, in the initial years, the first and second year, students get familiarized about basic principles of political science, economics, sociology and management. Their significance in law is demonstrated. Besides, in these years, fundamental principles and concepts of law, legislative principles, and theories of criminal and procedural laws are discussed.&nbsp; In the third year, students are exposed to advanced theories and principles of laws, such as trail advocacy, ethics, constitutionalism, etc. In the fourth and fifth year, the course intends to generate specialized knowledge of different domains of legal science.</span></p>\n<p>\n <img alt=\"tsagun\" src=\"http://localhost/kslnew/assets/uploads/tsagun.jpg\" style=\"height: 200px; width: 200px;\" /></p>\n<p>\n  <span font-size:=\"\" helvetica=\"\" style=\"color: rgb(51, 51, 51); font-family: \">KSL offers five-year B.A. LL.B. course after completing +2 or intermediate course in any stream. The course comprises of the studies on wide areas of law. In view of necessity to relate the study of law with wide range of professionalism, social science and technology, the course of B.A. LL.B. is designed accordingly. Hence, in the initial years, the first and second year, students get familiarized about basic principles of political science, e</span></p>\n', 'http://localhost/kslnew/program/ba-llb', 'ba-llb'),
(3, 'MHRD', '1 year', 'Masters', '<p>\n <span font-size:=\"\" helvetica=\"\" style=\"color: rgb(51, 51, 51); font-family: \">Kathmandu School of Law (KSL) has been a part of an international collaboration to develop a new Master&rsquo;s Programme in Human Rights and Democratisation (Asia Pacific) &ndash;The Course commenced since 2010 consisting a foundational semester at the University of Sydney and a second semester at the partner Universities i.e. Kathmandu School of Law, Nepal, GadjahMada University, Indonesia and University of Colombo, Sri Lanka, Mahidol University, Thailand and Ateneo Law University, Philippines[1]. Since 2016-17 cohort, the foundational semester has been shifted from Sydney University to Institute of Human Rights and Peace Studies (IHRP), Mahidol University, Thailand.Mahidol University, Thailand. Since then, the University of Sydney has remained an associated partner.</span></p>\n', 'http://localhost/kslnew/program/mhrd', 'mhrd'),
(4, 'LLM', '2 years', 'Masters', '<p>\n <span font-size:=\"\" helvetica=\"\" style=\"color: rgb(51, 51, 51); font-family: \">The LL.M. in Human Rights and Gender Justice has been recognized as first of its kind both in the nation and in South Asia as it offers a specialized and comprehensive course comprising of wider issues on the subject matter. It is designed to introduce students with Nepalese jurisprudence on the Human Rights and Gender Justice and to generate proficient lawyers with adequate knowledge and skills of applying international human rights standards in domestic situation. The program runs its academic and research activities through the Human Rights and Humanitarian Law Center.</span></p>\n', 'http://localhost/kslnew/program/llm', 'llm'),
(5, 'M.A', '2 years', 'Masters', '<p>\n <span style=\"color: rgb(51, 51, 51);\">KSL offers five-year B.A. LL.B. course after completing +2 or intermediate course in any stream. The course comprises of the studies on wide areas of law. In view of necessity to relate the study of law with wide range of professionalism, social science and technology, the course of B.A. LL.B. is designed accordingly. Hence, in the initial years, the first and second year, students get familiarized about basic principles of political science, economics, sociology and management. Their significance in law is demonstrated. Besides, in these years, fundamental principles and concepts of law, legislative principles, and theories of criminal and procedural laws are discussed.&nbsp; In the third year, students are exposed to advanced theories and principles of laws, such as trail advocacy, ethics, constitutionalism, etc. In the fourth and fifth year, the course intends to generate specialized knowledge of different domains of legal science.</span></p>\n', 'http://localhost/kslnew/program/m-a', 'm-a'),
(6, 'Special Programs', '2 years', 'Others', '<div>\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin congue euismod est vitae eleifend. Duis lobortis egestas pharetra. Nam in nunc interdum, placerat felis at, vestibulum velit. Integer tincidunt eros vel suscipit tincidunt. Etiam tincidunt eros eget justo aliquam iaculis. In aliquam odio non nulla ullamcorper, sed pretium elit tincidunt. Suspendisse ligula eros, molestie fringilla rutrum non, mattis sit amet ipsum. In turpis massa, egestas at orci aliquet, ullamcorper eleifend est. Aliquam dapibus nec metus ac consequat. Fusce mattis sapien in orci rutrum dictum. Duis sit amet sapien congue, finibus felis at, bibendum nisl.</div>\n<div>\n  &nbsp;</div>\n<div>\n Sed elementum pulvinar feugiat. Aliquam vitae auctor turpis, non maximus metus. Quisque id tempor nisl. Vivamus pharetra velit vel tincidunt laoreet. Proin condimentum facilisis volutpat. Donec dapibus erat at pharetra consectetur. Suspendisse vel risus varius nunc dictum cursus. Vivamus vulputate bibendum ipsum at viverra. Etiam risus velit, commodo sed nunc vel, pellentesque venenatis augue. Nulla imperdiet feugiat sem eu faucibus. Donec commodo est sit amet nulla blandit interdum sit amet vitae est. Cras sit amet lacus sit amet lacus imperdiet sollicitudin eu lobortis nibh. Morbi mattis ut nulla vitae pulvinar. Maecenas lobortis dictum hendrerit. Quisque ligula lacus, vehicula nec eros in, convallis ornare neque. Vivamus risus nibh, cursus pellentesque dolor ut, facilisis fringilla odio.</div>\n', 'http://localhost/kslnew/program/special-programs', 'special-programs');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL,
  `slider_title` varchar(255) NOT NULL,
  `slider_subheading` varchar(255) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `slider_url` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_title`, `slider_subheading`, `slider_image`, `slider_url`) VALUES
(4, 'About Kathmandu School of Law', '', 'c4d3e-ksl1.gif', 'http://google.com'),
(5, 'The Kathmandu Dialouge', '', 'b15c9-ksl3.jpg', 'http://ksl.edu.np'),
(6, 'Another test Slider For KSL', '', '63916-ksl2.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_by` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonial_id`, `testimonial_title`, `testimonial_by`, `testimonial_description`, `testimonial_image`) VALUES
(1, 'Lorem ipsum dolor sit amet', 'Sagun Siwakoti', '<p>\n  <span style=\"color: rgb(33, 37, 41); font-family: Catamaran, sans-serif; font-size: 14px; text-align: center;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>\n', 'c30ca-0266554465.jpeg'),
(2, 'Lorem ipsum dolor sit amet', 'Sagun Siwakoti', '<p>\n  <span style=\"color: rgb(33, 37, 41); font-family: Catamaran, sans-serif; font-size: 14px; text-align: center;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>\n', '38203-tsagun1.jpg'),
(3, 'Lorem ipsum dolor sit amet', 'Sagun Siwakoti', '<p>\r\n  <span style=\"color: rgb(33, 37, 41); font-family: Catamaran, sans-serif; font-size: 14px; text-align: center;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>\r\n', '38203-tsagun1.jpg'),
(4, 'Lorem ipsum dolor sit amet', 'Sagun Siwakoti', '<p>\r\n  <span style=\"color: rgb(33, 37, 41); font-family: Catamaran, sans-serif; font-size: 14px; text-align: center;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>\r\n', 'c30ca-0266554465.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$uA399cm3NhoUkqXUla5fK.GFk405MrJUpaBqdsoqdw1Rskhn5txRe', '', 'info@ksl.edu.np', '', NULL, NULL, NULL, 1268889823, 1546005059, 1, 'Admin', 'istrator', 'ADMIN', '9841546900');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(6, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `video_id` int(11) NOT NULL,
  `video_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_published_date` date NOT NULL,
  `is_homepage` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `video_thumbnail` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`video_id`, `video_name`, `video_url`, `video_description`, `video_published_date`, `is_homepage`, `video_thumbnail`) VALUES
(1, 'Hanuman Chalisa', 'https://www.youtube.com/embed/sX2bYV6nSy4', '<p>\n  <span style=\"color: rgb(17, 17, 17); font-family: Roboto, Arial, sans-serif; font-size: 14px; white-space: pre-wrap;\">Watch the Official Video of Shekhar Ravjiani&#39;s Hanuman Chalisa Full Song Director: Vishal Punjabi (The Wedding Filmer) Visual Promotions: Ravi Padda &amp; Binny Padda ( Pentacle Creationz ) Project Head &amp; Designs: Kanchana Sawant Recording &amp; Mixing: Sachit Harve</span></p>\n', '2018-11-15', 'yes', '25e73-ksl1.gif'),
(2, 'Hanuman Calisa New', 'https://www.youtube.com/embed/sX2bYV6nSy4', '<p>\n <span style=\"color: rgb(17, 17, 17); font-family: Roboto, Arial, sans-serif; font-size: 14px; white-space: pre-wrap;\">Watch the Official Video of Shekhar Ravjiani&#39;s Hanuman Chalisa Full Song Director: Vishal Punjabi (The Wedding Filmer) Visual Promotions: Ravi Padda &amp; Binny Padda ( Pentacle Creationz ) Project Head &amp; Designs: Kanchana Sawant</span></p>\n', '2018-11-17', 'yes', '7801d-ksl1.gif'),
(3, 'Hanuman Calisa New', 'https://www.youtube.com/embed/sX2bYV6nSy4', '<p>\n <span style=\"color: rgb(17, 17, 17); font-family: Roboto, Arial, sans-serif; font-size: 14px; white-space: pre-wrap;\">Watch the Official Video of Shekhar Ravjiani&#39;s Hanuman Chalisa Full Song Director: Vishal Punjabi (The Wedding Filmer) Visual Promotions: Ravi Padda &amp; Binny Padda ( Pentacle Creationz ) Project Head &amp; Designs: Kanchana Sawant</span></p>\n', '2018-11-17', 'yes', '27d2d-ksl3.jpg'),
(4, 'Hanuman Calisa New', 'https://www.youtube.com/embed/sX2bYV6nSy4', '<p>\n <span style=\"color: rgb(17, 17, 17); font-family: Roboto, Arial, sans-serif; font-size: 14px; white-space: pre-wrap;\">Watch the Official Video of Shekhar Ravjiani&#39;s Hanuman Chalisa Full Song Director: Vishal Punjabi (The Wedding Filmer) Visual Promotions: Ravi Padda &amp; Binny Padda ( Pentacle Creationz ) Project Head &amp; Designs: Kanchana Sawant</span></p>\n', '2018-11-17', 'yes', '66607-ksl1.gif'),
(5, 'Hanuman Calisa New', 'https://www.youtube.com/embed/sX2bYV6nSy4', '<p>\n <span style=\"color: rgb(17, 17, 17); font-family: Roboto, Arial, sans-serif; font-size: 14px; white-space: pre-wrap;\">Watch the Official Video of Shekhar Ravjiani&#39;s Hanuman Chalisa Full Song Director: Vishal Punjabi (The Wedding Filmer) Visual Promotions: Ravi Padda &amp; Binny Padda ( Pentacle Creationz ) Project Head &amp; Designs: Kanchana Sawant</span></p>\n', '2018-11-17', 'yes', 'a031f-ksl3.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`alumni_id`);

--
-- Indexes for table `collaborations`
--
ALTER TABLE `collaborations`
  ADD PRIMARY KEY (`collaboration_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `equipments`
--
ALTER TABLE `equipments`
  ADD PRIMARY KEY (`equipment_id`);

--
-- Indexes for table `equipment_categories`
--
ALTER TABLE `equipment_categories`
  ADD PRIMARY KEY (`equipment_category_id`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`result_id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD UNIQUE KEY `file_id` (`file_id`);

--
-- Indexes for table `financials`
--
ALTER TABLE `financials`
  ADD PRIMARY KEY (`financial_id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD UNIQUE KEY `folder_id` (`folder_id`),
  ADD UNIQUE KEY `folder_name` (`folder_name`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `jobsubmission`
--
ALTER TABLE `jobsubmission`
  ADD PRIMARY KEY (`submissionid`);

--
-- Indexes for table `management_committee`
--
ALTER TABLE `management_committee`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`program_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
  MODIFY `alumni_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `collaborations`
--
ALTER TABLE `collaborations`
  MODIFY `collaboration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `equipments`
--
ALTER TABLE `equipments`
  MODIFY `equipment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `equipment_categories`
--
ALTER TABLE `equipment_categories`
  MODIFY `equipment_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `result_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `faculty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `financials`
--
ALTER TABLE `financials`
  MODIFY `financial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `folder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jobsubmission`
--
ALTER TABLE `jobsubmission`
  MODIFY `submissionid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `management_committee`
--
ALTER TABLE `management_committee`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `program_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;
