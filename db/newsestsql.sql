-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2021 at 07:06 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `neosoftware`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL,
  `client_title` varchar(250) NOT NULL,
  `client_image` varchar(250) NOT NULL,
  `client_description` text NOT NULL,
  `rankorder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `client_title`, `client_image`, `client_description`, `rankorder`) VALUES
(1, 'Client Number 1', '93110-changunarayan-temple-nepal.jpg', '<p>\n	Test</p>\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `collaborations`
--

CREATE TABLE `collaborations` (
  `collaboration_id` int(11) NOT NULL,
  `collaboration_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collaboration_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collaboration_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rankorder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `collaborations`
--

INSERT INTO `collaborations` (`collaboration_id`, `collaboration_title`, `collaboration_image`, `collaboration_description`, `rankorder`) VALUES
(35, 'Sywambhu', '60932-swayambhunath-stupa-kathmandu.jpg', '<p>\n	test</p>\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_subject` varchar(255) NOT NULL,
  `contact_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `contact_name`, `contact_email`, `contact_subject`, `contact_message`) VALUES
(14, 'sushant chapagain', 'thirstyhacker97@gmail.com', '', 'sdasjdkakd'),
(15, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test'),
(16, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'tesst'),
(17, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test'),
(18, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test'),
(19, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `faq_id` int(11) NOT NULL,
  `faq_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faq_description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `faq_title`, `faq_description`) VALUES
(1, 'What should  i do if my interior is broken?', '<p>\n	<span style=\"color: rgb(128, 128, 128); font-family: &quot;nunito sans&quot;, -apple-system, BlinkMacSystemFont, &quot;segoe ui&quot;, Roboto, &quot;helvetica neue&quot;, Arial, sans-serif, &quot;apple color emoji&quot;, &quot;segoe ui emoji&quot;, &quot;segoe ui symbol&quot;, &quot;noto color emoji&quot;; font-size: 16px;\">Lorem ipsum dolor sit amet,&nbsp;</span><a href=\"http://localhost/redcurtain/assets/frontend/#\" style=\"box-sizing: border-box; color: rgb(152, 204, 211); text-decoration-line: none; transition: all 0.3s ease 0s; font-family: &quot;nunito sans&quot;, -apple-system, BlinkMacSystemFont, &quot;segoe ui&quot;, Roboto, &quot;helvetica neue&quot;, Arial, sans-serif, &quot;apple color emoji&quot;, &quot;segoe ui emoji&quot;, &quot;segoe ui symbol&quot;, &quot;noto color emoji&quot;; font-size: 16px;\">Cnsectetur adipisicing</a><span style=\"color: rgb(128, 128, 128); font-family: &quot;nunito sans&quot;, -apple-system, BlinkMacSystemFont, &quot;segoe ui&quot;, Roboto, &quot;helvetica neue&quot;, Arial, sans-serif, &quot;apple color emoji&quot;, &quot;segoe ui emoji&quot;, &quot;segoe ui symbol&quot;, &quot;noto color emoji&quot;; font-size: 16px;\">&nbsp;elit. Eos quos incidunt, perspiciatis, ad saepe, magnam error adipisci vitae ut provident alias! Odio debitis error ipsum molestiae voluptas accusantium quibusdam animi, soluta explicabo asperiores aliquid, modi natus suscipit deleniti. Corrupti, autem.</span></p>\n'),
(2, 'What is your location?', '<p>\n	<span style=\"color: rgb(128, 128, 128); font-family: &quot;nunito sans&quot;, -apple-system, BlinkMacSystemFont, &quot;segoe ui&quot;, Roboto, &quot;helvetica neue&quot;, Arial, sans-serif, &quot;apple color emoji&quot;, &quot;segoe ui emoji&quot;, &quot;segoe ui symbol&quot;, &quot;noto color emoji&quot;; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptate animi hic quasi sequi odio, vitae dolorum soluta sapiente debitis ad similique tempore, aliquam quae nam deserunt dicta ullam perspiciatis minima, quam. Quis repellat corporis aperiam, veritatis nemo iure inventore.</span></p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `file_type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `folder_id` int(11) NOT NULL,
  `file_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`file_id`, `file_name`, `file_type`, `folder_id`, `file_url`, `original_name`) VALUES
(19, 'Swayambhunath-Stupa-Kathmandu', 'jpg', 4, 'http://localhost/neosoftware/assets/uploads/Swayambhunath-Stupa-Kathmandu.jpg', 'Swayambhunath-Stupa-Kathmandu.jpg'),
(18, 'Changunarayan-Temple-Nepal', 'jpg', 4, 'http://localhost/neosoftware/assets/uploads/Changunarayan-Temple-Nepal.jpg', 'Changunarayan-Temple-Nepal.jpg'),
(17, 'pashupatinathj', 'jpg', 4, 'http://localhost/neosoftware/assets/uploads/pashupatinathj.jpg', 'pashupatinathj.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `folder_id` int(11) NOT NULL,
  `folder_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`folder_id`, `folder_name`, `project_id`) VALUES
(4, 'Page Photographs', 0),
(6, 'Notices', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'user', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `infographics`
--

CREATE TABLE `infographics` (
  `info_id` int(11) NOT NULL,
  `numberinfo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labelinfo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logomark` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `infographics`
--

INSERT INTO `infographics` (`info_id`, `numberinfo`, `labelinfo`, `logomark`) VALUES
(1, '79', 'Completed Projects', 'flaticon-briefcase'),
(2, '750', 'Happy Clients', 'flaticon-happy'),
(3, '36', 'Customer Services', 'flaticon-idea'),
(4, '20', 'Answered Questions', 'flaticon-customer-service');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL,
  `job_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_date` date NOT NULL,
  `ending_date` date NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `job_title`, `job_description`, `opening_date`, `ending_date`, `page_slug`, `page_url`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1, 'Jr Web Developer', '<p>\n	Jr Web Developer</p>\n', '2021-01-01', '2021-01-31', 'jr-web-developer', 'http://localhost/neosoftware/jobs/jr-web-developer', '', '', ''),
(2, '', '', '0000-00-00', '0000-00-00', '', 'http://localhost/neosoftware/jobs/', 'Test title', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `jobsubmission`
--

CREATE TABLE `jobsubmission` (
  `submissionid` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_id` int(11) NOT NULL,
  `cvupload` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateofsubmission` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(250) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `link` varchar(250) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `url_type` enum('internal','external') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_title`, `menu_order`, `link`, `parent_id`, `url_type`) VALUES
(75, 'Business Law and Arbitration Research Center', 1, 'page/business-law-and-arbitration-research-center', 74, 'internal'),
(76, 'Center For the Training of Trial Advocacy and Proffesionalism', 2, 'page/center-for-the-training-of-trial-advocacy-and-professionalism', 74, 'internal'),
(77, 'Center of Philosophical and Juristic Discourse', 3, 'page/center-of-philosophical-and-juristic-discourse', 74, 'internal'),
(26, 'Home', 0, 'home', 0, 'internal'),
(91, 'KSL Policy and Legal Research Center', 7, 'page/ksl-policy-and-legal-research-center', 74, 'internal'),
(48, 'Full Time Faculty', 0, 'faculty/full-time', 47, 'internal'),
(49, 'Adjunct Faculty', 1, 'faculty/adjunct-faculty', 47, 'internal'),
(50, 'Visiting Researchers/Fellows', 2, 'faculty/visiting-researchers', 47, 'internal'),
(51, 'Visiting Faculty', 3, 'faculty/visiting-faculty', 47, 'internal'),
(52, 'Faculty in News', 4, 'news/faculty-news', 47, 'internal'),
(54, 'Action Research', 1, 'page/action-research', 53, 'internal'),
(55, 'KSLPRC', 2, 'page/kslprc', 53, 'internal'),
(56, 'Research Department', 3, 'page/research-department', 53, 'internal'),
(84, 'MCD', 4, '', 57, 'internal'),
(85, 'RD', 5, '', 57, 'internal'),
(82, 'CLFSD', 2, '', 57, 'internal'),
(83, 'HRHLD', 3, '', 57, 'internal'),
(81, 'CLD', 1, '', 57, 'internal'),
(72, 'Notices', 1, 'notices/all', 71, 'internal'),
(73, 'Exam & Results', 2, 'exam', 71, 'internal'),
(88, 'Clinical Law Department', 4, 'page/clinical-law-department', 74, 'internal'),
(89, 'Human Rights and Humanitarian Law Department', 5, 'page/human-rights-and-humanitarian-law-department', 74, 'internal'),
(90, 'International Law and Relations Study Center', 6, 'page/international-law-and-relations-study-center', 74, 'internal'),
(92, 'Moot Courts Department', 8, 'page/moot-courts-department', 74, 'internal'),
(93, 'Research Department', 9, 'page/research-department', 74, 'internal'),
(94, 'Criminal Law and Forensic Science Department', 10, 'page/criminal-law-and-forensic-science-department', 74, 'internal'),
(95, 'Human Rights and Criminal Justice Clinic', 11, 'page/human-rights-and-criminal-justice-clinic', 74, 'internal'),
(96, 'Kathmandu School of Law Review ', 12, 'kslr', 74, 'internal'),
(104, 'About Us', 1, 'page/about-us', 0, 'internal');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `notice_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_date` date NOT NULL,
  `notice_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_intro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice_type` enum('notice','news') COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`notice_id`, `title`, `description`, `notice_date`, `notice_image`, `notice_intro`, `notice_type`, `page_slug`, `page_url`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(31, 'Hello world', '<p>\n	Hello world</p>\n', '2021-01-01', '0e80e-pashupatinathj.jpg', 'hello world', 'news', 'hello-world', 'http://localhost/neosoftware/news/hello-world', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_subtitle` varchar(250) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  `page_url` varchar(250) NOT NULL,
  `featured_image` varchar(250) NOT NULL,
  `page_intro` text NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_title`, `page_subtitle`, `page_slug`, `page_content`, `page_url`, `featured_image`, `page_intro`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(43, 'About Neo', 'Leading <span>Financial Consulting</span> Company', 'about-neo', '<div>\n	ITNepal Pvt.Ltd, is a leading software company focused on consulting and software products. IT Nepal is a member company of Golchha Organization established in 2002. Our focus is on providing innovative business solutions that can be delivered quickly and cost effective. We offer various ERP solutions options for you depending on your business requirement.</div>\n<div>\n	&nbsp;</div>\n<div>\n	Synergy ERP solutions can be your one stop solutions to seamlessly integrate your business functions in the local Nepalese context. We have over 50,000 users from 50+ customer organizations, since its inception. The company provides solutions to multiple verticals including manufacturing, banking, insurance, supply chain &amp; logistics, BPO&#39;s, hospitality, government and more. We give our customers a bold competitive edge, &quot;The neoEdge&quot; more so than ever, staying competitive is crucial for success.</div>\n', 'http://localhost/neosoftware/page/about-neo', '8c45e-home-about.png', '<p>\n	ITNepal Pvt.Ltd, is a leading software company focused on consulting and software products. IT Nepal is a member company of Golchha Organization established in 2002</p>\n<p>\n	Our focus is on providing innovative business solutions that can be delivered quickly and cost effective. We offer various ERP solutions options for you depending on your business requirement.</p>\n<p>\n	Synergy ERP solutions can be your one stop solutions to seamlessly integrate your business functions in the local Nepalese context. We have over 50,000 users from 50+ customer organizations, since its inception.</p>\n', 'Test title', 'Test description', 'Test keyword');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `portfolio_id` int(11) NOT NULL,
  `portfolio_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_category_id` int(11) NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`portfolio_id`, `portfolio_name`, `portfolio_description`, `featured_image`, `portfolio_category_id`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(3, 'Development of Neo Software Website', '<p>\n	Development of Neo Software Website</p>\n', 'ce665-changunarayan-temple-nepal.jpg', 2, 'Test title', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_category`
--

CREATE TABLE `portfolio_category` (
  `portfolio_category_id` int(11) NOT NULL,
  `portfolio_category_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolio_category`
--

INSERT INTO `portfolio_category` (`portfolio_category_id`, `portfolio_category_name`, `featured_image`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(2, 'Web Application Development', '169da-changunarayan-temple-nepal.jpg', 'Test title', 'test', 'test'),
(3, 'Mobile Application Development', '71239-swayambhunath-stupa-kathmandu.jpg', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `project_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_category_id` int(11) NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `project_title`, `project_description`, `project_image`, `project_category_id`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(3, 'Product 1', '<p>\n	Product 1</p>\n', '2098b-pashupatinathj.jpg', 5, 'Test title', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `project_category`
--

CREATE TABLE `project_category` (
  `project_category_id` int(11) NOT NULL,
  `project_category_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_category`
--

INSERT INTO `project_category` (`project_category_id`, `project_category_name`, `featured_image`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(5, 'SAP Softwares', '875e2-changunarayan-temple-nepal.jpg', 'Test title', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `resource_id` int(9) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_icon` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_title`, `service_description`, `service_icon`) VALUES
(1, 'Internal Audit & Control Services', '<p>\n	<span courier=\"\" lucida=\"\" style=\"font-family: Consolas, \" white-space:=\"\">Whether you are looking to develop an internal audit function or make an existing one more effective, ASA CA has the experience tools and dedicated full-time resources to help you succeed.</span></p>\n', 'b1dbd-seo_06.png'),
(2, 'Assurance Services', '<p>\n	<span courier=\"\" lucida=\"\" style=\"font-family: Consolas, \" white-space:=\"\">Our accounting member firms can guide you through the often complex financial, operational and compliance decisions you need to make. They have the skills to provide the reassurance that your stakeholders need. </span></p>\n', '8b1fe-seo_02.png'),
(3, 'Taxation Services', '<p>\n	<span courier=\"\" lucida=\"\" style=\"font-family: Consolas, \" white-space:=\"\">While we ensure, that you maintain compliance, we make definitely sure that we create value for your business, giving you the best tax advice &amp; assistance.</span></p>\n', 'e128a-seo_03.png'),
(4, 'Company Secretarial Services', '<p>\n	Our company secretarial support service is designed to help private companies meet their standard statutory compliance needs.</p>\n', '6ef4f-seo_04.png'),
(5, 'Advisory & Other Services', '<div>\n	Our company Advisoru and Other services is designed to help private companies meet their standard statutory compliance needs via advisory and counselling.</div>\n<div>\n	&nbsp;</div>\n', 'f2230-seo_05.png'),
(6, 'Trainings/Workshops', '<p>\n	Our company provide trainings and workshops to various professional and beginners via a group of highly skilled Chartered Accountant and specialists in the area of accounting.</p>\n', 'da812-seo_01.png');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `slider_title` varchar(250) NOT NULL,
  `slider_subtitle` text NOT NULL,
  `slider_topcaptain` varchar(250) NOT NULL,
  `slider_link` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_image`, `slider_title`, `slider_subtitle`, `slider_topcaptain`, `slider_link`) VALUES
(2, '5dc70-intro-1.jpg', 'Clever <span>Investing</span> Make Your Money Grow', '<p>\n	Capitalise on low hanging fruit to identify a ballpark<br />\n	value added activity to beta test. Override the digital divide<br />\n	with additional base clickthroughs.</p>\n', 'Consulte Company', 'https://google.com'),
(3, '9cc1f-intro-1.jpg', 'Clever <span>Investing</span> Make Your Money Grow', '<p>\n	Capitalise on low hanging fruit to identify a ballpark<br />\n	value added activity to beta test. Override the digital divide<br />\n	with additional base clickthroughs.</p>\n', 'Consulte Company', 'https://youtube.com');

-- --------------------------------------------------------

--
-- Table structure for table `socialmedia`
--

CREATE TABLE `socialmedia` (
  `socialmedia_id` int(11) NOT NULL,
  `socialmedia_title` varchar(250) NOT NULL,
  `socialmedia_icon` varchar(250) NOT NULL,
  `socialmedia_image` varchar(250) NOT NULL,
  `socialmedia_link` varchar(250) NOT NULL,
  `socialmedia_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `socialmedia`
--

INSERT INTO `socialmedia` (`socialmedia_id`, `socialmedia_title`, `socialmedia_icon`, `socialmedia_image`, `socialmedia_link`, `socialmedia_order`) VALUES
(1, 'Facebook', 'icofont-facebook', '', 'https://www.facebook.com/', 1),
(2, 'Twitter', 'icofont-twitter', '', 'https://google.com/', 2),
(3, 'Instagram', 'icofont-instagram', '', 'https://www.instagram.com/', 3);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`team_id`, `name`, `designation`, `info`, `image`, `team_order`) VALUES
(21, 'Sagun Siwakoti', 'Sr Web Developer', '<p>\n	7 years of PHP&nbsp;</p>\n', '5bf1b-changunarayan-temple-nepal.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_by` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonial_id`, `testimonial_title`, `testimonial_by`, `testimonial_description`, `testimonial_image`) VALUES
(7, 'Great Company', 'Sagun Siwakoti', '<p>\n	The company is great</p>\n', '461c8-pashupatinathj.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$uA399cm3NhoUkqXUla5fK.GFk405MrJUpaBqdsoqdw1Rskhn5txRe', '', 'info@neosoftware.com.np', '', NULL, NULL, NULL, 1268889823, 1611305791, 1, 'Admin', 'istrator', 'ADMIN', '9841546900');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(6, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `collaborations`
--
ALTER TABLE `collaborations`
  ADD PRIMARY KEY (`collaboration_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD UNIQUE KEY `file_id` (`file_id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD UNIQUE KEY `folder_id` (`folder_id`),
  ADD UNIQUE KEY `folder_name` (`folder_name`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infographics`
--
ALTER TABLE `infographics`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `jobsubmission`
--
ALTER TABLE `jobsubmission`
  ADD PRIMARY KEY (`submissionid`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`portfolio_id`);

--
-- Indexes for table `portfolio_category`
--
ALTER TABLE `portfolio_category`
  ADD PRIMARY KEY (`portfolio_category_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `project_category`
--
ALTER TABLE `project_category`
  ADD PRIMARY KEY (`project_category_id`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`resource_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `socialmedia`
--
ALTER TABLE `socialmedia`
  ADD PRIMARY KEY (`socialmedia_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `collaborations`
--
ALTER TABLE `collaborations`
  MODIFY `collaboration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `folder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `infographics`
--
ALTER TABLE `infographics`
  MODIFY `info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jobsubmission`
--
ALTER TABLE `jobsubmission`
  MODIFY `submissionid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `portfolio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `portfolio_category`
--
ALTER TABLE `portfolio_category`
  MODIFY `portfolio_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_category`
--
ALTER TABLE `project_category`
  MODIFY `project_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
  MODIFY `resource_id` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `socialmedia`
--
ALTER TABLE `socialmedia`
  MODIFY `socialmedia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;
