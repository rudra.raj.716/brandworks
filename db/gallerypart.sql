-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2020 at 07:24 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `redcurtain`
--

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `photo_id` int(11) NOT NULL,
  `photo_file` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_order` int(11) NOT NULL,
  `portfolio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`photo_id`, `photo_file`, `image_order`, `portfolio_id`) VALUES
(1, '7af41-burger2.jpg', 1, 1),
(2, 'df4b6-burger3.jpg', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `youtube_gallery`
--

CREATE TABLE `youtube_gallery` (
  `youtube_id` int(11) NOT NULL,
  `youtube_link` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_id` int(11) NOT NULL,
  `image_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `youtube_gallery`
--

INSERT INTO `youtube_gallery` (`youtube_id`, `youtube_link`, `portfolio_id`, `image_order`) VALUES
(1, 'https://www.youtube.com/embed/5z75WLmKfX', 1, 1),
(2, 'https://www.youtube.com/embed/ueSp6ipcXZY', 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `youtube_gallery`
--
ALTER TABLE `youtube_gallery`
  ADD PRIMARY KEY (`youtube_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `youtube_gallery`
--
ALTER TABLE `youtube_gallery`
  MODIFY `youtube_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;
