-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2020 at 01:03 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `asaca`
--

-- --------------------------------------------------------

--
-- Table structure for table `collaborations`
--

CREATE TABLE `collaborations` (
  `collaboration_id` int(11) NOT NULL,
  `collaboration_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collaboration_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collaboration_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rankorder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `collaborations`
--

INSERT INTO `collaborations` (`collaboration_id`, `collaboration_title`, `collaboration_image`, `collaboration_description`, `rankorder`) VALUES
(33, '', '09660-logo_01.png', '', 0),
(34, '', '9746f-logo_02.png', '', 0),
(35, '', '9c724-logo_03.png', '', 0),
(36, '', 'e3495-logo_04.png', '', 0),
(37, '', 'd1e8b-logo_05.png', '', 0),
(38, '', 'b52fb-logo_06.png', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_subject` varchar(255) NOT NULL,
  `contact_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `contact_name`, `contact_email`, `contact_subject`, `contact_message`) VALUES
(1, 'Bishal Mahat', 'sagunsiwakoti25@gmail.com', 'test message', 'test'),
(2, 'Bishal Mahat', 'sagunsiwakoti25@gmail.com', 'test message', 'test'),
(3, 'Bishal Mahat', 'sagunsiwakoti25@gmail.com', 'test message', 'test'),
(4, 'Bishal Mahat', 'sagunsiwakoti25@gmail.com', 'test message', 'test'),
(5, 'Bishal Mahat', 'sagunsiwakoti25@gmail.com', 'application as news editor', 'just another test message'),
(6, 'Bishal Mahat', 'sagunsiwakoti25@gmail.com', 'application as news editor', 'just another test message'),
(7, 'Bishal Mahat', 'sagunsiwakoti25@gmail.com', 'application as news editor', 'just another test message'),
(8, 'Bishal Mahat', 'admin@admin.com', 'application as news editor', 'just a test message'),
(9, 'sagun siwakoti', 'sagunsiwakoti25@gmail.com', 'just a test', 'just a test');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `file_type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `folder_id` int(11) NOT NULL,
  `file_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `folder_id` int(11) NOT NULL,
  `folder_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`folder_id`, `folder_name`, `project_id`) VALUES
(4, 'Page Photographs', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'user', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `infographics`
--

CREATE TABLE `infographics` (
  `info_id` int(11) NOT NULL,
  `numberinfo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labelinfo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `infographics`
--

INSERT INTO `infographics` (`info_id`, `numberinfo`, `labelinfo`) VALUES
(1, '79', 'Complated Projects'),
(2, '750', 'Happy Clients'),
(3, '36', 'Customer Services'),
(4, '20', 'Answered Questions');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(250) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `link` varchar(250) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `url_type` enum('internal','external') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_title`, `menu_order`, `link`, `parent_id`, `url_type`) VALUES
(75, 'Business Law and Arbitration Research Center', 1, 'page/business-law-and-arbitration-research-center', 74, 'internal'),
(76, 'Center For the Training of Trial Advocacy and Proffesionalism', 2, 'page/center-for-the-training-of-trial-advocacy-and-professionalism', 74, 'internal'),
(77, 'Center of Philosophical and Juristic Discourse', 3, 'page/center-of-philosophical-and-juristic-discourse', 74, 'internal'),
(27, 'Mission & Vision', 2, 'page/mission-and-vision', 0, 'internal'),
(26, 'Home', 0, '', 0, 'internal'),
(91, 'KSL Policy and Legal Research Center', 7, 'page/ksl-policy-and-legal-research-center', 74, 'internal'),
(40, 'About', 1, 'page/about-us', 0, 'internal'),
(48, 'Full Time Faculty', 0, 'faculty/full-time', 47, 'internal'),
(49, 'Adjunct Faculty', 1, 'faculty/adjunct-faculty', 47, 'internal'),
(50, 'Visiting Researchers/Fellows', 2, 'faculty/visiting-researchers', 47, 'internal'),
(51, 'Visiting Faculty', 3, 'faculty/visiting-faculty', 47, 'internal'),
(52, 'Faculty in News', 4, 'news/faculty-news', 47, 'internal'),
(54, 'Action Research', 1, 'page/action-research', 53, 'internal'),
(55, 'KSLPRC', 2, 'page/kslprc', 53, 'internal'),
(56, 'Research Department', 3, 'page/research-department', 53, 'internal'),
(84, 'MCD', 4, '', 57, 'internal'),
(85, 'RD', 5, '', 57, 'internal'),
(82, 'CLFSD', 2, '', 57, 'internal'),
(83, 'HRHLD', 3, '', 57, 'internal'),
(63, 'Resources', 6, 'resources', 0, 'internal'),
(65, 'Contact', 7, 'contact', 0, 'internal'),
(81, 'CLD', 1, '', 57, 'internal'),
(71, 'Notices', 4, 'notices', 0, 'internal'),
(72, 'Notices', 1, 'notices/all', 71, 'internal'),
(73, 'Exam & Results', 2, 'exam', 71, 'internal'),
(88, 'Clinical Law Department', 4, 'page/clinical-law-department', 74, 'internal'),
(89, 'Human Rights and Humanitarian Law Department', 5, 'page/human-rights-and-humanitarian-law-department', 74, 'internal'),
(90, 'International Law and Relations Study Center', 6, 'page/international-law-and-relations-study-center', 74, 'internal'),
(92, 'Moot Courts Department', 8, 'page/moot-courts-department', 74, 'internal'),
(93, 'Research Department', 9, 'page/research-department', 74, 'internal'),
(94, 'Criminal Law and Forensic Science Department', 10, 'page/criminal-law-and-forensic-science-department', 74, 'internal'),
(95, 'Human Rights and Criminal Justice Clinic', 11, 'page/human-rights-and-criminal-justice-clinic', 74, 'internal'),
(96, 'Kathmandu School of Law Review ', 12, 'kslr', 74, 'internal');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `notice_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_date` date NOT NULL,
  `notice_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`notice_id`, `title`, `description`, `notice_date`, `notice_image`, `page_url`, `page_slug`) VALUES
(27, 'Notice No 1', '<p>\n	Notice No 1</p>\n', '2020-03-28', '4b3ce-seo_01.png', 'http://localhost/acharyaca/notice/notice-no-1', 'notice-no-1'),
(28, 'Notice No 2', '<p>\n	Notice No 2</p>\n', '2020-03-27', 'f2e28-seo_02.png', 'http://localhost/acharyaca/notice/notice-no-2', 'notice-no-2');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  `page_url` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_title`, `page_slug`, `page_content`, `page_url`) VALUES
(43, 'About Us', 'about-us', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 20px; color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">\n	Quisque eget nisl id nulla sagittis auctor quis id. Aliquam quis vehicula enim, non aliquam risus. Sed a tellus quis mi honcus dignissim.</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 20px; color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">\n	Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum sed elit a ex ullamcorper gravida. Integer lorem libero, hendrerit sed hendrerit in, molestie quis urna. Nullam sed sapien at nisi mollis blandit porttitor at dolor. Suspendisse fermentum vestibulum iaculis. Quisque tempor sagittis bibendum. Quisque pulvinar dui vitae fringilla euismod. Integer gravida commodo nisl a feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>\n', 'http://localhost/acharyaca/page/about-us'),
(44, 'Mission and Vision', 'mission-and-vision', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 20px; color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">\n	Quisque eget nisl id nulla sagittis auctor quis id. Aliquam quis vehicula enim, non aliquam risus. Sed a tellus quis mi honcus dignissim.</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 20px; color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">\n	Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum sed elit a ex ullamcorper gravida. Integer lorem libero, hendrerit sed hendrerit in, molestie quis urna. Nullam sed sapien at nisi mollis blandit porttitor at dolor. Suspendisse fermentum vestibulum iaculis. Quisque tempor sagittis bibendum. Quisque pulvinar dui vitae fringilla euismod. Integer gravida commodo nisl a feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>\n', 'http://localhost/acharyaca/page/mission-and-vision');

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `resource_id` int(9) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` char(15) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`resource_id`, `name`, `file`) VALUES
(18, 'File For CA Application', 'a86dd-beingawri'),
(19, 'Form For Company Registration', 'a33b9-abroadstu');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_title`, `service_description`, `service_image`) VALUES
(1, 'Search Engine Optimization', '<p>\n	<span courier=\"\" lucida=\"\" style=\"font-family: Consolas, \" white-space:=\"\">Aliquam sagittis ligula et sem lacinia, ut facilisis enim sollicitudin. Proin nisi est, convallis nec purus vitae, iaculis posuere sapien. Cum sociis natoque.</span></p>\n', '09ce6-seo_01.png'),
(2, 'Email Marketing', '<p>\n	<span style=\"font-family: Consolas, &quot;Lucida Console&quot;, &quot;Courier New&quot;, monospace; white-space: pre-wrap;\">Duis at tellus at dui tincidunt scelerisque nec sed felis. Suspendisse id dolor sed leo rutrum euismod. Nullam vestibulum fermentum erat. It nam auctor. </span></p>\n', '8b1fe-seo_02.png'),
(3, 'Pay Per Click', '<p>\n	<span style=\"font-family: Consolas, &quot;Lucida Console&quot;, &quot;Courier New&quot;, monospace; white-space: pre-wrap;\">Etiam materials ut mollis tellus, vel posuere nulla. Etiam sit amet lacus vitae massa sodales aliquam at eget quam. Integer ultricies et magna quis accumsan.</span></p>\n', 'e128a-seo_03.png');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL,
  `slider_title` varchar(255) NOT NULL,
  `slider_subheading` varchar(255) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `slider_url` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_title`, `slider_subheading`, `slider_image`, `slider_url`) VALUES
(6, 'Best free SEO friendly website', 'Etiam felis elit, mollis posuere accumsan ac, dignissim a ligula. Nam ullamcorper ornare tortor sed dapibus. Aliquam ultrices vestibulum sodales. Aenean efficitur massa vel tellus dapibus pellentesque.', 'c79f0-slider-03.jpg', 'http://google.com'),
(7, 'Social Networking', 'Etiam felis elit, mollis posuere accumsan ac, dignissim a ligula. Nam ullamcorper ornare tortor sed dapibus. Aliquam ultrices vestibulum sodales. Aenean efficitur massa vel tellus dapibus pellentesque.', '2ef43-slider-04.jpg', 'http://google.com'),
(8, 'Search engine, Analytics, Traffic', 'Etiam felis elit, mollis posuere accumsan ac, dignissim a ligula. Nam ullamcorper ornare tortor sed dapibus. Aliquam ultrices vestibulum sodales. Aenean efficitur massa vel tellus dapibus pellentesque.', '5d3de-slider-03.jpg', 'http://google.com');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_by` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonial_id`, `testimonial_title`, `testimonial_by`, `testimonial_description`, `testimonial_image`) VALUES
(5, ' Great & Talented Team!', 'Sikhar Dhawan - Batsman', '<p>\n	The master-builder of human happines no one rejects, dislikes avoids pleasure itself, because it is very pursue pleasure.&nbsp;</p>\n', ''),
(6, ' Awesome Services! ', 'Mitchell Starc - Fast Bowler', '<p>\n	Explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you completed.</p>\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$uA399cm3NhoUkqXUla5fK.GFk405MrJUpaBqdsoqdw1Rskhn5txRe', '', 'info@asa-ca.com.np', '', NULL, NULL, NULL, 1268889823, 1583056522, 1, 'Admin', 'istrator', 'ADMIN', '9841546900');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(6, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `collaborations`
--
ALTER TABLE `collaborations`
  ADD PRIMARY KEY (`collaboration_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD UNIQUE KEY `file_id` (`file_id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD UNIQUE KEY `folder_id` (`folder_id`),
  ADD UNIQUE KEY `folder_name` (`folder_name`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infographics`
--
ALTER TABLE `infographics`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`resource_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `collaborations`
--
ALTER TABLE `collaborations`
  MODIFY `collaboration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `folder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `infographics`
--
ALTER TABLE `infographics`
  MODIFY `info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
  MODIFY `resource_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;
