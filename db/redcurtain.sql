-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2020 at 11:44 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `redcurtain`
--

-- --------------------------------------------------------

--
-- Table structure for table `collaborations`
--

CREATE TABLE `collaborations` (
  `collaboration_id` int(11) NOT NULL,
  `collaboration_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collaboration_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collaboration_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rankorder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `collaborations`
--

INSERT INTO `collaborations` (`collaboration_id`, `collaboration_title`, `collaboration_image`, `collaboration_description`, `rankorder`) VALUES
(33, '', '09660-logo_01.png', '', 0),
(34, '', '9746f-logo_02.png', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_subject` varchar(255) NOT NULL,
  `contact_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `contact_name`, `contact_email`, `contact_subject`, `contact_message`) VALUES
(14, 'sushant chapagain', 'thirstyhacker97@gmail.com', '', 'sdasjdkakd'),
(15, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test'),
(16, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'tesst'),
(17, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test'),
(18, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test'),
(19, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `faq_id` int(11) NOT NULL,
  `faq_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faq_description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `faq_title`, `faq_description`) VALUES
(1, 'What should  i do if my interior is broken?', '<p>\n	<span style=\"color: rgb(128, 128, 128); font-family: &quot;nunito sans&quot;, -apple-system, BlinkMacSystemFont, &quot;segoe ui&quot;, Roboto, &quot;helvetica neue&quot;, Arial, sans-serif, &quot;apple color emoji&quot;, &quot;segoe ui emoji&quot;, &quot;segoe ui symbol&quot;, &quot;noto color emoji&quot;; font-size: 16px;\">Lorem ipsum dolor sit amet,&nbsp;</span><a href=\"http://localhost/redcurtain/assets/frontend/#\" style=\"box-sizing: border-box; color: rgb(152, 204, 211); text-decoration-line: none; transition: all 0.3s ease 0s; font-family: &quot;nunito sans&quot;, -apple-system, BlinkMacSystemFont, &quot;segoe ui&quot;, Roboto, &quot;helvetica neue&quot;, Arial, sans-serif, &quot;apple color emoji&quot;, &quot;segoe ui emoji&quot;, &quot;segoe ui symbol&quot;, &quot;noto color emoji&quot;; font-size: 16px;\">Cnsectetur adipisicing</a><span style=\"color: rgb(128, 128, 128); font-family: &quot;nunito sans&quot;, -apple-system, BlinkMacSystemFont, &quot;segoe ui&quot;, Roboto, &quot;helvetica neue&quot;, Arial, sans-serif, &quot;apple color emoji&quot;, &quot;segoe ui emoji&quot;, &quot;segoe ui symbol&quot;, &quot;noto color emoji&quot;; font-size: 16px;\">&nbsp;elit. Eos quos incidunt, perspiciatis, ad saepe, magnam error adipisci vitae ut provident alias! Odio debitis error ipsum molestiae voluptas accusantium quibusdam animi, soluta explicabo asperiores aliquid, modi natus suscipit deleniti. Corrupti, autem.</span></p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `file_type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `folder_id` int(11) NOT NULL,
  `file_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`file_id`, `file_name`, `file_type`, `folder_id`, `file_url`, `original_name`) VALUES
(11, '1', 'jpg', 4, 'http://localhost/covid/assets/uploads/1.jpg', '1.jpg'),
(12, '3', 'jpg', 4, 'http://localhost/covid/assets/uploads/3.jpg', '3.jpg'),
(13, '2', 'jpg', 4, 'http://localhost/covid/assets/uploads/2.jpg', '2.jpg'),
(14, '5', 'jpg', 4, 'http://localhost/covid/assets/uploads/5.jpg', '5.jpg'),
(15, '6', 'jpg', 4, 'http://localhost/covid/assets/uploads/6.jpg', '6.jpg'),
(16, 'noticefille', 'jpg', 6, 'http://localhost/covid/assets/uploads/noticefille.jpg', 'noticefille.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `folder_id` int(11) NOT NULL,
  `folder_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`folder_id`, `folder_name`, `project_id`) VALUES
(4, 'Page Photographs', 0),
(6, 'Notices', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'user', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `infographics`
--

CREATE TABLE `infographics` (
  `info_id` int(11) NOT NULL,
  `numberinfo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labelinfo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logomark` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `infographics`
--

INSERT INTO `infographics` (`info_id`, `numberinfo`, `labelinfo`, `logomark`) VALUES
(1, '79', 'Completed Projects', 'flaticon-briefcase'),
(2, '750', 'Happy Clients', 'flaticon-happy'),
(3, '36', 'Customer Services', 'flaticon-idea'),
(4, '20', 'Answered Questions', 'flaticon-customer-service');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(250) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `link` varchar(250) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `url_type` enum('internal','external') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_title`, `menu_order`, `link`, `parent_id`, `url_type`) VALUES
(75, 'Business Law and Arbitration Research Center', 1, 'page/business-law-and-arbitration-research-center', 74, 'internal'),
(76, 'Center For the Training of Trial Advocacy and Proffesionalism', 2, 'page/center-for-the-training-of-trial-advocacy-and-professionalism', 74, 'internal'),
(77, 'Center of Philosophical and Juristic Discourse', 3, 'page/center-of-philosophical-and-juristic-discourse', 74, 'internal'),
(102, 'Our Healthworkers', 2, 'page/our-healthworkers', 0, 'internal'),
(26, 'Home', 0, 'home', 0, 'internal'),
(91, 'KSL Policy and Legal Research Center', 7, 'page/ksl-policy-and-legal-research-center', 74, 'internal'),
(40, 'Message From Editorial', 3, 'page/message-from-editorial-', 0, 'internal'),
(48, 'Full Time Faculty', 0, 'faculty/full-time', 47, 'internal'),
(49, 'Adjunct Faculty', 1, 'faculty/adjunct-faculty', 47, 'internal'),
(50, 'Visiting Researchers/Fellows', 2, 'faculty/visiting-researchers', 47, 'internal'),
(51, 'Visiting Faculty', 3, 'faculty/visiting-faculty', 47, 'internal'),
(52, 'Faculty in News', 4, 'news/faculty-news', 47, 'internal'),
(54, 'Action Research', 1, 'page/action-research', 53, 'internal'),
(55, 'KSLPRC', 2, 'page/kslprc', 53, 'internal'),
(56, 'Research Department', 3, 'page/research-department', 53, 'internal'),
(84, 'MCD', 4, '', 57, 'internal'),
(85, 'RD', 5, '', 57, 'internal'),
(82, 'CLFSD', 2, '', 57, 'internal'),
(83, 'HRHLD', 3, '', 57, 'internal'),
(103, 'Contact Us', 8, 'home/contact', 0, 'internal'),
(81, 'CLD', 1, '', 57, 'internal'),
(72, 'Notices', 1, 'notices/all', 71, 'internal'),
(73, 'Exam & Results', 2, 'exam', 71, 'internal'),
(88, 'Clinical Law Department', 4, 'page/clinical-law-department', 74, 'internal'),
(89, 'Human Rights and Humanitarian Law Department', 5, 'page/human-rights-and-humanitarian-law-department', 74, 'internal'),
(90, 'International Law and Relations Study Center', 6, 'page/international-law-and-relations-study-center', 74, 'internal'),
(92, 'Moot Courts Department', 8, 'page/moot-courts-department', 74, 'internal'),
(93, 'Research Department', 9, 'page/research-department', 74, 'internal'),
(94, 'Criminal Law and Forensic Science Department', 10, 'page/criminal-law-and-forensic-science-department', 74, 'internal'),
(95, 'Human Rights and Criminal Justice Clinic', 11, 'page/human-rights-and-criminal-justice-clinic', 74, 'internal'),
(96, 'Kathmandu School of Law Review ', 12, 'kslr', 74, 'internal'),
(104, 'About Us', 1, '', 0, 'internal'),
(105, 'Donate', 6, 'page/donate', 0, 'internal'),
(110, 'Membership', 5, 'home/membership', 0, 'internal'),
(108, 'About Organization', 1, 'page/about-us', 104, 'internal'),
(109, 'Our Team', 2, 'home/teams', 104, 'internal'),
(111, 'Notices', 7, 'notice/notice', 0, 'internal'),
(112, 'Notices', 1, 'home/notice/notice', 111, 'internal'),
(113, 'News', 2, 'home/notice/news', 111, 'internal');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `notice_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_date` date NOT NULL,
  `notice_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_intro` text COLLATE utf8mb4_unicode_ci,
  `notice_type` enum('notice','news') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`notice_id`, `title`, `description`, `notice_date`, `notice_image`, `notice_intro`, `notice_type`) VALUES
(27, 'Will Europe\'s second wave of Covid-19 cases mean a second huge death toll?', '<div>\n	At first glance, the outlook doesn&#39;t seem too grim. While reported coronavirus cases are reaching record highs as Europe endures a &quot;second wave,&quot; deaths are still well below their peak in April.</div>\n<div>\n	&nbsp;</div>\n<div>\n	But experts warn the signs point to more tragedy ahead this winter.</div>\n<div>\n	Europe&#39;s hospitals are now better equipped for treating Covid-19. Measures such as social distancing and mask-wearing have become the norm and the latest spread of infection has been primarily among younger people, who are less likely to die if they contract the virus.</div>\n<div>\n	Yet colder weather is beginning to set in and the flu season is approaching. The infection is spreading to older populations, and there are signs that people are growing tired of adhering to the restrictions.</div>\n', '2020-03-28', '34e87-berlinrally.jpg', 'At first glance, the outlook doesn\'t seem too grim. While reported coronavirus cases are reaching record highs as Europe endures a \"second wave,\" deaths are still well below their peak in April.', 'news'),
(28, 'Hidden immune weakness found in 14% of gravely ill COVID-19 patients', '<div>\n	From the first months of the COVID-19 pandemic, scientists baffled by the disease&rsquo;s ferocity have wondered whether the body&rsquo;s vanguard virus fighter, a molecular messenger called type I interferon, is missing in action in some severe cases. Two papers published online in Science this week confirm that suspicion. They reveal that in a significant minority of patients with serious COVID-19, the interferon response has been crippled by genetic flaws or by rogue antibodies that attack interferon itself.</div>\n<div>\n	&nbsp;</div>\n<div>\n	&ldquo;Together these two papers explain nearly 14% of severe COVID-19 cases. That is quite amazing,&rdquo; says Qiang Pan- Hammarstr&ouml;m, an immunologist at the Karolinska Institute.</div>\n<div>\n	&nbsp;</div>\n<div>\n	<div>\n		The paired studies have immediate practical implications. Synthetic interferons, long used to treat other diseases, might help some at-risk patients, as might other therapies aimed at removing the damaging antibodies. A common kind of antibody test could be readily developed and return answers in hours. Those found to be at high risk of developing severe COVID- 19 could take precautions to avoid exposure or be prioritized for vaccination, says Elina Zuniga, an immunologist who studies interferons at the University of California, San Diego.</div>\n	<div>\n		&nbsp;</div>\n	<div>\n		The findings also raise a red flag for plasma donations from recovered patients. Because it may be rich in antibodies to the virus, &ldquo;convalescent plasma&rdquo; is already given to some patients to fight the infection. But some donations could harbor the interferon-neutralizing antibodies. &ldquo;You should eliminate these patients from the pool of donors,&rdquo; Zuniga says. &ldquo;You definitely don&rsquo;t want to be transferring these autoantibodies into another person.&rdquo;</div>\n	<div>\n		&nbsp;</div>\n	<div>\n		Type I interferons are made by every cell in the body and are vital leaders of the antiviral battle early in infection. They launch an immediate, intense local response when a virus invades a cell, triggering infected cells to produce proteins that attack the virus. They also summon immune cells to the site and alert uninfected neighboring cells to prepare their own defenses.</div>\n</div>\n<p>\n	&nbsp;</p>\n', '2020-09-22', 'bad5b-covid19immune.jpg', 'From the first months of the COVID-19 pandemic, scientists baffled by the disease’s ferocity have wondered whether the body’s vanguard virus fighter, a molecular messenger called type I interferon', 'news'),
(29, 'COVID-19 cases climbing in US, Europe, Canada', '<div>\n	After a steady decline from summer peaks through the beginning of September, new COVID-19 cases appear to be on the rise again in the United States.</div>\n<div>\n	&nbsp;</div>\n<div>\n	According to data from the New York Times, there have been 41,822 new cases per day in the country over the past week, a 14% increase from the previous 2 weeks. Among the states seeing the largest increases in new cases over the past 14 days are Wisconsin, North Dakota, and South Dakota.</div>\n<div>\n	&nbsp;</div>\n<div>\n	And the United States is far from the only country seeing a rise in COVID-19 cases as the fall begins. In a televised speech yesterday, Canadian Prime Minister Justin Trudeau said the nation is at a &quot;crossroads,&quot; with British Columbia, Alberta, Ontario, and Quebec all entering a second wave of the pandemic, the CBC reports. Trudeau urged Canadians to follow public health guidelines.</div>\n<div>\n	&nbsp;</div>\n<div>\n	&quot;I know this isn&#39;t the news that any of us wanted to hear. And we can&#39;t change today&#39;s numbers or even tomorrow&#39;s &hellip; but what we can change is where we are in October, and into the winter,&quot; he said.</div>\n<div>\n	&nbsp;</div>\n<div>\n	Meanwhile, coronavirus infections are surging throughout Europe, where health officials are warning the coming months could look similar to this past spring.</div>\n<div>\n	&nbsp;</div>\n<div>\n	Globally, there are now 32,048,333 confirmed COVID-19 cases, and 979,454 deaths, according to the Johns Hopkins COVID-19 dashboard. The United States accounts for 6,962,333 of those cases, with 202,467 deaths.</div>\n', '2020-09-30', 'e9226-hero_bg_1.jpg', 'After a steady decline from summer peaks through the beginning of September, new COVID-19 cases appear to be on the rise again in the United States.', 'news'),
(30, 'Appeal for Medical Assistance to SARS-COV2 PCR positive Doctors and Nurses', '<p>\n	<img alt=\"noticefille\" src=\"http://localhost/covid/assets/uploads/noticefille.jpg\" style=\"height: 1699px; width: 1200px;\" /></p>\n', '2020-09-27', '', 'Appeal for Medical Assistance to SARS-COV2 PCR positive Doctors and Nurses', 'notice');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  `page_url` varchar(250) NOT NULL,
  `featured_image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_title`, `page_slug`, `page_content`, `page_url`, `featured_image`) VALUES
(43, 'About Us', 'about-us', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 20px; color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 15px;\">\n	This page is dedicated for fundraising to support families of Nepalese doctors and nurses who might lose their lives whilst working during the COVID19 pandemic. Fund raised from the initiative will be given to help the bereaved families of the deceased.</p>\n', 'http://localhost/redcurtain/page/about-us', '3defa-hero_bg_1.jpg'),
(45, 'Our Healthworkers', 'our-healthworkers', '<div>\n	<img alt=\"5\" src=\"http://localhost/covid/assets/uploads/5.jpg\" style=\"height: 500px; width: 375px; float: left; margin: 10px;\" /><img alt=\"6\" src=\"http://localhost/covid/assets/uploads/6.jpg\" style=\"height: 500px; float: left; margin: 10px; width: 344px;\" /></div>\n<p>\n	<img alt=\"2\" src=\"http://localhost/covid/assets/uploads/2.jpg\" style=\"width: 375px; height: 500px; margin: 10px; float: left;\" /><img alt=\"1\" src=\"http://localhost/covid/assets/uploads/1.jpg\" style=\"height: 863px; width: 1150px;\" /></p>\n', 'http://localhost/covid/page/our-healthworkers', ''),
(46, 'Message From Editorial ', 'message-from-editorial-', '<p>\n	In December 2019, an epidemic by a new strain of Coronavirus, now commonly known as Coronavirus Disease 2019 (COVID-19), affected Wuhan, China. With the blink of an eye, the disease did not take much time to transition from epidemic to pandemic. The statistics for 24th September 2020 shows there are 31,664,104 confirmed cases and 972,221 deaths due to COVID-19 globally. COVID-19 has affected worldwide at the next level, which comes with an unimaginable price. Staying at home, social distancing, wearing a mask, and frequent hand sanitization quickly became the new norm for almost all with few exceptions. Professionals like doctors, nurses, other healthcare workers, and police officers stayed back in this latest battle between humankind and COVID-19.&nbsp; They were the new superheroes of the society, the front-liners, and the most vulnerable population for contracting this notorious infection. All healthcare workers have contributed at this challenging time of pandemic with immense determination and endurance. Not only this, most of them even realize that they may never go back to their families and loved ones for fear of putting them at increased risk of infection or due to the uncertainty of their future.</p>\n<div>\n	In an attempt to support the changing dynamic of healthcare workers&#39; lives in this pandemic, we have come forth with a Non-Profit Organization &#39;Covid-19 Health-workers Support Organization&#39;. The organization aims to provide doctors, nurses, and other healthcare workers, including their families, with psychological, social, and financial aid. The donation that the organization receives will mostly support families of deceased Nepalese healthcare workers who lost their precious lives while working in the COVID-19 pandemic. While this effort of our organization is minimal compared to the contribution they have made to our society, we aim at sustaining our kinds in need at such a stressful and demanding situation. Our plan includes making this service accessible to every doctor, nurses, and healthcare workers throughout Nepal. We will highly appreciate any contribution and inputs toward this organization. Let us all fight COVID-19 together!</div>\n<div>\n	&nbsp;</div>\n', 'http://localhost/covid/page/message-from-editorial-', ''),
(47, 'Donate', 'donate', '<div dir=\"auto\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: small;\">\n	<strong>For Domestic Public &nbsp;</strong></div>\n<div dir=\"auto\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: small;\">\n	1) esewa/khalti/ IPS</div>\n<div dir=\"auto\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: small;\">\n	2) HCW-Nmc number/Nnc number / others&nbsp;</div>\n<div dir=\"auto\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: small;\">\n	&nbsp;</div>\n<div dir=\"auto\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: small;\">\n	<strong>For International Public </strong></div>\n<div dir=\"auto\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: small;\">\n	1) Xoom/PayPal/wirebailey/others</div>\n<div dir=\"auto\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: small;\">\n	2)HCW-nmc number/Nnc numbers/ others&nbsp;</div>\n<div dir=\"auto\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: small;\">\n	&nbsp;</div>\n', 'http://localhost/covid/page/donate', '');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `portfolio_id` int(11) NOT NULL,
  `portfolio_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`portfolio_id`, `portfolio_name`, `portfolio_description`, `featured_image`, `portfolio_category_id`) VALUES
(1, 'Portfolio 2', '<p>\n	Portfolio 2</p>\n', '9883e-hero_bg_1.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_category`
--

CREATE TABLE `portfolio_category` (
  `portfolio_category_id` int(11) NOT NULL,
  `portfolio_category_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolio_category`
--

INSERT INTO `portfolio_category` (`portfolio_category_id`, `portfolio_category_name`, `featured_image`) VALUES
(1, 'Swimming Pool Design', 'b4a50-hero_bg_1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `project_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `project_title`, `project_description`, `project_image`, `project_category_id`) VALUES
(1, 'Project 1', '<p>\n	Project 1</p>\n', '19ea0-hero_bg_1.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_category`
--

CREATE TABLE `project_category` (
  `project_category_id` int(11) NOT NULL,
  `project_category_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_category`
--

INSERT INTO `project_category` (`project_category_id`, `project_category_name`, `featured_image`) VALUES
(1, 'Interior Design', '93fe8-tia-photo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `resource_id` int(9) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_title`, `service_description`, `service_image`) VALUES
(1, 'Internal Audit & Control Services', '<p>\n	<span courier=\"\" lucida=\"\" style=\"font-family: Consolas, \" white-space:=\"\">Whether you are looking to develop an internal audit function or make an existing one more effective, ASA CA has the experience tools and dedicated full-time resources to help you succeed.</span></p>\n', 'b1dbd-seo_06.png'),
(2, 'Assurance Services', '<p>\n	<span courier=\"\" lucida=\"\" style=\"font-family: Consolas, \" white-space:=\"\">Our accounting member firms can guide you through the often complex financial, operational and compliance decisions you need to make. They have the skills to provide the reassurance that your stakeholders need. </span></p>\n', '8b1fe-seo_02.png'),
(3, 'Taxation Services', '<p>\n	<span courier=\"\" lucida=\"\" style=\"font-family: Consolas, \" white-space:=\"\">While we ensure, that you maintain compliance, we make definitely sure that we create value for your business, giving you the best tax advice &amp; assistance.</span></p>\n', 'e128a-seo_03.png'),
(4, 'Company Secretarial Services', '<p>\n	Our company secretarial support service is designed to help private companies meet their standard statutory compliance needs.</p>\n', '6ef4f-seo_04.png'),
(5, 'Advisory & Other Services', '<div>\n	Our company Advisoru and Other services is designed to help private companies meet their standard statutory compliance needs via advisory and counselling.</div>\n<div>\n	&nbsp;</div>\n', 'f2230-seo_05.png'),
(6, 'Trainings/Workshops', '<p>\n	Our company provide trainings and workshops to various professional and beginners via a group of highly skilled Chartered Accountant and specialists in the area of accounting.</p>\n', 'da812-seo_01.png');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `slider_title` varchar(250) NOT NULL,
  `slider_subtitle` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_image`, `slider_title`, `slider_subtitle`) VALUES
(9, 'c37aa-hero_bg_1.jpg', 'INTERIOR DESIGN COMPANY', 'Experience Interior Design');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`team_id`, `name`, `designation`, `info`, `image`, `team_order`) VALUES
(4, 'Dr Milan K Thapa', 'President', '<p>\n	Milan K Thapa, MD</p>\n<p>\n	Anesthesiologist/ critical care</p>\n<p>\n	Grande international Hospital</p>\n<p>\n	President CHSO</p>\n<p>\n	Special interest in heart lung transplant</p>\n<p>\n	I believe the donation from the public and health-workers will defiantly help many health worker and their family</p>\n', '84657-milanthapa.jpg', 1),
(5, 'Dr. Mahesh Pathak', 'Vice President', '<p>\n	Mahesh Pathak, MD</p>\n<p>\n	Radiologist ,Nepal cancer hospital</p>\n<p>\n	Vice President CHSO.</p>\n<p>\n	Special interest in interventional radiology</p>\n<p>\n	Founder doctors society Nepal</p>\n<p>\n	Former member, NMA youth committe</p>\n<p>\n	maheshpathak1@yahoo.com</p>\n', '1fe64-maheshpathak.jpg', 2),
(6, 'Dr Hari Poudel', 'General Secretary', '<p>\n	Hari Poudel, MD</p>\n<p>\n	Anesthesiologists / critical care MCOMS</p>\n<p>\n	General Secretary, CHSO</p>\n<p>\n	Special interest in pain management</p>\n<p>\n	haripoudel1702@gmail.com</p>\n', '9ee84-haripoudel.jpg', 3),
(7, 'Dr Archana Budhathoki', 'Treasurer', '<p>\n	Archana Budhathoki, MD</p>\n<p>\n	PGY2 Ophthalmology</p>\n<p>\n	Treasurer CHSO</p>\n<p>\n	I would like to provide comprehensive service in general ophthalmology including cataract surgery and glaucoma treatment.</p>\n<p>\n	I want to raise awareness to prevent unnecessary visual loss.</p>\n<p>\n	archanabudhathoki7@gmail.com</p>\n', 'cfd64-archanabudathoki.jpg', 4),
(8, 'Dr Dipen Kadaria', 'Advisor', '<p>\n	Dipen Kadaria MD, FACP,FCCP</p>\n<p>\n	Associate Professor</p>\n<p>\n	Pulmonary critical careAdvisor (USA) CHSO.</p>\n<p>\n	President Danfe foundation</p>\n<p>\n	Interested in social work focused in health education</p>\n<p>\n	dipen.kadaria@yahoo.com</p>\n', '9f88f-dipenkadaria.jpg', 5),
(9, 'Dr Prabhat Adhikari', 'Advisor', '<p>\n	Prabhat Adhikari, MD</p>\n<p>\n	Fellowship critical care</p>\n<p>\n	Fellowship Infectious Disease</p>\n<p>\n	Grande international hospital</p>\n<p>\n	Advisor (CHSO)</p>\n<p>\n	Special interest in providing advanced medical equipment</p>\n<p>\n	Prabhatadhikari@gmail.com</p>\n', 'cf387-prabhatadhikari.jpg', 7),
(10, 'Dr Pawan Pushpa Baral', 'Executive Member', '<p>\n	Pawan Puspa Baral, MD</p>\n<p>\n	Anesthesiology/ critical care</p>\n<p>\n	UCMS</p>\n<p>\n	Executive member (CHSO)</p>\n<p>\n	Special interest in teaching and social work.</p>\n<p>\n	pawonbrl.dr@gmail.com</p>\n', '7dc79-pawanpuspabaral.jpg', 8),
(11, 'Dr Subechha Khadka', 'Executive Member', '<p>\n	Subechha khadka, DMD</p>\n<p>\n	Masters in global health.</p>\n<p>\n	Executive member CHSO</p>\n<p>\n	Former chair of Smile high foundation</p>\n<p>\n	Life time member of Texas medical association</p>\n<p>\n	subechha@gmail.com</p>\n', '28564-subechhakhadka.jpg', 9),
(12, 'Dr Sadikshya Pandey', 'Executive Member', '<p>\n	Sadikshya Pandey, MD PGY3 Pediatrics</p>\n<p>\n	Executive member CHSO</p>\n<p>\n	Special interest in field of paediatric neurology and behavioural science I am looking forward for the welfare of children with special needs .</p>\n', '1b7db-sadikshyapandey.jpg', 10),
(13, 'Dr Rabi Paudel', 'Executive Member', '<p>\n	Rabi Paudel, MBBS.</p>\n<p>\n	Inspector APF Nepal</p>\n<p>\n	Executive member CHSO</p>\n<p>\n	Special interest in Transfusion medicine</p>\n<p>\n	Paudelravee@gmail.com</p>\n', 'f0a29-rabipaudel.jpg', 11),
(14, 'Dr Laxman Thapa', 'Regional Member', '<p>\n	Laxman Thapa, GPST3.</p>\n<p>\n	NHS</p>\n<p>\n	Regional member (U.K)CHSO.</p>\n<p>\n	Special interest in mental health and rural medicine.</p>\n<p>\n	lax.thapa@outlook.com</p>\n', '0eb8e-laxmanthapa.jpg', 12),
(15, 'Dr Yub Raj Sedhai', 'Regional Member', '<p>\n	Yub Raj Sedhai, MD Internal medicine</p>\n<p>\n	Regional member (USA) CHSO.</p>\n<p>\n	Special interest in critical care</p>\n<p>\n	dr.sedhai@gmail.com</p>\n', '774dd-yubrajsehdai.jpg', 14),
(16, 'Dr Binayak Sigdel', 'Regional Member', '<p>\n	Binayak Sigdel, MD Pediatric intensivist.</p>\n<p>\n	Regional member(USA)CHSO.</p>\n<p>\n	CHSO is a great initiative to help raise fund which will be used for healthcare providers in Nepal who work with covid patients.</p>\n<p>\n	&ldquo;We are all in this together, we are here for each other.&rdquo;</p>\n<p>\n	drbinayak@hotmail.com</p>\n', 'ec429-binayaksigndel.jpg', 15),
(17, 'Dr Anjan Sharma', 'Regional Member', '<p>\n	Anjan Sharma, FRACGP</p>\n<p>\n	Regional member (AUS) CHSO</p>\n<p>\n	member NAWA(Nepalese association of Western Australia)</p>\n<p>\n	member ANMDA(Australian Nepalese Medical and Dental Association)</p>\n<p>\n	anjandoc@gmail.com</p>\n', '29628-anjansharma.jpg', 16),
(18, 'Dr Smita Joshi', 'Regional Member', '<p>\n	Smita Joshi, FRACGP</p>\n<p>\n	Regional member (AUS)CHSO.</p>\n<p>\n	Member AMADA Australasian medical and dental association.</p>\n<p>\n	joshi.smita80@gmail.com</p>\n', '26e30-smitajoshi.jpg', 17),
(19, 'Dr Jamun Singh', 'Regional Member', '<p>\n	Jamun Singh,MD</p>\n<p>\n	HOD pediatric Regional member( Dhanusha) CHSO.</p>\n<p>\n	Interest in teaching young doctors and developing a new hospitals for childrens who has a special needs.</p>\n<p>\n	jamunkch@gmail.com</p>\n', '76aa1-jamunsingh.jpg', 18),
(20, 'Dr Parash Pandey', 'Regional Member', '<p>\n	Parash Pandey, MD</p>\n<p>\n	Anesthesiology / critical care Regional member (Bheri) CHSO .</p>\n<p>\n	Philanthropy and training health worker.</p>\n<p>\n	Parashpandey12794@gmail.com</p>\n', 'd0e1c-parashpandey.jpg', 19);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_by` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonial_id`, `testimonial_title`, `testimonial_by`, `testimonial_description`, `testimonial_image`) VALUES
(5, ' Great & Talented Team!', 'Sikhar Dhawan - Batsman', '<p>\n	The master-builder of human happines no one rejects, dislikes avoids pleasure itself, because it is very pursue pleasure.&nbsp;</p>\n', '26080-sagun2.jpg'),
(6, ' Awesome Services! ', 'Mitchell Starc - Fast Bowler', '<p>\n	Explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you completed.</p>\n', '4ab10-testi_02.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$uA399cm3NhoUkqXUla5fK.GFk405MrJUpaBqdsoqdw1Rskhn5txRe', '', 'info@redcurtain.com', '', NULL, NULL, NULL, 1268889823, 1602665226, 1, 'Admin', 'istrator', 'ADMIN', '9841546900');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(6, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `collaborations`
--
ALTER TABLE `collaborations`
  ADD PRIMARY KEY (`collaboration_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD UNIQUE KEY `file_id` (`file_id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD UNIQUE KEY `folder_id` (`folder_id`),
  ADD UNIQUE KEY `folder_name` (`folder_name`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infographics`
--
ALTER TABLE `infographics`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`portfolio_id`);

--
-- Indexes for table `portfolio_category`
--
ALTER TABLE `portfolio_category`
  ADD PRIMARY KEY (`portfolio_category_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `project_category`
--
ALTER TABLE `project_category`
  ADD PRIMARY KEY (`project_category_id`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`resource_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `collaborations`
--
ALTER TABLE `collaborations`
  MODIFY `collaboration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `folder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `infographics`
--
ALTER TABLE `infographics`
  MODIFY `info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `portfolio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `portfolio_category`
--
ALTER TABLE `portfolio_category`
  MODIFY `portfolio_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_category`
--
ALTER TABLE `project_category`
  MODIFY `project_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
  MODIFY `resource_id` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;
